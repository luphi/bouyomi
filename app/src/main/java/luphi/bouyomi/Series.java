/* Copyright (C) 2019 Lucas Philipsen
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see http://www.gnu.org/licenses/.
 */

package luphi.bouyomi;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import androidx.annotation.NonNull;

import java.io.File;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Series {
    private List<Chapter> mChapters = new LinkedList<>();
    private Bitmap mCover = null;

    public String title = ""; /* Series title */
    public String summary = null; /* Series summary/description/synopsis */
    public String source = ""; /* Root location of the series, a URL or a local directory */
    public Scraper scraper = null;
    public boolean isRead = false; /* True if the entire series has been read */
    public boolean isLocal = false; /* True if the entire series is saved to disk */

    public void addChapter(Chapter chapter) {
        if ((chapter == null) || mChapters.contains(chapter))
            return;
        mChapters.add(chapter);
        if (!chapter.isLocal)
            isLocal = false;
        if (!chapter.isRead)
            isRead = false;
    }

    public List<Chapter> getChapters() {
        return mChapters;
    }

    public void sortChapters() {
        Collections.sort(mChapters); /* Sort the chapters by chapter number in ascending order */
        Collections.reverse(mChapters); /* Descending order is the goal, therefore... */
    }

    public void setCover(Bitmap cover) {
        mCover = cover;
    }

    public Bitmap getCover() {
        if (mCover != null)
            return mCover;
        if (scraper == null)
            return null;
        String filename = Core.get().downloader.coverPath(this);
        /* If there is a cover on disk for this series */
        if (new File(filename).exists()) {
            try {
                mCover = BitmapFactory.decodeFile(filename);
                return mCover;
            } catch (IllegalArgumentException e) {
                Utils.Log.e("Series", "Could not load cover at \"" + mCover + "\"");
            }
        }
        return null;
    }

    void freeCover() {
        if (mCover != null) {
            mCover.recycle();
            mCover = null;
        }
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Series))
            return false;
        return (hashCode() == other.hashCode());
    }

    @Override
    public int hashCode() {
        if ((scraper == null) || (title == null))
            return 0;
        return (scraper.hostName.hashCode() * title.hashCode());
    }

    @NonNull
    @Override
    public String toString() {
        return title;
    }
}
