/* Copyright (C) 2019 Lucas Philipsen
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see http://www.gnu.org/licenses/.
 */

package luphi.bouyomi;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static luphi.bouyomi.Utils.areAllChaptersLocal;
import static luphi.bouyomi.Utils.numLocalPages;

public class Downloader {
    private static class ChapterDownloadTask extends AsyncTask<Chapter, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Chapter... chapter) {
            if (isCancelled() || (chapter[0] == null))
                return false;
            long ticks = System.currentTimeMillis();
            /* While the number of pages retrieved from the host is still zero (i.e. not yet */
            /* retrieved) and 7.5 seconds have not yet passed */
            while ((chapter[0].getPages().size() == 0) &&
                    ((System.currentTimeMillis() - ticks) <= 7500)) {
                /* Wait for the scraper to retrieve the pages and/or number of pages */
                try {
                    Thread.sleep(100); /* Sleep 100 milliseconds */
                    if (isCancelled()) {
                        chapter[0].series.scraper.cancelAll();
                        return false;
                    }
                } catch (Exception e) {
                    if (!(e instanceof InterruptedException))
                        Utils.Log.e("Downloader", "Chapter task error", e);
                }
            }
            /* This task was successful if a list of pages was retrieved from the host */
            return (chapter[0].getPages().size() != 0);
        }

        @Override
        protected void onPostExecute(Boolean wasSuccess) {
            Core.get().downloader.onTaskExit(this, (wasSuccess ? TaskExitStatus.CHAPTER_SUCCESS :
                    TaskExitStatus.CHAPTER_FAILURE));
            }
    }

    private static class PageDownloadTask extends AsyncTask<Page, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Page... page) {
            if (isCancelled() || (page[0] == null))
                return false;
            long ticks = System.currentTimeMillis();
            /* While the bitmap hasn't been downloaded and the download has taken no more than */
            /* 7.5 seconds */
            while ((page[0].getImage() == null) && (System.currentTimeMillis() - ticks <= 7500)) {
                /* Wait for the download to finish */
                try {
                    Thread.sleep(100); /* Sleep 100 milliseconds */
                    if (isCancelled()) {
                        page[0].series.scraper.cancelAll();
                        return false;
                    }
                } catch (Exception e) {
                    if (!(e instanceof InterruptedException))
                        Utils.Log.e("Downloader", "Page task error", e);
                }
            }
            /* If the download was a success */
            if (page[0].getImage() != null) {
                boolean wasWritten = Core.get().downloader.writePageToDisk(page[0]);
                /* If this page_pager does not belong to the current chapter */
                if (Core.get().currentChapter != page[0].chapter)
                    page[0].freeImage(); /* Free it, there's no chance the chapter is being read */
                return wasWritten;
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean wasSuccess) {
            Core.get().downloader.onTaskExit(this, (wasSuccess ? TaskExitStatus.PAGE_SUCCESS :
                    TaskExitStatus.PAGE_FAILURE));
        }
    }

    private boolean mIsDownloading = false;
    private int mTaskCount = 0;
    private final int mMaxTaskCount = 12;
    private Deque<Chapter> mChapterQueue = new LinkedList<>();
    private Deque<Page> mPageQueue = new LinkedList<>();
    private static Map<Chapter, Integer> mChapterFailures = new HashMap<>();
    private static Map<Page, Integer> mPageFailures = new HashMap<>();
    private static Map<AsyncTask, Chapter> mChapterTasks = new HashMap<>();
    private static Map<AsyncTask, Page> mPageTasks = new HashMap<>();

    public boolean hasErrored = false;

    public void addSeriesToQueue(Series series) {
        if (series == null)
            return;
        /* Chapters, in their Series, are stored in descending order such that the latest */
        /* chapter is at index 0.  It makes more sense to begin downloading the oldest chapter */
        /* first so the chapter list is reversed only within the Downloader.  The Chapter */
        /* references remain. */
        List<Chapter> reversedList = new LinkedList<>(series.getChapters());
        Collections.reverse(reversedList);
        for (Chapter chapter : reversedList)
            addChapterToQueue(chapter);
    }

    void addChapterToQueue(Chapter chapter) {
        if ((chapter == null) || chapter.isLocal || mChapterQueue.contains(chapter))
            return;
        Core.get().downloadList.add(chapter);
        /* If the chapter's page_pager list must be populated */
        if (chapter.getPages().size() == 0)
            mChapterQueue.addLast(chapter);
        for (Page page : chapter.getPages())
            if (!page.isLocal)
                addPageToQueue(page);
    }

    public void start() {
        if (mIsDownloading || (mChapterQueue.isEmpty() && mPageQueue.isEmpty()))
            return;
        mIsDownloading = true;
        mTaskCount = 0;
        mPageFailures.clear();
        mChapterFailures.clear();
        hasErrored = false;
        while ((mTaskCount <= mMaxTaskCount) &&
                !(mChapterQueue.isEmpty() && mPageQueue.isEmpty()))
            nextTask();
        if (Core.get().mainActivity != null)
            Core.get().mainActivity.refreshDownloadQueueFragment();
    }

    public void stop() {
        if (!mIsDownloading)
            return;
        mIsDownloading = false;
        for (Map.Entry<AsyncTask, Page> entry : mPageTasks.entrySet()) {
            mPageQueue.addFirst(entry.getValue());
            entry.getKey().cancel(false);
        }
        for (Map.Entry<AsyncTask, Chapter> entry : mChapterTasks.entrySet()) {
            mChapterQueue.addFirst(entry.getValue());
            entry.getKey().cancel(false);
        }
        mPageTasks.clear();
        mChapterTasks.clear();
        hasErrored = false;
        Core.get().writeCatalogLite();
        if (Core.get().mainActivity != null)
            Core.get().mainActivity.refreshDownloadQueueFragment();
    }

    public boolean isDownloading() {
        return mIsDownloading;
    }

    public void clear() {
        if (mIsDownloading)
            stop();
        Core.get().downloadList.clear();
        mPageQueue.clear();
        mChapterQueue.clear();
        Core.get().writeCatalogLite();
    }

    String seriesPath(Series series) {
        if ((series == null) || (series.scraper == null))
            return "";
        return (Environment.getExternalStorageDirectory() + File.separator + ".bouyomi" +
                File.separator + series.title.replace(File.separator, "") + " (" +
                series.scraper.hostName + ")");
    }

    String coverPath(Series series) {
        return (seriesPath(series) + File.separator + "cover.png");
    }

    String pagePath(Page page) {
        String partialPath = chapterPath(page.chapter) + File.separator +
                page.chapter.getPages().indexOf(page);
        if (Core.get().getPreference("use_lossy_compression").equals("true"))
            return (partialPath + ".webp");
        return (partialPath + ".png");
    }

    public boolean writePageToDisk(Page page) {
        if ((page == null) || (page.getImage() == null))
            return false;
        String chapterPath = chapterPath(page.chapter), pagePath = pagePath(page);
        File file = new File(chapterPath);
        /* If the chapter's directory does not exist yet, try to create it */
        if (!file.exists() && !file.mkdirs()) {
            Utils.Log.w("Downloader", "Failed to create directory for chapter \"" +
                    page.chapter.title + "\" at " + chapterPath);
            return false;
        }
        file = new File(pagePath);
        /* If the image has not already been written to disk, write it */
        if (!file.exists()) {
            try {
                FileOutputStream fileOutputStream = new FileOutputStream(pagePath);
                if (Core.get().getPreference("use_lossy_compression").equals("true")) {
                    /* WebP has lossy and lossless modes.  No documentation for compress() */
                    /* specifies how the quality parameter (50 here) affects this.  It appears to */
                    /* vary by device.  A quality of 50 appears to be lossy for all devices but */
                    /* quality may vary.  In testing, the images were indistinguishable from PNG. */
                    page.getImage().compress(Bitmap.CompressFormat.WEBP, 50, fileOutputStream);
                } else
                    page.getImage().compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
                fileOutputStream.close();
            } catch (Exception e) {
                Utils.Log.e("Downloader", "Page download exception", e);
                return false;
            }
        }
        page.isLocal = true;
        page.source = pagePath;
        page.chapter.numLocalPages = numLocalPages(page.chapter);
        /* If all pages of the chapter are now local */
        if (page.chapter.getPages().size() == page.chapter.numLocalPages) {
            page.chapter.isLocal = true;
            if (areAllChaptersLocal(page.series))
                page.series.isLocal = true;
            Core.get().writeCatalogLite();
        }
        return true;
    }

    private void onTaskExit(AsyncTask task, TaskExitStatus status) {
        mTaskCount -= 1;
        if ((task == null) || !mIsDownloading)
            return;
        switch (status) {
            case CHAPTER_SUCCESS:
                Chapter chapter = mChapterTasks.remove(task);
                if (chapter == null)
                    break;
                mChapterFailures.remove(chapter);
                mPageQueue.addAll(chapter.getPages());
                while ((mTaskCount <= mMaxTaskCount) &&
                        !(mChapterQueue.isEmpty() && mPageQueue.isEmpty()))
                    nextTask();
                return;
            case CHAPTER_FAILURE:
                chapter = mChapterTasks.remove(task);
                if (chapter == null)
                    break;
                if (!mChapterFailures.containsKey(chapter))
                    mChapterFailures.put(chapter, 0);
                mChapterFailures.put(chapter, mChapterFailures.get(chapter) + 1);
                Utils.Log.w("Downloader", "Download task for chapter \"" + chapter.title +
                        "\" failed " + mChapterFailures.get(chapter) + " time(s)");
                if (mChapterFailures.get(chapter) == 3) {
                    hasErrored = true;
                    if (Core.get().mainActivity != null)
                        Core.get().mainActivity.refreshDownloadQueueFragment();
                }
                else
                    mChapterQueue.addFirst(chapter);
                break;
            case PAGE_SUCCESS:
                Page page = mPageTasks.remove(task);
                if (page == null)
                    break;
                mPageFailures.remove(page);
                /* writePageToDisk() will have already changed the "is local" flags if needed */
                if (page.chapter.isLocal) {
                    /* Check all the chapters to determine if they're all local with this success */
                    if ((Core.get().currentSeries == page.series) &&
                            (Core.get().mainActivity != null))
                        Core.get().mainActivity.refreshSeriesFragment();
                    Core.get().downloadList.remove(page.chapter);
                    /* If all chapters of the series are now local */
                    if (page.series.isLocal && (Core.get().mainActivity != null))
                        Core.get().mainActivity.refreshLibraryFragment();
                }
                /* Update progress bars, remove chapters when done, etc. */
                if (Core.get().mainActivity != null) {
                    // If this was the final task and all downloading is finished
                    if (mChapterQueue.isEmpty() && mPageQueue.isEmpty()&& (mTaskCount == 0))
                        mIsDownloading = false; // Update the flash before refreshing the fragment
                    Core.get().mainActivity.refreshDownloadQueueFragment();
                }
                break;
            case PAGE_FAILURE:
                page = mPageTasks.remove(task);
                if (page == null)
                    break;
                if (!mPageFailures.containsKey(page))
                    mPageFailures.put(page, 0);
                mPageFailures.put(page, mPageFailures.get(page) + 1);
                Utils.Log.w("Downloader", "Download task for page_pager index " + page.index +
                        " of chapter \"" + page.chapter.title + "\" failed " +
                        mPageFailures.get(page) + " time(s)");
                if (mPageFailures.get(page) == 3) {
                    hasErrored = true;
                    if (Core.get().mainActivity != null)
                        Core.get().mainActivity.refreshDownloadQueueFragment();
                }
                else
                    mPageQueue.addFirst(page);
                break;
        }
        nextTask();
    }

    private void nextTask() {
        if (mChapterQueue.isEmpty() && mPageQueue.isEmpty()) {
            if (mTaskCount == 0)
                stop();
            return;
        }
        if (!mPageQueue.isEmpty()) {
            PageDownloadTask pageDownloadTask = new PageDownloadTask();
            Page page = mPageQueue.removeFirst();
            if (page == null) {
                nextTask();
                return;
            }
            page.series.scraper.fillPage(page);
            try {
                pageDownloadTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, page);
                mPageTasks.put(pageDownloadTask, page);
                mTaskCount += 1;
            }
            catch (Exception e) {
                Utils.Log.e("Downloader", "Error while attempting to create a page_pager task for " +
                        "index " + page.index + " of chapter \"" + page.chapter.title + "\"", e);
                stop();
                hasErrored = true;
                if (Core.get().mainActivity != null)
                    Core.get().mainActivity.refreshDownloadQueueFragment();
            }
        }
        else {
            ChapterDownloadTask chapterDownloadTask = new ChapterDownloadTask();
            Chapter chapter = mChapterQueue.removeFirst();
            if (chapter == null) {
                nextTask();
                return;
            }
            chapter.series.scraper.fillChapter(chapter);
            try {
                chapterDownloadTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, chapter);
                mChapterTasks.put(chapterDownloadTask, chapter);
                mTaskCount += 1;
            }
            catch (Exception e) {
                Utils.Log.e("Downloader", "Error while attempting to create a chapter task for " +
                        "chapter \"" + chapter.title + "\"", e);
                stop();
                hasErrored = true;
                if (Core.get().mainActivity != null)
                    Core.get().mainActivity.refreshDownloadQueueFragment();
            }
        }
    }

    private boolean isLocal(Page page) {
        /* If the page_pager was already downloaded */
        if (page.isLocal)
            return true;
        /* If the page_pager was interrupted while downloading such that its image file exists but */
        /* has not been marked as local (the image will be saved at the path returned by */
        /* pagePath()) */
        return ((new File(pagePath(page))).exists());
    }

    private String chapterPath(Chapter chapter) {
        return seriesPath(chapter.series) + File.separator +
                chapter.title.replace(File.separator, "");
    }

    private void addPageToQueue(Page page) {
        if ((page == null) || mPageQueue.contains(page))
            return;
        /* If the page_pager has already been downloaded */
        if (isLocal(page))
            writePageToDisk(page);
        else
            mPageQueue.addLast(page);
    }

    private enum TaskExitStatus {
        CHAPTER_SUCCESS,
        CHAPTER_FAILURE,
        PAGE_SUCCESS,
        PAGE_FAILURE
    }
}
