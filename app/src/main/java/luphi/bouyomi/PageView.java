/* Copyright (C) 2019 Lucas Philipsen
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see http://www.gnu.org/licenses/.
 */

package luphi.bouyomi;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;

public class PageView extends androidx.appcompat.widget.AppCompatImageView {
    private class GestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onDoubleTap(MotionEvent e) {
            /* If the page_pager is not currently fit to the view */
            if (!mIsFit) {
                /* Fit it to whatever is appropriate for the mode and aspect ratio(s) */
                fitToView();
            } else /* if (mIsFit) */ {
                /* Zoom in */
                zoom(e.getX(), e.getY());
            }
            setMatrix(); /* Update the image matrix to apply the coordinates/dimensions */
            return true;
        }

        @Override
        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
            /* Increase the scroll speed a little */
            distanceX *= 1.5f;
            distanceY *= 1.5f;
            /* Translate the image such that its movement matches that of the scroll event */
            mModelRect.left -= distanceX;
            mModelRect.right -= distanceX;
            mModelRect.bottom -= distanceY;
            mModelRect.top -= distanceY;
            snapToEdge(); /* Prevent the image from being translated too far */
            setMatrix(); /* Update the image matrix to apply the coordinates/dimensions */
            return true;
        }
    }

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            /* The scale factor arrives as a value near 1.0 such that it may be be used in a */
            /* multiplication operation but the following math uses it more as a percentage */
            float scaleFactorNormalized = detector.getScaleFactor() - 1f;
            /* Calculate the new width and height (old value * percentage change) */
            float dW = (mModelRect.right - mModelRect.left) * scaleFactorNormalized,
                    dH = (mModelRect.bottom - mModelRect.top) * scaleFactorNormalized;
            /* Adjust the corners to apply the new width and height */
            mModelRect.left -= (dW / 2f);
            mModelRect.right += (dW / 2f);
            mModelRect.top -= (dH / 2f);
            mModelRect.bottom += (dH / 2f);
            mIsFit = false;
            /* If the image has been made too small */
            if (((mViewAspectRatio > mBitmapAspectRatio) &&
                    (mModelRect.bottom - mModelRect.top) < (mViewRect.bottom - mViewRect.top)) ||
                    ((mViewAspectRatio < mBitmapAspectRatio) && (mModelRect.right - mModelRect.left)
                            < (mViewRect.right - mViewRect.left))) {
                /* If the page_pager is wider than the view */
                if (mBitmapAspectRatio > mViewAspectRatio)
                    fitToPageWidth();
                else /* if (mBitmapAspectRatio <= mViewAspectRatio) */
                    fitToPageHeight();
            }
            setMatrix(); /* Update the image matrix to apply the coordinates/dimensions */
            return true;
        }
    }

    private GestureDetector mGestureDetector;
    private ScaleGestureDetector mScaleGestureDetector;
    private float mViewAspectRatio = 1f; /* Width / height of the display */
    private float mBitmapAspectRatio = 1f; /* Width / height of the page_pager image */
    private RectF mViewRect = new RectF(); /* Holds the values of the display */
    private RectF mBitmapRect = new RectF(); /* Holds the values of the original page_pager image */
    private RectF mModelRect = new RectF(); /* Holds the values of the currently displayed page_pager */
    private boolean mIsFit = false;

    public PageView(Context context) {
        this(context, null);
    }

    public PageView(Context context, AttributeSet attr) {
        this(context, attr, 0);
    }

    public PageView(Context context, AttributeSet attr, int defStyle) {
        super(context, attr, defStyle);
        if (Core.get().pagerReaderActivity == null) /* Probably impossible */
            return;
        mGestureDetector = new GestureDetector(Core.get().pagerReaderActivity,
                new GestureListener());
        mScaleGestureDetector = new ScaleGestureDetector(Core.get().pagerReaderActivity,
                new ScaleListener());
        setScaleType(ScaleType.MATRIX); /* Scale the image based on the model rect */
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent e) {
        mGestureDetector.onTouchEvent(e);
        mScaleGestureDetector.onTouchEvent(e);
        return true;
    }

    @Override
    public void setImageBitmap(Bitmap bitmap) {
        mBitmapAspectRatio = (float) bitmap.getWidth() / (float) bitmap.getHeight();
        mBitmapRect = new RectF(0, 0, bitmap.getWidth(), bitmap.getHeight());
        super.setImageBitmap(bitmap);
    }

    @Override
    protected boolean setFrame(int left, int top, int right, int bottom) {
        mViewAspectRatio = ((float) (right - left)) / ((float) (bottom - top));
        mViewRect = new RectF(left, top, right, bottom);
        fitToView(); /* Fit the page_pager to the display */
        setMatrix(); /* Update the image matrix to apply the coordinates/dimensions */
        return super.setFrame(left, top, right, bottom);
    }

    public boolean canSwipe() {
        if (isLandscapeMode() && (mIsFit ||
                ((mModelRect.right - mModelRect.left) <= (mViewRect.right - mViewRect.left))))
            return true;
        return mIsFit;
    }

    public void fitToView() {
        if (isPortraitMode()) {
            /* If the page_pager is wider than the view */
            if (mBitmapAspectRatio > mViewAspectRatio)
                fitToPageWidth();
            else
                fitToPageHeight();
        } else if (isLandscapeMode()) {
            /* If the page_pager is wider than the view */
            if (mBitmapAspectRatio > mViewAspectRatio)
                fitToPageHeight();
            else
                fitToPageWidth();
        }
        /* If the setting couldn't be determined (should be impossible) */
        else
            fitToPageWidth();
    }

    private void fitToPageWidth() {
        if (mIsFit)
            return;
        if (isPortraitMode()) {
            /* Set the bitmap's width to match the view's and center it vertically */
            mModelRect.left = mViewRect.left;
            mModelRect.right = mViewRect.right;
            float center = mViewRect.top + ((mViewRect.bottom - mViewRect.top) / 2f),
                    height = (mModelRect.right - mModelRect.left) / mBitmapAspectRatio;
            mModelRect.top = center - (height / 2f);
            mModelRect.bottom = center + (height / 2f);
        } else {
            /* Set the bitmap's width to match the view's and position it to the top of the page_pager */
            mModelRect.left = mViewRect.left;
            mModelRect.right = mViewRect.right;
            float height = (mModelRect.right - mModelRect.left) / mBitmapAspectRatio;
            mModelRect.top = mViewRect.top;
            mModelRect.bottom = mViewRect.top + height;
        }
        mIsFit = true;
        setMatrix();
    }

    private void fitToPageHeight() {
        if (mIsFit)
            return;
        /* Set the bitmap's height to match the view's and center it horizontally */
        mModelRect.top = mViewRect.top;
        mModelRect.bottom = mViewRect.bottom;
        float center = mViewRect.left + ((mViewRect.right - mViewRect.left) / 2f),
                width = (mModelRect.bottom - mModelRect.top) * mBitmapAspectRatio;
        mModelRect.left = center - (width / 2f);
        mModelRect.right = center + (width / 2f);
        mIsFit = true;
        setMatrix();
    }

    private void snapToEdge() {
        /* If the page_pager's width exceeds the view's width */
        if ((mModelRect.right - mModelRect.left) >= (mViewRect.right - mViewRect.left)) {
            /* Prohibit the user from panning past the edge of the page_pager */
            /* If the page_pager's right edge is to the left of the view's right edge */
            if (mModelRect.right < mViewRect.right) {
                float delta = Math.abs(mModelRect.right - mViewRect.right);
                mModelRect.right = mViewRect.right;
                mModelRect.left += delta;
            }
            /* If the page_pager's left edge is to the right of the view's left edge */
            else if (mModelRect.left > mViewRect.left) {
                float delta = Math.abs(mModelRect.left - mViewRect.left);
                mModelRect.left = mViewRect.left;
                mModelRect.right -= delta;
            }
        }
        /* If the view's width exceeds the page_pager's */
        else {
            /* Center the page_pager horizontally */
            float center = mViewRect.left + ((mViewRect.right - mViewRect.left) / 2f),
                    dimension = mModelRect.right - mModelRect.left;
            mModelRect.left = center - (dimension / 2f);
            mModelRect.right = center + (dimension / 2f);
        }
        /* If the page_pager's height exceeds the view's height */
        if ((mModelRect.bottom - mModelRect.top) >= (mViewRect.bottom - mViewRect.top)) {
            /* Prohibit the user from panning past the edge of the page_pager */
            /* If the page_pager's top edge is above the view's top edge */
            if (mModelRect.top > mViewRect.top) {
                float delta = Math.abs(mModelRect.top - mViewRect.top);
                mModelRect.top = mViewRect.top;
                mModelRect.bottom -= delta;
            }
            /* If the page_pager's bottom edge is below the view's bottom edge */
            else if (mModelRect.bottom < mViewRect.bottom) {
                float delta = Math.abs(mModelRect.bottom - mViewRect.bottom);
                mModelRect.bottom = mViewRect.bottom;
                mModelRect.top += delta;
            }
        }
        /* If the view's height exceeds the page_pager's */
        else {
            /* Center the page_pager vertically */
            float center = mViewRect.top + ((mViewRect.bottom - mViewRect.top) / 2f),
                    dimension = mModelRect.bottom - mModelRect.top;
            mModelRect.top = center - (dimension / 2f);
            mModelRect.bottom = center + (dimension / 2f);
        }
    }

    private void zoom(float focusX, float focusY) {
        float viewCenterX = mViewRect.left + ((mViewRect.right - mViewRect.left) / 2f),
                viewCenterY = mViewRect.top + ((mViewRect.bottom - mViewRect.top) / 2f);
        /* New dimensions = 1.75 * current dimensions */
        float newWidth = 1.75f * (mModelRect.right - mModelRect.left),
                newHeight = 1.75f * (mModelRect.bottom - mModelRect.top);
        /* Adjust the boundaries relative to the center of the view */
        mModelRect.left = viewCenterX - (newWidth / 2f);
        mModelRect.right = viewCenterX + (newWidth / 2f);
        mModelRect.top = viewCenterY - (newHeight / 2f);
        mModelRect.bottom = viewCenterY + (newHeight / 2f);
        /* Translate the page_pager by calculating the focus' coordinates relative to the view's center */
        mModelRect.left += 2f * (viewCenterX - focusX);
        mModelRect.right += 2f * (viewCenterX - focusX);
        mModelRect.top += 2f * (viewCenterY - focusY);
        mModelRect.bottom += 2f * (viewCenterY - focusY);
        snapToEdge();
        mIsFit = false;
    }

    private void setMatrix() {
        Matrix matrix = getImageMatrix();
        matrix.setRectToRect(mBitmapRect, mModelRect, Matrix.ScaleToFit.CENTER);
        setImageMatrix(matrix);
        invalidate();
    }

    private boolean isPortraitMode() {
        return (Core.get().getPreference("reader_orientation").equals("Portrait"));
    }

    private boolean isLandscapeMode() {
        return (Core.get().getPreference("reader_orientation").equals("Landscape"));
    }
}
