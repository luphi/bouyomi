/* Copyright (C) 2019 Lucas Philipsen
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see http://www.gnu.org/licenses/.
 */

package luphi.bouyomi.fragments;

import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import luphi.bouyomi.Core;
import luphi.bouyomi.activities.MainActivity;
import luphi.bouyomi.R;
import luphi.bouyomi.Scraper;
import luphi.bouyomi.Series;

public class SearchFragment extends Fragment implements AdapterView.OnItemSelectedListener {
    private View mView = null;
    private ArrayAdapter<Series> mAdapter = null;
    private Scraper mScraper = null;

    @SuppressWarnings("ConstantConditions")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_search, container, false);
            SearchView searchView = mView.findViewById(R.id.search);
            Spinner spinner = mView.findViewById(R.id.search_scraper_spinner);
            ListView listView = mView.findViewById(R.id.search_list);
            Core.get().searchResults.clear();
            mAdapter = new ArrayAdapter<>(Core.get().mainActivity.getApplicationContext(),
                    R.layout.list_item_text, Core.get().searchResults);
            listView.setAdapter(mAdapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    /* This routine is called when an item in the ListView element in the search */
                    /* fragment is pressed */
                    if ((getActivity() != null) && (position < mAdapter.getCount()) &&
                            (mAdapter.getItem(position) != null)) {
                        Core.get().currentSeries = mAdapter.getItem(position);
                        ((MainActivity) getActivity()).openDrawerByID(R.id.nav_series);
                        if (mScraper != null) {
                            mScraper.fillSeriesFragment(mAdapter.getItem(position));
                        }
                    }
                }
            });
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    /* This routine is executed upon performing a search via the SearchView */
                    /* element at the top of the fragment */
                    Core.get().searchResults.clear();
                    Core.get().searchComplete = false;
                    mView.findViewById(R.id.search_progress_bar).setVisibility(View.VISIBLE);
                    /* Chances are, the scraper is idle but, seeing as a series has been */
                    /* selected, no more searching needs to be done (a scraper may potentially be */
                    /* parsing multiple search result pages) */
                    mScraper.cancelAll();
                    mScraper.fillSearchFragment(query);
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    return false;
                }
            });
            spinner.setOnItemSelectedListener(this);
            mScraper = Core.get().getDefaultScraper(); /* Never returns null */
            TypedArray entryValues = Core.get().mainActivity.getResources().obtainTypedArray(
                    R.array.scraper_list);
            for (int i = 0; i < entryValues.length(); i++) {
                if (entryValues.getString(i).equals(mScraper.hostName)) {
                    spinner.setSelection(i);
                    break;
                }
            }
            entryValues.recycle();
        }
        return mView;
    }

    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        mScraper = Core.get().getScraperByName((String) parent.getItemAtPosition(position));
    }

    /* Needed by the Spinner but unused */
    public void onNothingSelected(AdapterView<?> parent) {
    }

    public void refresh() {
        /* If not being executed on the UI thread */
        if (Looper.getMainLooper().getThread() != Thread.currentThread())
            return;
        if (mView != null) {
            if (Core.get().searchComplete)
                mView.findViewById(R.id.search_progress_bar).setVisibility(View.GONE);
            else
                mView.findViewById(R.id.search_progress_bar).setVisibility(View.VISIBLE);
            if (mAdapter != null)
                mAdapter.notifyDataSetChanged();
        }
    }
}
