/* Copyright (C) 2019 Lucas Philipsen
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see http://www.gnu.org/licenses/.
 */

package luphi.bouyomi.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import luphi.bouyomi.Chapter;
import luphi.bouyomi.Core;
import luphi.bouyomi.R;
import luphi.bouyomi.activities.PagerReaderActivity;
import luphi.bouyomi.Series;
import luphi.bouyomi.activities.StitcherReaderActivity;
import luphi.bouyomi.adapters.ChapterAdapter;

public class SeriesFragment extends Fragment {
    private View mView = null;
    private ChapterAdapter mAdapter = null;
    private Series mSeries = new Series();

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_series, container, false);
            ListView listView = mView.findViewById(R.id.series_chapter_list);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Chapter chapter;

                    if (Core.get().isCurrentSeriesAvailable) {
                        chapter = mAdapter.getItem(position);
                        if (chapter != null) {
                            Core.get().currentSeries = chapter.series;
                            Core.get().currentChapter = chapter;
                            /* The PagerReaderActivity will manage the loading of chapters and */
                            /* pages based on the series and chapter set in the above lines */
                            if (Core.get().getPreference("reader_type").equals("Pager"))
                                startActivity(new Intent(
                                        Core.get().mainActivity.getApplicationContext(),
                                        PagerReaderActivity.class));
                            else if (Core.get().getPreference("reader_type").equals("Stitcher"))
                                startActivity(new Intent(
                                        Core.get().mainActivity.getApplicationContext(),
                                        StitcherReaderActivity.class));
                            refresh();
                        }
                    }
                }
            });
        }
        refresh();
        return mView;
    }

    public void refresh() {/* If not being executed on the UI thread or a necessary object is missing */
        if ((Looper.getMainLooper().getThread() != Thread.currentThread()) || (mView == null) ||
                (Core.get().mainActivity == null) || (Core.get().currentSeries == null))
            return;
        boolean isNewSeries = (mSeries != Core.get().currentSeries);
        mSeries = Core.get().currentSeries;
        /* First, the availability notice */
        if (Core.get().isCurrentSeriesAvailable)
            mView.findViewById(R.id.series_unavailable).setVisibility(View.GONE);
        else {
            /* Display the notice */
            mView.findViewById(R.id.series_unavailable).setVisibility(View.VISIBLE);
            /* If this fragment is displaying the results of a randomly-chosen series */
            if (Core.get().mainActivity.previousNavID == R.id.nav_random) {
                /* As it is possible for a random series to be licensed and unavailable, change */
                /* the options menu to remove the "Add to library" option but retain the "New */
                /* random series" option */
                Core.get().mainActivity.setOptionsMenu(R.menu.series_random_without_add);
            }
            /* If this fragment is displaying the results of a series search */
            else if (Core.get().mainActivity.previousNavID == R.id.nav_search) {
                /* Remove the options menu, preventing the user from adding the series to the */
                /* library. The options menu only contains "Add to library" during a search. */
                Core.get().mainActivity.clearOptionsMenu();
            }
        }
        /* Second, the cover */
        if (mSeries.getCover() != null) {
            ((ImageView) mView.findViewById(R.id.series_cover)).setImageBitmap(mSeries.getCover());
            mView.findViewById(R.id.series_cover_space).setVisibility(View.VISIBLE);
        }
        else {
            ((ImageView) mView.findViewById(R.id.series_cover)).setImageResource(0);
            mView.findViewById(R.id.series_cover_space).setVisibility(View.GONE);
        }
        /* Third, the title */
        ((TextView) mView.findViewById(R.id.series_title)).setText(mSeries.title);
        /* Fourth, the summary */
        if (mSeries.summary != null) {
            ((TextView) mView.findViewById(R.id.series_summary)).setText(mSeries.summary);
            mView.findViewById(R.id.series_summary_progress_bar).setVisibility(View.GONE);
            mView.findViewById(R.id.series_summary_scrollview).setVisibility(View.VISIBLE);
        }
        else {
            mView.findViewById(R.id.series_summary_progress_bar).setVisibility(View.VISIBLE);
            mView.findViewById(R.id.series_summary_scrollview).setVisibility(View.GONE);
        }
        /* Fifth, the  chapter list */
        if (mSeries.getChapters().isEmpty()) {
            mView.findViewById(R.id.series_chapter_list_progress_bar).setVisibility(View.VISIBLE);
            mView.findViewById(R.id.series_chapter_list).setVisibility(View.GONE);
            mAdapter = null;
        }
        else {
            mView.findViewById(R.id.series_chapter_list_progress_bar).setVisibility(View.GONE);
            mView.findViewById(R.id.series_chapter_list).setVisibility(View.VISIBLE);
            if ((mAdapter == null) || isNewSeries) {
                /* The false parameter indicates the chapter list is not showing updates (which */
                /* is instead used by the UpdatesFragment) */
                mAdapter = new ChapterAdapter(mSeries.getChapters(), false);
                ((ListView) mView.findViewById(R.id.series_chapter_list)).setAdapter(mAdapter);
            }
            else {
                mAdapter.notifyDataSetChanged();
            }
        }
    }
}
