/* Copyright (C) 2019 Lucas Philipsen
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see http://www.gnu.org/licenses/.
 */

package luphi.bouyomi.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import luphi.bouyomi.Core;
import luphi.bouyomi.activities.PagerReaderActivity;
import luphi.bouyomi.activities.StitcherReaderActivity;
import luphi.bouyomi.adapters.UpdatesAdapter;
import luphi.bouyomi.Chapter;
import luphi.bouyomi.R;

public class UpdatesFragment extends Fragment {
    private View mView = null;
    private UpdatesAdapter mAdapter = new UpdatesAdapter();

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (mView == null) {
            /* Inflate the view (and store it as a variable) */
            mView = inflater.inflate(R.layout.fragment_updates, container, false);
            ListView listView = mView.findViewById(R.id.updates_list);
            listView.setAdapter(mAdapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Chapter chapter;

                    chapter = mAdapter.getItem(position);
                    if (chapter != null) {
                        Core.get().currentSeries = chapter.series;
                        Core.get().currentChapter = chapter;
                        /* The PagerReaderActivity will manage the loading of chapters of pages based */
                        /* on the series and chapter set in the above lines */
                        if (Core.get().getPreference("reader_type").equals("Pager"))
                            startActivity(new Intent(
                                    Core.get().mainActivity.getApplicationContext(),
                                    PagerReaderActivity.class));
                        else if (Core.get().getPreference("reader_type").equals("Stitcher"))
                            startActivity(new Intent(
                                    Core.get().mainActivity.getApplicationContext(),
                                    StitcherReaderActivity.class));
                    }
                }
            });
        }
        Core.get().sortUpdatesList();
        refresh();
        return mView;
    }

    public void refresh() {
        /* If not being executed on the UI thread */
        if (Looper.getMainLooper().getThread() != Thread.currentThread())
            return;
        if (mView != null) {
            Core.get().expireUpdatesList();
            mAdapter.notifyDataSetChanged();
        }
    }
}
