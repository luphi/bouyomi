/* Copyright (C) 2019 Lucas Philipsen
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see http://www.gnu.org/licenses/.
 */

package luphi.bouyomi.fragments;

import android.os.Bundle;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import luphi.bouyomi.Core;
import luphi.bouyomi.R;
import luphi.bouyomi.adapters.ChapterAdapter;

public class DownloadQueueFragment extends Fragment {
    private View mView = null;
    private ChapterAdapter mAdapter = new ChapterAdapter(Core.get().downloadList, true);

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_download_queue, container, false);
            ((ListView) mView.findViewById(R.id.download_queue_list)).setAdapter(mAdapter);
        }
        refresh();
        return mView;
    }

    public void refresh() {
        /* If not being executed on the UI thread */
        if ((mView == null) || (Looper.getMainLooper().getThread() != Thread.currentThread()))
            return;
        mAdapter.notifyDataSetChanged();
        if (Core.get().mainActivity != null) {
            if (Core.get().downloader.isDownloading())
                Core.get().mainActivity.setOptionsMenu(R.menu.download_queue_active);
            else
                Core.get().mainActivity.setOptionsMenu(R.menu.download_queue_idle);
        }
        if (Core.get().downloader.hasErrored)
            mView.findViewById(R.id.download_queue_error_message).setVisibility(View.VISIBLE);
        else
            mView.findViewById(R.id.download_queue_error_message).setVisibility(View.GONE);
    }
}
