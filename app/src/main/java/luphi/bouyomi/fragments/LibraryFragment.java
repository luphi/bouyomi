/* Copyright (C) 2019 Lucas Philipsen
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see http://www.gnu.org/licenses/.
 */

package luphi.bouyomi.fragments;

import android.os.Bundle;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import luphi.bouyomi.Core;
import luphi.bouyomi.activities.MainActivity;
import luphi.bouyomi.R;
import luphi.bouyomi.adapters.SeriesAdapter;

public class LibraryFragment extends Fragment {
    private View mView = null;
    private SeriesAdapter mAdapter = new SeriesAdapter();

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_library, container, false);
            ListView listView = mView.findViewById(R.id.library_list);
            listView.setAdapter(mAdapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if ((position >= mAdapter.getCount()) || (mAdapter.getItem(position) == null))
                        return;
                    Core.get().currentSeries = mAdapter.getItem(position);
                    if (getActivity() != null)
                        ((MainActivity) getActivity()).openDrawerByID(R.id.nav_series);
                }
            });
        }
        refresh();
        return mView;
    }

    public void refresh() {
        /* If not being executed on the UI thread */
        if (Looper.getMainLooper().getThread() != Thread.currentThread())
            return;
        if (mView != null) {
            if (Core.get().isLibraryLoading) {
                mView.findViewById(R.id.library_loading_message).setVisibility(View.VISIBLE);
                mView.findViewById(R.id.library_empty_message).setVisibility(View.INVISIBLE);
            } else if (Core.get().library.isEmpty()) {
                mView.findViewById(R.id.library_loading_message).setVisibility(View.INVISIBLE);
                mView.findViewById(R.id.library_empty_message).setVisibility(View.VISIBLE);
            } else {
                mView.findViewById(R.id.library_loading_message).setVisibility(View.INVISIBLE);
                mView.findViewById(R.id.library_empty_message).setVisibility(View.INVISIBLE);
            }
            mAdapter.notifyDataSetChanged();
        }
    }
}
