/* Copyright (C) 2019 Lucas Philipsen
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see http://www.gnu.org/licenses/.
 */

package luphi.bouyomi.fragments;

import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.preference.PreferenceFragmentCompat;

import luphi.bouyomi.Core;
import luphi.bouyomi.R;

public class PreferenceFragment extends PreferenceFragmentCompat implements
        SharedPreferences.OnSharedPreferenceChangeListener {
    @Override
    public void onCreatePreferences(Bundle bundle, String s) {
        addPreferencesFromResource(R.xml.preferences);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        /* If the user wants to use landscape reader orientation AND the stitcher type (which */
        /* are not a recommended combination), tell the user */
        if ((key.equals("reader_orientation") || key.equals("reader_type")) &&
                sharedPreferences.getString("reader_orientation", "").equals("Landscape") &&
                sharedPreferences.getString("reader_type", "").equals("Stitcher")) {
            if (Core.get().mainActivity == null)
                return;
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(Core.get().mainActivity);
            alertBuilder.setMessage(R.string.stitcher_recommendation);
            AlertDialog alertDialog = alertBuilder.create();
            alertDialog.show();
        }
        /* If the user just changed the default scraper */
        else if (key.equals("scraper"))
            Core.get().setScraperByName(sharedPreferences.getString(key, ""));
        /* Have the core store the preferences in its own map, and respond to changes as needed */
        Core.get().readPreferences();
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(
                this);
    }

    @Override
    public void onPause() {
        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(
                this);
        super.onPause();
    }
}
