/* Copyright (C) 2019 Lucas Philipsen
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see http://www.gnu.org/licenses/.
 */

package luphi.bouyomi;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.viewpager.widget.ViewPager;

import luphi.bouyomi.adapters.PagerReaderAdapter;

public class ReaderViewPager extends ViewPager {
    private boolean mCanSwipe = true;

    public ReaderViewPager(Context context) {
        this(context, null);
    }

    public ReaderViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        setOverScrollMode(OVER_SCROLL_NEVER);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent e) {
        if (e.getAction() == MotionEvent.ACTION_MOVE) {
            /* Get the adapter and, through it, the page_pager to check if it should pan or if this */
            /* pager should change pages */
            PagerReaderAdapter adapter = (PagerReaderAdapter) getAdapter();
            if (adapter != null) {
                PageView pageView = adapter.getCurrentPageView();
                if (pageView != null)
                    mCanSwipe = pageView.canSwipe();
            }
        }
        /* return true; -> children do NOT receive the event */
        /* return false; -> children do receive the event */
        return (mCanSwipe && super.onInterceptTouchEvent(e));
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent e) {
        return (mCanSwipe && super.onTouchEvent(e));
    }
}
