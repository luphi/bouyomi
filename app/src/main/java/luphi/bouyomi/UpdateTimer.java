/* Copyright (C) 2019 Lucas Philipsen
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see http://www.gnu.org/licenses/.
 */

package luphi.bouyomi;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

public class UpdateTimer extends BroadcastReceiver {
    private boolean mRunning = false;

    @Override
    public void onReceive(Context context, Intent intent) {
        Core.get().expireUpdatesList();
        Core.get().checkForUpdates();
    }

    void start() {
        if (mRunning || (Core.get().mainActivity == null))
            return;
        Intent intent = new Intent(Core.get().mainActivity, UpdateTimer.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(Core.get().mainActivity, 0, intent,
                0);
        AlarmManager alarmManager = (AlarmManager)
                Core.get().mainActivity.getSystemService(Context.ALARM_SERVICE);
        if (alarmManager != null) {
            alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                    SystemClock.elapsedRealtime() + 10000, /* Do the initial run in 10 seconds */
                    AlarmManager.INTERVAL_FIFTEEN_MINUTES, /* Repeat every 15 minutes */
                    pendingIntent);
            mRunning = true;
        }
    }

    void stop() {
        if (!mRunning || (Core.get().mainActivity == null))
            return;
        Intent intent = new Intent(Core.get().mainActivity, UpdateTimer.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(Core.get().mainActivity, 0, intent,
                0);
        AlarmManager alarmManager = (AlarmManager)
                Core.get().mainActivity.getSystemService(Context.ALARM_SERVICE);
        if (alarmManager != null) {
            alarmManager.cancel(pendingIntent);
            mRunning = false;
        }
    }
}
