/* Copyright (C) 2019 Lucas Philipsen
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see http://www.gnu.org/licenses/.
 */

package luphi.bouyomi;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.InterruptedIOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.LinkedList;
import java.util.List;

public abstract class Scraper {
    protected static List<AsyncTask> mTasks = new LinkedList<>();
    public boolean hasRandom = false; /* True if the host has a random series feature */
    public String hostName = ""; /* The name of the host, not for DNS resolves */

    public void cancelAll() {
        synchronized (this) {
            for (AsyncTask task : mTasks)
                task.cancel(true);
            mTasks.clear();
        }
    }

    public static List<String> downloadPage(String pageUrl) {
        List<String> markup = new LinkedList<>();
        InputStream inputStream = null;
        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(pageUrl).openConnection();
            inputStream = new BufferedInputStream(connection.getInputStream());
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK)
                throw new Exception("Received HTTP response code " + connection.getResponseCode());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = bufferedReader.readLine()) != null)
                markup.add(line);
        } catch (Exception e) {
            /* If the exception isn't an ignorable one */
            if (!(e instanceof InterruptedIOException || e instanceof UnknownHostException ||
                    e instanceof MalformedURLException))
                Utils.Log.e("Scraper", "downloadPage exception", e);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    Utils.Log.e("Scraper", "Stream I/O error (downloadPage)", e);
                }
            }
        }
        return markup;
    }

    public static Bitmap downloadImage(String imageUrl) {
        InputStream inputStream = null;
        Bitmap image = null;
        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(imageUrl).openConnection();
            connection.setDoInput(true);
            connection.connect();
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK)
                throw new Exception("Received HTTP response code " + connection.getResponseCode());
            inputStream = connection.getInputStream();
            image = BitmapFactory.decodeStream(inputStream);
            if (image == null)
                return null;
            /* If either dimensions of the image exceeds the maximum possible OpenGL texture */
            /* resolution (varies by device) */
            int max = Utils.getOpenGLMaxTextureDimension();
            if ((image.getWidth() > max) || (image.getHeight() > max)) {
                float aspectRatio = (float) image.getWidth() / (float) image.getHeight();
                if (image.getWidth() >= image.getHeight()) {
                    image = Bitmap.createScaledBitmap(image, max,
                            (int) ((float) max / aspectRatio), true);
                } else {
                    image = Bitmap.createScaledBitmap(image, (int) ((float) max * aspectRatio),
                            max, true);
                }
            }
        } catch (Exception e) {
            if (!(e instanceof InterruptedIOException || e instanceof MalformedURLException))
                Utils.Log.e("Scraper", "downloadImage exception", e);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    Utils.Log.e("Scraper", "Stream I/O error (downloadImage)", e);
                }
            }
        }
        return image;
    }

    /* Passes a list of Series objects to the SearchFragment */
    /* These Series objects have their scraper, source, and title filled out */
    public abstract void fillSearchFragment(String title);

    /* Passes a Series object to the SeriesFragment */
    /* This Series object has its summary, cover source, and chapter list filled out */
    /* If the series is marked unavailable or mature, the SeriesFragment is alerted */
    public abstract void fillSeriesFragment(Series series);

    /* If supported by the scraper, performs the same operations as fillSeriesFragment() */
    public abstract void fillSeriesFragmentRandom();

    /* Provides the chapter its list of pages.  These Page objects have their source filled out. */
    public abstract void fillChapter(Chapter chapter);

    /* Provides the page_pager with its image */
    public abstract void fillPage(Page page);

    /* Checks the host for new chapters and a new cover */
    public abstract void checkForUpdates(Series series);
}
