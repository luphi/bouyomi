/* Copyright (C) 2019 Lucas Philipsen
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see http://www.gnu.org/licenses/.
 */

package luphi.bouyomi.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.List;

import luphi.bouyomi.Chapter;
import luphi.bouyomi.Core;
import luphi.bouyomi.R;

public class ChapterAdapter extends ArrayAdapter<Chapter> {
    private List<Chapter> mList;
    private boolean mIsDownload; /* True if displaying in the download queue */

    public ChapterAdapter(List<Chapter> list, boolean isDownload) {
        super(Core.get().mainActivity.getApplicationContext(), R.layout.list_item_chapter, list);
        mList = list;
        mIsDownload = isDownload;
    }

    @NonNull
    @Override
    public View getView(int position, View listItem, @NonNull ViewGroup parent) {
        /* If the list item does not exist (i.e. this is the first instantiation of this list) */
        if (listItem == null)
            listItem = LayoutInflater.from(Core.get().mainActivity.getApplicationContext()).inflate(
                    R.layout.list_item_chapter, parent, false); /* Inflate the list item */
        if ((mList == null) || (position >= mList.size()))
            return listItem;
        Chapter chapter = mList.get(position);
        ImageView cover = listItem.findViewById(R.id.chapter_item_cover),
                readIcon = listItem.findViewById(R.id.chapter_item_read_icon),
                localIcon = listItem.findViewById(R.id.chapter_item_local_icon);
        TextView seriesTitle = listItem.findViewById(R.id.chapter_item_series_title),
                chapterTitle = listItem.findViewById(R.id.chapter_item_chapter_title);
        ProgressBar progressBar = listItem.findViewById(R.id.chapter_item_progress_bar);
        /* If the series' cover art and a progress bar should be displayed with each list item */
        if (mIsDownload && (chapter.series != null) && (chapter.series.getCover() != null))
            cover.setImageBitmap(chapter.series.getCover());
        else
            cover.setVisibility(View.GONE);
        /* If displaying in the download queue, display the series' title */
        if (mIsDownload && (chapter.series != null)) {
            seriesTitle.setText(chapter.series.title);
            seriesTitle.setVisibility(View.VISIBLE);
        } else
            seriesTitle.setVisibility(View.GONE);
        chapterTitle.setText(chapter.title);
        /* If the chapter has been read */
        if (chapter.isRead)
            readIcon.setVisibility(View.VISIBLE);
        else
            readIcon.setVisibility(View.GONE);
        /* If the chapter does not have all of its pages saved locally */
        if (!chapter.isLocal)
            localIcon.setVisibility(View.GONE);
        else
            localIcon.setVisibility(View.VISIBLE);
        /* If displaying in the download queue and some number of pages has been downloaded */
        if (mIsDownload && !chapter.getPages().isEmpty() && (chapter.numLocalPages != 0)) {
            /* Make the progress bar view visible */
            progressBar.setVisibility(View.VISIBLE);
            /* Set the progress as a percentage calculated from the number of pages which are */
            /* local out of the total number of pages */
            progressBar.setProgress((int) (100.0f *
                    ((float) chapter.numLocalPages) / ((float) chapter.getPages().size())));
        } else
            progressBar.setVisibility(View.GONE);
        return listItem;
    }
}