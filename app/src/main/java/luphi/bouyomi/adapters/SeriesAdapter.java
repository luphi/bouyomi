/* Copyright (C) 2019 Lucas Philipsen
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see http://www.gnu.org/licenses/.
 */

package luphi.bouyomi.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import luphi.bouyomi.Series;
import luphi.bouyomi.Core;
import luphi.bouyomi.R;

public class SeriesAdapter extends ArrayAdapter<Series> {
    public SeriesAdapter() {
        super(Core.get().mainActivity.getApplicationContext(), R.layout.list_item_series,
                Core.get().library);
    }

    @NonNull
    @Override
    public View getView(int position, View listItem, @NonNull ViewGroup parent) {
        /* If the list item does not exist (i.e. this is the first instantiation of this list) */
        if (listItem == null)
            listItem = LayoutInflater.from(Core.get().mainActivity.getApplicationContext()).inflate(
                    R.layout.list_item_series, parent, false); /* Inflate the list item */
        if (position >= Core.get().library.size())
            return listItem;
        Series series = Core.get().library.get(position);
        ImageView cover = listItem.findViewById(R.id.series_item_cover),
                readIcon = listItem.findViewById(R.id.series_item_read_icon),
                localIcon = listItem.findViewById(R.id.series_item_local_icon);
        TextView title = listItem.findViewById(R.id.series_item_title),
                scraper = listItem.findViewById(R.id.series_item_scraper);
        if (series.getCover() == null)
            cover.setVisibility(View.GONE);
        else {
            cover.setVisibility(View.VISIBLE);
            cover.setImageBitmap(series.getCover());
        }
        title.setText(series.title);
        scraper.setText(series.scraper.hostName);
        /* If not every chapter of the series has been read */
        if (!series.isRead)
            readIcon.setVisibility(View.GONE);
        else
            readIcon.setVisibility(View.VISIBLE);
        /* If not every chapter of the series has been saved locally */
        if (!series.isLocal)
            localIcon.setVisibility(View.GONE);
        else
            localIcon.setVisibility(View.VISIBLE);
        return listItem;
    }
}