package luphi.bouyomi.adapters;

import android.os.AsyncTask;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;

import java.lang.ref.WeakReference;
import java.util.List;

import luphi.bouyomi.Core;
import luphi.bouyomi.Page;
import luphi.bouyomi.R;

public class StitcherReaderAdapter extends ArrayAdapter<Page> {
    private static class PageWaitTask extends PagerReaderAdapter.PageWaitTask {
        @Override
        protected void onPostExecute(Void v) {
            if (isCancelled())
                return;
            mProgressBar.get().setVisibility(View.GONE);
            if (mPage.getImage() != null) {
                mImageView.get().setVisibility(View.VISIBLE);
                mImageView.get().setImageBitmap(mPage.getImage());
            } else if (Core.get().pagerReaderActivity != null) {
                Core.get().stitcherReaderActivity.timedOut = true;
                Core.get().stitcherReaderActivity.refresh();
            }
        }
    }

    private static class BackgroundReadTask extends PageWaitTask {
        @Override
        protected Void doInBackground(PagerReaderAdapter.PageWaitParams... pageWaitParams) {
            if (isCancelled() || (pageWaitParams[0].page == null) ||
                    (pageWaitParams[0].imageView == null) ||
                    (pageWaitParams[0].progressBar == null))
                return null;
            mPage = pageWaitParams[0].page;
            mImageView = new WeakReference<>(pageWaitParams[0].imageView);
            mProgressBar = new WeakReference<>(pageWaitParams[0].progressBar);
            mPage.getImage(); /* Causes Page to read the image from disk, used in on PostExecute */
            return null;
        }
    }

    private List<Page> mPages;
    private static SparseArray<AsyncTask> mTasks = new SparseArray<>();

    public StitcherReaderAdapter(List<Page> pages) {
        super(Core.get().stitcherReaderActivity, R.layout.page_stitcher, pages);
        mPages = pages;
    }

    @NonNull
    @Override
    public View getView(int position, View listItem, @NonNull ViewGroup parent) {
        /* If the list item does not exist (i.e. this is the first instantiation of this list) */
        if (listItem == null)
            listItem = LayoutInflater.from(Core.get().stitcherReaderActivity).inflate(
                    R.layout.page_stitcher, parent, false); /* Inflate the view from XML */
        if ((mPages == null) || (position >= mPages.size()))
            return listItem;
        Page page = mPages.get(position);
        if (page == null) { /* Should be impossible but just in case */
            return listItem;
        }
        ImageView imageView = listItem.findViewById(R.id.page_image);
        ProgressBar progressBar = listItem.findViewById(R.id.page_progress_bar);
        /* If the page image's bitmap is already in memory */
        if (page.getImageImmediate() != null) {
            imageView.setImageBitmap(page.getImageImmediate());
            progressBar.setVisibility(View.GONE);
        }
        /* If the page image is saved locally but not in memory */
        else if (page.isLocal) {
            /* Load the image in a background task so it doesn't slow the UI thread down */
            imageView.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            mTasks.put(position, new BackgroundReadTask().executeOnExecutor(
                    AsyncTask.THREAD_POOL_EXECUTOR,
                    new PagerReaderAdapter.PageWaitParams(page, imageView, progressBar)));
        }
        /* If the page image is still remote and the scraper needs to download it */
        else {
            imageView.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            page.series.scraper.fillPage(page);
            mTasks.put(position, new PageWaitTask().executeOnExecutor(
                    AsyncTask.THREAD_POOL_EXECUTOR,
                    new PagerReaderAdapter.PageWaitParams(page, imageView, progressBar)));
        }
        return listItem;
    }

    public void cancelAll() {
        for (int i = 0; i < mTasks.size(); i++)
            mTasks.get(mTasks.keyAt(i)).cancel(true);
    }
}
