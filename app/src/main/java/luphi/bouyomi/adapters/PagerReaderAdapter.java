/* Copyright (C) 2019 Lucas Philipsen
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see http://www.gnu.org/licenses/.
 */

package luphi.bouyomi.adapters;

import android.graphics.ColorMatrixColorFilter;
import android.os.AsyncTask;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import java.lang.ref.WeakReference;
import java.util.List;

import luphi.bouyomi.Core;
import luphi.bouyomi.Page;
import luphi.bouyomi.PageView;
import luphi.bouyomi.R;
import luphi.bouyomi.Utils;

public class PagerReaderAdapter extends PagerAdapter {
    static class PageWaitParams {
        Page page;
        ImageView imageView;
        View progressBar;

        PageWaitParams(Page p, ImageView i, View b) {
            page = p;
            imageView = i;
            progressBar = b;
        }
    }

    protected static class PageWaitTask extends AsyncTask<PageWaitParams, Void, Void> {
        Page mPage;
        WeakReference<ImageView> mImageView;
        WeakReference<View> mProgressBar;

        @Override
        protected Void doInBackground(PageWaitParams... pageWaitParams) {
            if (isCancelled() || (pageWaitParams[0].page == null) ||
                    (pageWaitParams[0].imageView == null) ||
                    (pageWaitParams[0].progressBar == null))
                return null;
            mPage = pageWaitParams[0].page;
            mImageView = new WeakReference<>(pageWaitParams[0].imageView);
            mProgressBar = new WeakReference<>(pageWaitParams[0].progressBar);
            long ticks = System.currentTimeMillis();
            /* While 15 seconds have not yet passed */
            while ((System.currentTimeMillis() - ticks) <= 15000) {
                if (isCancelled())
                    return null;
                /* If the page_pager was found */
                if (mPage.getImage() != null) {
                    /* If the preferences dictate that page_pager images should be saved to disk as */
                    /* they're downloaded for reading and this series is in the library */
                    if (Core.get().getPreference("save_pages_while_reading").equals("true") &&
                            Core.get().library.contains(mPage.series))
                        Core.get().downloader.writePageToDisk(mPage);
                    return null;
                }
                try {
                    Thread.sleep(50); /* Sleep 50 milliseconds */
                } catch (Exception e) {
                    if (!(e instanceof InterruptedException))
                        Utils.Log.e("*ReaderAdapter", "PageWaitTask error", e);
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            if (isCancelled())
                return;
            /* Remove the progress bar view whether the image downloaded or not */
            mProgressBar.get().setVisibility(View.GONE);
            /* If the page_pager downloaded successfully */
            if (mPage.getImage() != null) {
                /* Display the image view and set it to display the page_pager */
                mImageView.get().setVisibility(View.VISIBLE);
                mImageView.get().setImageBitmap(mPage.getImage());
            } else if (Core.get().pagerReaderActivity != null) {
                /* Set a flag indicating a timeout occurred so the reader knows to display the */
                /* retry button */
                Core.get().pagerReaderActivity.timedOut = true;
                Core.get().pagerReaderActivity.refresh();
            }
        }
    }

    private List<Page> mPages;
    private SparseArray<View> mViews = new SparseArray<>();
    private int mCurrentPosition = 0;
    private static SparseArray<AsyncTask> mTasks = new SparseArray<>();

    public PagerReaderAdapter(List<Page> pages) {
        mPages = pages;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public int getCount() {
        if (mPages == null)
            return 0;
        return mPages.size();
    }

    @NonNull
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View view = LayoutInflater.from(Core.get().pagerReaderActivity).inflate(
                R.layout.page_pager, container, false); /* Inflate the view from XML */
        mViews.put(position, view);
        if ((mPages == null) || (position >= mPages.size()) || (mPages.get(position) == null))
            return view;
        Page page = mPages.get(position);
        container.addView(view);
        ImageView imageView = view.findViewById(R.id.page_image);
        ProgressBar progressBar = view.findViewById(R.id.page_progress_bar);
        /* If the user's preferences indicate the page_pager should have all color values inverted */
        if (Core.get().getPreference("invert_page_color").equals("true")) {
            /* Set the color matrix such that black becomes white, etc. */
            imageView.setColorFilter(new ColorMatrixColorFilter(
                    new float[]{-1.0f, 0, 0, 0, 255, /* red = -red */
                            0, -1.0f, 0, 0, 255,     /* green = -green */
                            0, 0, -1.0f, 0, 255,     /* blue = -blue */
                            0, 0, 0, 1.0f, 0}));
        }
        /* If the page_pager image is already in memory or can be read immediately */
        if (page.getImage() != null) {
            /* Hide the progress bar but display the image */
            progressBar.setVisibility(View.GONE);
            imageView.setVisibility(View.VISIBLE);
            imageView.setImageBitmap(page.getImage());
        } else {
            /* Hide the image view but display the progress bar */
            imageView.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            /* Begin downloading the page_pager in the background */
            page.series.scraper.fillPage(page);
            /* Create a task to wait for the download to complete and, once complete, set the */
            /* image view and remove the progress bar */
            mTasks.put(position, new PageWaitTask().executeOnExecutor(
                    AsyncTask.THREAD_POOL_EXECUTOR,
                    new PageWaitParams(page, imageView, progressBar)));
        }
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object view) {
        container.removeView((View) view);
        if (mTasks.get(position) != null) {
            /* If the preferences do NOT dictate that pages should be saved as they're downloaded */
            if (!Core.get().getPreference("save_pages_while_reading").equals("true"))
                mTasks.get(position).cancel(true); /* Let it wait and save the page_pager */
            mTasks.delete(position);
        }
    }

    @Override
    public void setPrimaryItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        /* Track the index of the current page_pager so it can be used to grab current PageView */
        mCurrentPosition = position;
        super.setPrimaryItem(container, position, object);
    }

    public PageView getCurrentPageView() {
        if (mViews.get(mCurrentPosition) == null)
            return null;
        return mViews.get(mCurrentPosition).findViewById(R.id.page_image);
    }

    public void cancelAll() {
        for (int i = 0; i < mTasks.size(); i++)
            mTasks.get(mTasks.keyAt(i)).cancel(true);
    }
}
