/* Copyright (C) 2019 Lucas Philipsen
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see http://www.gnu.org/licenses/.
 */

package luphi.bouyomi.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.text.SimpleDateFormat;
import java.util.Locale;

import luphi.bouyomi.Chapter;
import luphi.bouyomi.Core;
import luphi.bouyomi.R;

public class UpdatesAdapter extends ArrayAdapter<Chapter> {
    public UpdatesAdapter() {
        super(Core.get().mainActivity.getApplicationContext(), R.layout.list_item_update,
                Core.get().updatesList);
    }

    @NonNull
    @Override
    public View getView(int position, View listItem, @NonNull ViewGroup parent) {
        /* If the list item does not exist (i.e. this is the first instantiation of this list) */
        if (listItem == null)
            listItem = LayoutInflater.from(Core.get().mainActivity.getApplicationContext()).inflate(
                    R.layout.list_item_update, parent, false); /* Inflate the list item */
        if (position >= Core.get().updatesList.size())
            return listItem;
        Chapter chapter = Core.get().updatesList.get(position);
        ImageView cover = listItem.findViewById(R.id.update_item_cover);
        TextView seriesTitle = listItem.findViewById(R.id.update_item_series_title),
                chapterTitle = listItem.findViewById(R.id.update_item_chapter_title),
                date = listItem.findViewById(R.id.update_item_date);
        cover.setImageBitmap(chapter.series.getCover());
        seriesTitle.setText(chapter.series.title);
        chapterTitle.setText(chapter.title);
        /* "E" corresponds to the day of the week */
        date.setText(new SimpleDateFormat("EEEE", Locale.getDefault()).format(chapter.date));
        return listItem;
    }
}