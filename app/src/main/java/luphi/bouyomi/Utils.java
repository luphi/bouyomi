package luphi.bouyomi;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import androidx.core.app.NotificationCompat;

import java.util.List;

import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;

import luphi.bouyomi.activities.MainActivity;

public class Utils {
    private static NotificationCompat.Builder mNotificationBuilder;
    private static int mOpenGLMaxTextureDimension = 0;

    public static class Log {
        public static void e(String tag, String message) {
            e(tag, message, null);
        }

        public static void e(String tag, String message, Exception e) {
            if (BuildConfig.DEBUG) {
                if (e != null)
                    android.util.Log.e(tag, message, e);
                else
                    android.util.Log.e(tag, message);
            }
        }

        static void w(String tag, String message) {
            w(tag, message, null);
        }

        static void w(String tag, String message, Exception e) {
            if (BuildConfig.DEBUG) {
                if (e != null)
                    android.util.Log.w(tag, message, e);
                else
                    android.util.Log.w(tag, message);
            }
        }

        public static void d(String tag, String message) {
            if (BuildConfig.DEBUG) {
                android.util.Log.d(tag, message);
            }
        }
    }

    static int numLocalPages(Chapter chapter) {
        if (chapter == null)
            return 0;
        int count = 0;
        for (Page page : chapter.getPages()) {
            if (page.isLocal)
                count += 1;
        }
        return count;
    }

    static boolean areAllChaptersLocal(Series series) {
        if (series == null)
            return false;
        for (Chapter chapter : series.getChapters()) {
            if (!chapter.isLocal)
                return false;
        }
        return true;
    }

    public static boolean areAllChaptersRead(Series series) {
        if (series == null)
            return false;
        for (Chapter chapter: series.getChapters()) {
            if (!chapter.isRead)
                return false;
        }
        return true;
    }

    public static void showUpdateNotification(List<Chapter> newChapters) {
        if (!Core.get().getPreference("updates_show_notification").equals("true") ||
                (Core.get().mainActivity == null) || newChapters.isEmpty())
            return;
        if (mNotificationBuilder == null) {
            Intent intent = new Intent(Core.get().mainActivity, MainActivity.class);
            intent.putExtra("notification", 0);
            PendingIntent pendingIntent = PendingIntent.getActivity(Core.get().mainActivity, 0,
                    intent, PendingIntent.FLAG_UPDATE_CURRENT);
            mNotificationBuilder = new NotificationCompat.Builder(Core.get().mainActivity,
                    "bouyomi_updates");
            mNotificationBuilder.setContentIntent(pendingIntent);
            mNotificationBuilder.setSmallIcon(R.drawable.ic_notification);
            mNotificationBuilder.setContentTitle("New chapter(s)");
            mNotificationBuilder.setPriority(NotificationCompat.PRIORITY_DEFAULT);
            mNotificationBuilder.setAutoCancel(true);
            mNotificationBuilder.setOnlyAlertOnce(true);
        }
        for (Chapter chapter : newChapters) {
            mNotificationBuilder.setContentText(chapter.series.title + " - " + chapter.title);
            NotificationManager notificationManager = (NotificationManager)
                    Core.get().mainActivity.getSystemService(Context.NOTIFICATION_SERVICE);
            if (notificationManager != null) {
                notificationManager.notify(1, mNotificationBuilder.build());
            }
        }
    }

    static int getOpenGLMaxTextureDimension() {
        /* If the value hasn't been set yet, it must be set now */
        if (mOpenGLMaxTextureDimension == 0) {
            /* Notes on texture sizes: When bitmaps are placed into a view, they use an OpenGL */
            /* texture.  These textures have limited buffer sizes which vary by device.  Android */
            /* requires that the maximum texture dimension must be at least 2048 pixels but 2048 */
            /* is not preferred.  In testing, 4096 was acceptable but 2048 produced clearly */
            /* pixelated images for some series. As a result, EGL will be probed for OpenGL's */
            /* maximum.  This process requires an OpenGL/EGL context.  Despite how it may look, */
            /* the following routine is the method (for getting a context and the maximum) with */
            /* the smallest footprint. */
            EGL10 egl = (EGL10) EGLContext.getEGL();
            EGLDisplay display = egl.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
            int[] version = new int[2]; /* Required but ignored */
            egl.eglInitialize(display, version);
            int[] totalConfigurations = new int[1]; /* Number of configurations to cycle through */
            egl.eglGetConfigs(display, null, 0, totalConfigurations);
            EGLConfig[] configurationsList = new EGLConfig[totalConfigurations[0]];
            egl.eglGetConfigs(display, configurationsList, totalConfigurations[0],
                    totalConfigurations); /* Get the list of actual configurations */
            int[] textureSize = new int[1];
            int maximumTextureSize = 0;
            /* Iterate through all the configurations to search for a maximum */
            for (int i = 0; i < totalConfigurations[0]; i++) {
                egl.eglGetConfigAttrib(display, configurationsList[i], EGL10.EGL_MAX_PBUFFER_HEIGHT,
                        textureSize); /* Height/width doesn't matter, max resolutions are square */
                if (maximumTextureSize < textureSize[0])
                    maximumTextureSize = textureSize[0];
            }
            egl.eglTerminate(display);
            /* Compare EGL's reported maximum vs. Android's required minimum allowable maximum */
            mOpenGLMaxTextureDimension = Math.max(maximumTextureSize, 2048);
        }
        return mOpenGLMaxTextureDimension;
    }
}
