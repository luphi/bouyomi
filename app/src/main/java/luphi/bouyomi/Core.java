/* Copyright (C) 2019 Lucas Philipsen
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see http://www.gnu.org/licenses/.
 */

package luphi.bouyomi;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;

import androidx.core.app.ActivityCompat;
import androidx.preference.PreferenceManager;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import luphi.bouyomi.activities.MainActivity;
import luphi.bouyomi.activities.PagerReaderActivity;
import luphi.bouyomi.activities.StitcherReaderActivity;
import luphi.bouyomi.scrapers.MangaBatScraper;
import luphi.bouyomi.scrapers.MangaEdenScraper;
import luphi.bouyomi.scrapers.MangaTownScraper;
import luphi.bouyomi.scrapers.MangapandaScraper;
import luphi.bouyomi.scrapers.MangareaderScraper;

public class Core {
    private static Core mInstance = new Core();
    private NetworkReceiver mNetworkReceiver = new NetworkReceiver();
    private UpdateTimer updateTimer = new UpdateTimer();
    private Map<String, String> mPreferences = new HashMap<>();
    private Map<String, Scraper> mScrapers = new HashMap<>();
    private Scraper mDefaultScraper;
    private boolean mDisclaimerAgreement = false;
    private boolean mDisclaimerShown = false;
    private CatalogWriteTask mCatalogTask = null;
    private boolean mWriteCatalogAgain = false;
    private int mCatalogFailedWrites = 0;

    /* Memory ejection restoration variables */
    private int mBookmarkedSeriesHash = 0;
    private int mBookmarkedChapterHash = 0;
    private Page mBookmarkedPage = null;

    /* XML parsing variables */
    private List<Chapter> mCatalogDownloadQueue = new LinkedList<>();

    /* Exposed, single-instance members */
    public final List<Series> library = new LinkedList<>();
    public boolean isLibraryLoading = true;
    public MainActivity mainActivity = null;
    public PagerReaderActivity pagerReaderActivity = null;
    public StitcherReaderActivity stitcherReaderActivity = null;
    public Downloader downloader = new Downloader();
    public Series currentSeries = new Series();
    public boolean isCurrentSeriesAvailable = true;
    public Chapter currentChapter = new Chapter();
    public List<Series> searchResults = new LinkedList<>();
    public boolean searchComplete = true;
    public List<Chapter> updatesList = new LinkedList<>();
    public List<Chapter> downloadList = new LinkedList<>();

    private Core() {
        mScrapers.put("Manga Eden", new MangaEdenScraper());
        mScrapers.put("Mangapanda", new MangapandaScraper());
        mScrapers.put("Mangareader", new MangareaderScraper());
        mScrapers.put("MangaTown", new MangaTownScraper());
        mScrapers.put("MangaBat", new MangaBatScraper());
    }

    public static Core get() {
        return mInstance;
    }

    public void init() {
        PreferenceManager.setDefaultValues(mainActivity.getApplicationContext(), R.xml.preferences,
                false);
        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        mainActivity.registerReceiver(mNetworkReceiver, intentFilter);
        if (Build.VERSION.SDK_INT >= 26) {
            NotificationChannel notificationChannel = new NotificationChannel("bouyomi_updates",
                    "Updates", NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager notificationManager = (NotificationManager)
                    mainActivity.getSystemService(Context.NOTIFICATION_SERVICE);
            if (notificationManager != null)
                notificationManager.createNotificationChannel(notificationChannel);
        }
        /* Store the preferences in this object to avoid any form of context-based issues */
        readPreferences();
        /* If any of the list preferences have an unexpected default value */
        if (!mScrapers.containsKey(getPreference("scraper")) ||
                (!getPreference("reader_type").equals("Pager") &&
                        !getPreference("reader_type").equals("Stitcher")) ||
                (!getPreference("reader_orientation").equals("Portrait") &&
                        !getPreference("reader_orientation").equals("Landscape"))) {
            Utils.Log.w("Core", "mScrapers.containsKey(" + getPreference("scraper") + ") = " +
                    mScrapers.containsKey(getPreference("scraper")));
            Utils.Log.w("Core", "getPreference(reader_type) = \"" +
                    getPreference("reader_type") + "\"");
            Utils.Log.w("Core", "getPreference(reader_orientation) = \"" +
                    getPreference("reader_orientation") + "\"");
            Utils.Log.w("Core", "getPreference(scraper) = \"" +
                    getPreference("scraper") + "\"");
            /* The preferences should be populated with the first index of their respective */
            /* value arrays but they've been seen with the assigned value "1" (e.g. when using */
            /* the preferences to clear app data) rather than their respective strings so */
            /* defaults should be forced here if any are an unexpected value */
            SharedPreferences.Editor sharedPreferencesEditor =
                    PreferenceManager.getDefaultSharedPreferences(mainActivity).edit();
            sharedPreferencesEditor.putString("scraper", "Mangapanda");
            sharedPreferencesEditor.putString("reader_type", "Pager");
            sharedPreferencesEditor.putString("reader_orientation", "Portrait");
            sharedPreferencesEditor.apply();
            readPreferences(); /* Load the new preferences into the local map */
        }
        mDefaultScraper = mScrapers.get(getPreference("scraper"));
    }

    public Scraper getDefaultScraper() {
        if (mDefaultScraper == null)
            mDefaultScraper = mScrapers.get("Mangapanda");
        return mDefaultScraper;
    }

    public void unregisterListeners() {
        if (mainActivity != null)
            mainActivity.unregisterReceiver(mNetworkReceiver);
    }

    public String getPreference(String key) {
        if (mPreferences.containsKey(key))
            return mPreferences.get(key);
        return "";
    }

    public void readCatalog() {
        /* If the library has already been loaded (likely by the reader activity) */
        if (!library.isEmpty()) {
            return;
        }
        File file = new File(Environment.getExternalStorageDirectory(), ".bouyomi" +
                File.separator + "disclaimer");
        mDisclaimerAgreement = file.exists(); /* The user already agreed if the file exists */
        if (!mDisclaimerAgreement)
            showDisclaimer();
        if (!canReadFromDisk()) {
            Core.get().isLibraryLoading = false;
            return; /* Impossible to read the catalog; give up */
        }
        try {
            File path = new File(Environment.getExternalStorageDirectory(), ".bouyomi");
            file = new File(path, "catalog.xml");
            if (!path.exists() || !file.exists()) {
                Core.get().isLibraryLoading = false;
                return;
            }
            FileInputStream inputStream = new FileInputStream(file);
            CatalogReadTask catalogReadTask = new CatalogReadTask();
            catalogReadTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
                    new CatalogReadParam(inputStream, mCatalogDownloadQueue));
        } catch (Exception e) {
            Core.get().isLibraryLoading = false;
            /* A FileNotFoundException is expected when the app is first run, so... */
            if (!(e instanceof FileNotFoundException))
                Utils.Log.e("Core", "Failed to open catalog.xml for reading", e);
        }
    }

    public void writeCatalog() {
        writeCatalog(false);
    }

    public void writeCatalogLite() {
        synchronized (library) {
            /* If there's nothing to write */
            if (library.isEmpty())
                return;
        }
        if (mCatalogTask != null)
            mWriteCatalogAgain = true;
        else {
            mCatalogTask = new CatalogWriteTask();
            mCatalogTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, true); /* write XML */
        }
    }

    public void addToLibrary(Series series) {
        addToLibrary(series, true);
        if (mainActivity != null)
            mainActivity.refreshLibraryFragment();
    }

    @SuppressWarnings("ConstantConditions")
    public void clearApplicationData() {
        deleteAllSeries();
        updatesList.clear();
        mainActivity.refreshUpdatesFragment();
        downloader.clear();
        mainActivity.refreshDownloadQueueFragment();
        deleteRecursively(new File(Environment.getExternalStorageDirectory(), ".bouyomi"));
        if ((mainActivity != null) && (mainActivity.getApplicationContext() != null)) {
            if (mainActivity.getApplicationContext().getSystemService(Context.ACTIVITY_SERVICE)
                    != null)
                ((ActivityManager) mainActivity.getApplicationContext().getSystemService(
                        Context.ACTIVITY_SERVICE)).clearApplicationUserData();
        }
    }

    public void setScraperByName(String name) {
        if (mScrapers.containsKey(name)) {
            mDefaultScraper = mScrapers.get(name);
            /* Not all hosts have a random series feature and there's no reasonable way to */
            /* implement one, so hide the "Random" drawer if the scraper doesn't support it */
            if (mDefaultScraper != null)
                mainActivity.setRandomDrawerVisible(mDefaultScraper.hasRandom);
        }
    }

    public Scraper getScraperByName(String name) {
        if (mScrapers.containsKey(name))
            return mScrapers.get(name);
        return null;
    }

    public void checkForUpdates() {
        /* If the user's preference forbid updating at the moment */
        if (!canCheckUpdates())
            return;
        synchronized (library) {
            /* Spin off tasks to check all library series for new chapters */
            for (Series series : library)
                series.scraper.checkForUpdates(series);
        }
    }

    public void deleteSeries(Series series) {
        deleteSeries(series, true);
    }

    public void deleteAllSeries() {
        List<Series> toRemove = new LinkedList<>();
        synchronized (library) {
            for (Series series : library) {
                deleteSeries(series, false);
                toRemove.add(series);
            }
            for (Series series : toRemove)
                library.remove(series);
        }
        writeCatalog(true);
        if (mainActivity != null)
            mainActivity.refreshLibraryFragment();
    }

    public void sortUpdatesList() {
        Collections.sort(updatesList, new ChapterDateComparator());
    }

    public void expireUpdatesList() {
        List<Chapter> toRemove = new LinkedList<>();
        for (Chapter chapter : updatesList) {
            /* If the chapter's publication date is greater than a week ago */
            if (((System.currentTimeMillis()) - chapter.date.getTime()) > 604800000)
                toRemove.add(chapter);
        }
        for (Chapter chapter : toRemove)
            updatesList.remove(chapter);
    }

    public void readPreferences() {
        SharedPreferences sharedPreferences = null;
        try {
            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(
                    mainActivity.getApplicationContext());
        } catch (Exception e) {
            Utils.Log.e("Core", "Context misuse while reading preferences", e);
        }
        if (sharedPreferences != null) {
            mPreferences.put("scraper", sharedPreferences.getString("scraper", ""));
            mPreferences.put("reader_type", sharedPreferences.getString("reader_type", ""));
            mPreferences.put("reader_orientation",
                    sharedPreferences.getString("reader_orientation", ""));
            mPreferences.put("use_lossy_compression", Boolean.toString(
                    sharedPreferences.getBoolean("use_lossy_compression", false)));
            mPreferences.put("save_pages_while_reading",
                    Boolean.toString(sharedPreferences.getBoolean("save_pages_while_reading",
                            false)));
            mPreferences.put("updates_check_periodically",
                    Boolean.toString(sharedPreferences.getBoolean("updates_check_periodically",
                            false)));
            mPreferences.put("updates_show_notification",
                    Boolean.toString(sharedPreferences.getBoolean("updates_show_notification",
                            true)));
            mPreferences.put("updates_wifi_only",
                    Boolean.toString(sharedPreferences.getBoolean("updates_wifi_only", true)));
            mPreferences.put("download_wifi_only",
                    Boolean.toString(sharedPreferences.getBoolean("download_wifi_only", true)));
            mPreferences.put("fullscreen",
                    Boolean.toString(sharedPreferences.getBoolean("fullscreen", false)));
            mPreferences.put("invert_page_color",
                    Boolean.toString(sharedPreferences.getBoolean("invert_page_color", false)));
            /* If downloading is (now) permitted */
            if (canDownload())
                downloader.start(); /* Begin downloading */
                /* If downloading is not permitted (anymore) */
            else
                downloader.stop(); /* Stop downloading */
            /* If periodic update checks are (now) permitted */
            if (getPreference("updates_check_periodically").equals("true") && canCheckUpdates())
                updateTimer.start();
                /* If periodic update checks are not permitted (anymore) */
            else
                updateTimer.stop();
        }
    }

    public boolean canDownload() {
        /* If the device has some form of internet connection */
        if (isOnline()) {
            /* Return false in the case that downloading is only permitted on WiFi and this */
            /* device is not currently on WiFi, otherwise return true */
            return (!(!isOnWifi() && getPreference("download_wifi_only").equals("true")));
        }
        /* If the device has no internet connection */
        return false;
    }
    
    public void restoreFromBundle(Bundle bundle) {
        mBookmarkedPage = new Page();
        mBookmarkedPage.source = bundle.getString("page_source");
        mBookmarkedPage.index = bundle.getInt("page_index");
        mBookmarkedPage.isLocal = bundle.getBoolean("page_is_local");
        mBookmarkedPage.series = currentSeries;
        mBookmarkedPage.chapter = currentChapter;
        mBookmarkedPage.series.scraper = getScraperByName(bundle.getString("series_host"));
        mBookmarkedSeriesHash = bundle.getInt("series_hash");
        mBookmarkedChapterHash = bundle.getInt("chapter_hash");
        /* Trick setChapter() into loading this as a series and chapter with only one page_pager */
        currentChapter.getPages().add(mBookmarkedPage);
        currentChapter.series = currentSeries;
        /* Temporarily set some preferences prior to reading the actual application */
        /* preferences once the main activity is created */
        mPreferences.put("fullscreen", bundle.getString("fullscreen"));
        mPreferences.put("reader_orientation", bundle.getString("reader_orientation"));
        mPreferences.put("invert_page_color", bundle.getString("invert_page_color"));
        readCatalog();
        /* readCatalog() will spin off a background task and that task, when it has finished */
        /* reading the catalog, will check the mBookmarkedPage variable and, if it's not null, */
        /* will call setChapter() with the series and chapter matching the set hashes and */
        /* turn to the index of the bookmarked page_pager. For now, setChapter() will load the */
        /* current chapter as if it only contains a single page_pager (mBookmarkedPage). */
    }

    private void writeCatalog(boolean shouldWriteEmpty) {
        /* If there's nothing to write */
        if (library.isEmpty() && !shouldWriteEmpty)
            return;
        /* If the catalog is currently being written */
        if (mCatalogTask != null) {
            /* Set the flag indicating the catalog task should execute at least once more */
            mWriteCatalogAgain = true;
        }
        else {
            mCatalogTask = new CatalogWriteTask();
            mCatalogTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, false); /* full write */
        }
    }

    private void addToLibrary(Series series, boolean shouldWrite) {
        synchronized (library) {
            if ((series == null) || (library.contains(series)))
                return;
            library.add(series);
            for (Chapter chapter : series.getChapters()) {
                /* If the chapter's publication date (on its host) is less than a week ago and */
                /* the chapter is not already in the list */
                if ((((System.currentTimeMillis()) - chapter.date.getTime()) < 604800000) &&
                        !updatesList.contains(chapter))
                    updatesList.add(chapter);
            }
            sortUpdatesList();
            if (mainActivity != null) {
                mainActivity.refreshUpdatesFragment();
                mainActivity.refreshLibraryFragment();
            }
            if (shouldWrite)
                writeCatalog();
        }
    }

    private boolean isOnline() {
        if (mainActivity == null)
            return false;
        ConnectivityManager connectionManager = (ConnectivityManager)
                mainActivity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = null;
        if (connectionManager != null)
            networkInfo = connectionManager.getActiveNetworkInfo();
        return ((networkInfo != null) && networkInfo.isConnected());
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    private boolean isOnWifi() {
        if (mainActivity == null)
            return false;
        ConnectivityManager connectionManager = (ConnectivityManager)
                mainActivity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = null;
        if (connectionManager != null)
            networkInfo = connectionManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return ((networkInfo != null) && networkInfo.isConnected());
    }

    private boolean canCheckUpdates() {
        /* The logic here is the same as canDownload() */
        if (isOnline())
            return (!(!isOnWifi() && getPreference("updates_wifi_only").equals("true")));
        return false;
    }

    private void deleteSeries(Series series, boolean shouldRemoveFromLibraryAndWrite) {
        if (series == null)
            return;
        series.freeCover();
        List<Chapter> toRemove = new LinkedList<>();
        /* Find any chapters in the download list belonging to this series */
        for (Chapter chapter : downloadList) {
            if (chapter.series == series)
                toRemove.add(chapter);
        }
        /* If at least one chapter belonging to this series was found in the download list */
        if (toRemove.size() > 0) {
            /* Remove all such chapters from the download queue */
            for (Chapter chapter : toRemove)
                downloadList.remove(chapter);
            if (mainActivity != null)
                mainActivity.refreshDownloadQueueFragment();
        }
        toRemove.clear();
        /* Find any chapters in the updates list belonging to this series */
        for (Chapter chapter : updatesList) {
            if (chapter.series == series)
                toRemove.add(chapter);
        }
        /* If at least one chapter belonging to this series was found in the recent updates */
        if (toRemove.size() > 0) {
            /* Remove all such chapters from the recent updates */
            for (Chapter chapter : toRemove)
                updatesList.remove(chapter);
            if (mainActivity != null)
                mainActivity.refreshUpdatesFragment();
        }
        toRemove.clear();
        File file = new File(downloader.seriesPath(series)); /* The series' storage directory */
        /* If the series has a directory in local storage */
        if (file.exists()) {
            /* Attempt to remove the series' directory */
            if (!deleteRecursively(file)) { /* Delete the directory and its content */
                /* There's no good automated response to a failed deletion so... */
                Utils.Log.w("Core", "Could not delete series directory for " + series.title);
            }
        }
        /* If the series should be removed from the library of all series and the catalog should */
        /* be written at this time (this condition exists to avoid writing the catalog */
        /* multiple times when the user opts to delete all series) */
        if (shouldRemoveFromLibraryAndWrite) {
            synchronized (library) {
                library.remove(series);
            }
            writeCatalog(true);
            if (mainActivity != null)
                mainActivity.refreshLibraryFragment();
        }
    }

    private static String xmlEncode(String string) {
        if (string == null)
            return "";
        StringBuilder encoded = new StringBuilder();
        for (int i = 0; i < string.length(); i++) {
            char character = string.charAt(i);
            // If the character is greater than the greatest possible ASCII letter value
            if (character > 127) {
                // If the character is recognized as a line terminator, ignore it
                if ((character == 0x0085) || (character == 0x2028) || (character == 0x2029)) {
                    encoded.append("&#10;"); // insert a line feed
                    continue;
                }
                // Insert the appropriate unicode value
                String concatenation = "&#x" + String.format("%04x", (int) character) + ";";
                encoded.append(concatenation);
            } else {
                // Replace a few special characters with their XML equivalents, or append directly
                switch (character) {
                    case 0x0026: // &
                        encoded.append("&amp;");
                        break;
                    case 0x003C: // <
                        encoded.append("&lt;");
                        break;
                    case 0x003E: // >
                        encoded.append("&gt;");
                        break;
                    case 0x0022: // "
                        encoded.append("&quot;");
                        break;
                    case 0x0027: // '
                        encoded.append("&apos;");
                        break;
                    case 0x000A: // line feed
                    case 0x000B: // vertical tab
                    case 0x000C: // form feed
                    case 0x000D: // carriage return
                        encoded.append("&#10;"); // insert a line feed
                        break;
                    default:
                        encoded.append(character);
                        break;
                }
            }
        }
        return encoded.toString().trim();
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    private boolean createAppDirectory() {
        File file = new File(Environment.getExternalStorageDirectory(), ".bouyomi");
        if (!file.exists() && !file.mkdirs()) {
            Utils.Log.w("Core", "Could not create app directory");
            return false;
        }
        return true;
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    private boolean canWriteToDisk() {
        String storageState = Environment.getExternalStorageState();
        /* If the external storage (SD card or local flash storage) is NOT present */
        if (!storageState.equals(Environment.MEDIA_MOUNTED)) {
            Utils.Log.w("Core", "Cannot write catalog: external media is not writable");
            return false; /* It's impossible to write the catalog, give up */
        }
        /* If the Android version is one which requires this app to gain permission from the user */
        /* to write to external storage */
        if (Build.VERSION.SDK_INT >= 23) {
            Activity activity = null;
            if (mainActivity != null)
                activity = mainActivity;
            else if (pagerReaderActivity != null)
                activity = pagerReaderActivity;
            else if (stitcherReaderActivity != null)
                activity = stitcherReaderActivity;
            if (activity == null)
                return false;
            /* If this app does not already have permission to write to external storage */
            if (activity.getApplicationContext().checkSelfPermission(
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                Utils.Log.w("Core", "WRITE_EXTERNAL_STORAGE permission not granted");
                /* Request permission to write to external storage */
                ActivityCompat.requestPermissions(mainActivity,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        return true;
    }

    private boolean canReadFromDisk() {
        String storageState = Environment.getExternalStorageState();
        if (!(storageState.equals(Environment.MEDIA_MOUNTED) ||
                storageState.equals(Environment.MEDIA_MOUNTED_READ_ONLY))) {
            Utils.Log.w("Core", "Cannot read catalog: external media is not readable");
            return false;
        }
        if (Build.VERSION.SDK_INT >= 23) {
            Activity activity = null;
            if (mainActivity != null)
                activity = mainActivity;
            else if (pagerReaderActivity != null)
                activity = pagerReaderActivity;
            else if (stitcherReaderActivity != null)
                activity = stitcherReaderActivity;
            if (activity == null)
                return false;
            /* If this app does not already have permission to read from external storage */
            if (activity.getApplicationContext().checkSelfPermission(
                    android.Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                Utils.Log.w("Core", "READ_EXTERNAL_STORAGE permission not granted");
                ActivityCompat.requestPermissions(mainActivity,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        }
        return true;
    }

    private void writeDisclaimerAgreement() {
        File file = new File(Environment.getExternalStorageDirectory(), ".bouyomi" +
                File.separator + "disclaimer");
        if (!file.exists()) {
            try {
                if (!file.createNewFile())
                    Utils.Log.w("Core", "Could not write disclaimer agreement flag");
            } catch (Exception e) {
                Utils.Log.w("Core", "Error while writing disclaimer agreement flag", e);
            }
        }
    }

    private boolean deleteRecursively(File directory) {
        if (directory != null) {
            File[] content = directory.listFiles();
            if (content != null) {
                for (File file : content) {
                    if (file.isDirectory()) {
                        if (!deleteRecursively(file))
                            return false;
                    } else {
                        if (!file.delete())
                            return false;
                    }
                }
                return directory.delete();
            } else
                return false;
        }
        return true;
    }

    private void showDisclaimer() {
        if ((mainActivity == null) || mDisclaimerShown)
            return;
        mDisclaimerShown = true;
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(mainActivity);
        alertBuilder.setMessage(R.string.disclaimer)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        mDisclaimerAgreement = true;
                        if (!createAppDirectory())
                            return;
                        writeDisclaimerAgreement();
                    }
                });
        AlertDialog alertDialog = alertBuilder.create();
        alertDialog.show();
    }

    private void writeCatalogRun() {
        if ((mainActivity == null) || (mainActivity.getApplicationContext() == null) ||
                !canWriteToDisk() || !createAppDirectory())
            return;
        synchronized (library) {
            for (Series series : library) {
                /* If the series has a cover */
                if (series.getCover() != null) {
                    File path = new File(downloader.seriesPath(series));
                    /* Create the series' directory if it doesn't exist */
                    if (!path.exists() && !path.mkdirs()) {
                        Utils.Log.e("Core", "Could not create directory for series \"" +
                                series.title + "\"");
                        continue;
                    }
                    try {
                        /* Always write the series' cover; it may have changed after an update */
                        /* check (note that PNG is lossless so quality will not be harmed) */
                        FileOutputStream fileOutputStream = new FileOutputStream(
                                Core.get().downloader.coverPath(series));
                        series.getCover().compress(Bitmap.CompressFormat.PNG, 100,
                                fileOutputStream);
                        fileOutputStream.close();
                    } catch (Exception e) {
                        Utils.Log.e("Core", "Failed to write cover for series \"" + series.title +
                                "\"");
                    }
                }
            }
        }
        writeCatalogXmlRun(); /* Write the XML catalog */
    }

    private void writeCatalogXmlRun() {
        if (!canWriteToDisk() || !createAppDirectory())
            return;
        if (mDisclaimerAgreement)
            writeDisclaimerAgreement();
        OutputStreamWriter outputStreamWriter;
        try {
            /* This single XML file will contain the entire list of series, chapters, etc. */
            File file = new File(Environment.getExternalStorageDirectory(), ".bouyomi" +
                    File.separator + "catalog.xml");
            /* If the catalog exists */
            if (file.exists()) {
                /* If deleting the existing catalog fails (in order to write a new one) */
                if (!file.delete()) {
                    Utils.Log.e("Core", "Unable to delete " + file.getAbsolutePath());
                    return; /* It's impossible to write the catalog, give up */
                }
                /* If creating a new catalog fail */
                if (!file.createNewFile()) {
                    Utils.Log.e("Core", "Unable to create " + file.getAbsolutePath());
                    return; /* It's impossible to write the catalog, give up */
                }
            }
            outputStreamWriter = new OutputStreamWriter(new FileOutputStream(file));
        } catch (Exception e) {
            Utils.Log.e("Core", "Failed to open catalog for writing", e);
            /* There may have been some sort of race condition that caused an error.  As a */
            /* possible solution, attempt to write the catalog again.  If this is found to cause */
            /* infinite loops, it may need a more proper solution. */
            mCatalogFailedWrites += 1;
            /* If writing the catalog has not failed an excessive number of times */
            if (mCatalogFailedWrites < 5)
                mWriteCatalogAgain = true; /* Try to write again */
            return;
        }
        StringBuilder stringBuilder = new StringBuilder();
        try {
            stringBuilder.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
            /* The <catalog> tag does not serve any actual purpose; XML requires that there be */
            /* only one root element which is the role <catalog> plays */
            stringBuilder.append("<catalog>\n");
            synchronized (library) {
                for (Series series : library) {
                    String line = "  <series title=\"" + xmlEncode(series.title) + "\" " +
                            "summary=\"" + (series.summary != null ? xmlEncode(series.summary) :
                            "") + "\" source=\"" + xmlEncode(series.source) + "\" " +
                            "host=\"" + series.scraper.hostName + "\" " +
                            "read=\"" + (series.isRead ? "yes" : "no") + "\" " +
                            "local=\"" + (series.isLocal ? "yes" : "no") + "\">\n";
                    stringBuilder.append(line);
                    for (Chapter chapter : series.getChapters()) {
                        line = "    <chapter title=\"" + xmlEncode(chapter.title) + "\" " +
                                "number=\"" + chapter.chapterNumber + "\" " +
                                "source=\"" + xmlEncode(chapter.source) + "\" " +
                                "date=\"" + chapter.date.getTime() + "\" " +
                                "numlocal=\"" + chapter.numLocalPages + "\" " +
                                "local=\"" + (chapter.isLocal ? "yes" : "no") + "\" " +
                                "read=\"" + (chapter.isRead ? "yes" : "no") + "\" " +
                                "index=\"" + chapter.currentIndex + "\" " +
                                "queued=\"" + (downloadList.contains(chapter) ? "yes" : "no") +
                                "\">\n";
                        stringBuilder.append(line);
                        for (Page page : chapter.getPages()) {
                            line = "      <page_pager source=\"" + xmlEncode(page.source) + "\" " +
                                    "local=\"" + (page.isLocal ? "yes" : "no") + "\" " +
                                    "index=\"" + page.index + "\" />\n";
                            stringBuilder.append(line);
                        }
                        stringBuilder.append("    </chapter>\n");
                    }
                    stringBuilder.append("  </series>\n");
                }
            }
            stringBuilder.append("</catalog>\n");
            outputStreamWriter.write(stringBuilder.toString());
            outputStreamWriter.close();
            mCatalogFailedWrites = 0;
        } catch (Exception e) {
            Utils.Log.e("Core", "Error while writing catalog", e);
            mCatalogFailedWrites += 1;
            /* If writing the catalog has not failed an excessive number of times */
            if (mCatalogFailedWrites < 5)
                mWriteCatalogAgain = true; /* Try to write again */
        }
    }

    private static class CatalogParser extends DefaultHandler {
        private List<Chapter> mCatalogDownloadQueue;
        private Series mXmlCurrentSeries = null;
        private Chapter mXmlCurrentChapter = null;
        List<Series> library = new LinkedList<>();

        CatalogParser(List<Chapter> catalogDownloadQueue) {
            mCatalogDownloadQueue = catalogDownloadQueue;
        }

        @Override
        public void startElement(String namespaceURI, String localName, String qName,
                                 Attributes atts) {
            switch (localName) {
                case "series":
                    mXmlCurrentSeries = new Series();
                    mXmlCurrentSeries.title = atts.getValue("title");
                    mXmlCurrentSeries.summary = atts.getValue("summary");
                    mXmlCurrentSeries.source = atts.getValue("source");
                    mXmlCurrentSeries.scraper = Core.get().getScraperByName(atts.getValue("host"));
                    mXmlCurrentSeries.isRead = atts.getValue("read").equals("yes");
                    mXmlCurrentSeries.isLocal = atts.getValue("local").equals("yes");
                    break;
                case "chapter":
                    mXmlCurrentChapter = new Chapter();
                    mXmlCurrentChapter.series = mXmlCurrentSeries;
                    mXmlCurrentChapter.title = atts.getValue("title");
                    mXmlCurrentChapter.chapterNumber = Float.parseFloat(atts.getValue("number"));
                    mXmlCurrentChapter.numLocalPages = Integer.parseInt(atts.getValue("numlocal"));
                    mXmlCurrentChapter.isLocal = atts.getValue("local").equals("yes");
                    mXmlCurrentChapter.isRead = atts.getValue("read").equals("yes");
                    mXmlCurrentChapter.source = atts.getValue("source");
                    mXmlCurrentChapter.date = new Date(Long.parseLong(atts.getValue("date")));
                    mXmlCurrentChapter.currentIndex = Integer.parseInt(atts.getValue("index"));
                    mXmlCurrentSeries.addChapter(mXmlCurrentChapter);
                    /* If the chapter's publication date (on its host) is less than a week ago */
                    /* and the list does not already contain the chapter */
                    if ((((System.currentTimeMillis()) - Long.parseLong(atts.getValue("date"))) <
                            604800000) && !Core.get().updatesList.contains(mXmlCurrentChapter))
                        Core.get().updatesList.add(mXmlCurrentChapter);
                    /* If the page_pager was in the download queue */
                    if (atts.getValue("queued").equals("yes"))
                        mCatalogDownloadQueue.add(mXmlCurrentChapter);
                    break;
                case "page_pager":
                    Page page = new Page();
                    page.series = mXmlCurrentSeries;
                    page.chapter = mXmlCurrentChapter;
                    page.isLocal = atts.getValue("local").equals("yes");
                    page.source = atts.getValue("source");
                    page.index = Integer.parseInt(atts.getValue("index"));
                    mXmlCurrentChapter.addPage(page);
                    break;
            }
        }

        @Override
        public void endElement(String namespaceURI, String localName, String qName) {
            switch (localName) {
                case "series":
                    library.add(mXmlCurrentSeries);
                    mXmlCurrentSeries = null;
                    break;
                case "chapter":
                    mXmlCurrentSeries.addChapter(mXmlCurrentChapter);
                    mXmlCurrentChapter = null;
                    break;
            }
        }
    }

    private class NetworkReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            /* This routine is called when the system's network status changes (e.g. loss of */
            /* connection, switch to mobile data, switch to WiFi, etc.) */
            if (canDownload())
                downloader.start();
            else
                downloader.stop();
            if (getPreference("updates_check_periodically").equals("true") && canCheckUpdates())
                updateTimer.start();
            else
                updateTimer.stop();
        }
    }

    private class CatalogReadParam {
        InputStream inputStream;
        List<Chapter> catalogDownloadQueue;

        CatalogReadParam(InputStream i, List<Chapter> c) {
            inputStream = i;
            catalogDownloadQueue = c;
        }
    }

    private static class CatalogReadTask extends AsyncTask<CatalogReadParam, Void, Void> {
        private List<Chapter> mCatalogDownloadQueue;
        private List<Series> mLibrary = null;

        @Override
        protected Void doInBackground(CatalogReadParam... catalogReadParams) {
            mCatalogDownloadQueue = catalogReadParams[0].catalogDownloadQueue;
            mCatalogDownloadQueue.clear();
            try {
                SAXParserFactory spf = SAXParserFactory.newInstance();
                SAXParser saxParser = spf.newSAXParser();
                XMLReader reader = saxParser.getXMLReader();
                CatalogParser catalogParser = new CatalogParser(mCatalogDownloadQueue);
                reader.setContentHandler(catalogParser);
                reader.parse(new InputSource(catalogReadParams[0].inputStream));
                mLibrary = catalogParser.library;
            } catch (ParserConfigurationException e) {
                Utils.Log.e("Core", "SAX parse error", e);
            } catch (SAXException e) {
                Utils.Log.e("Core", "SAX error", e);
            } catch (IOException e) {
                Utils.Log.e("Core", "SAX parse I/O error", e);
            } catch (NullPointerException e) {
                Utils.Log.e("Core", "Null pointer during load", e);
            }
            /* Series hold their chapters in descending order whereas the download queue is in */
            /* ascending order.  This list will have been populated in descending order so it */
            /* must be reversed before re-queueing the chapters. */
            Collections.reverse(mCatalogDownloadQueue);
            /* If the bookmark is not null, the reader activity is being restored from an */
            /* "ejection from memory" case.  The goal now, with the catalog fully read, is to set */
            /* the reader activity's chapter with the appropriate chapter and update its current */
            /* index with that of the bookmark. */
            if (Core.get().mBookmarkedPage != null) {
                /* Find the series by comparing hashes */
                for (Series series : mLibrary) {
                    if (series.hashCode() == Core.get().mBookmarkedSeriesHash) {
                        /* Find the chapter by comparing hashes */
                        for (Chapter chapter : series.getChapters()) {
                            if (chapter.hashCode() == Core.get().mBookmarkedChapterHash) {
                                chapter.currentIndex = Core.get().mBookmarkedPage.index;
                                Core.get().currentSeries = series;
                                Core.get().currentChapter = chapter;
                                break;
                            }
                        }
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            if (mLibrary != null) {
                for (Series series : mLibrary)
                    Core.get().addToLibrary(series, false);
                for (Chapter chapter : mCatalogDownloadQueue)
                    Core.get().downloader.addChapterToQueue(chapter);
                if (!mCatalogDownloadQueue.isEmpty() && Core.get().canDownload())
                    Core.get().downloader.start();
                if (Core.get().mBookmarkedPage != null) {
                    /* If the image is already local, either because it was on disk or as a */
                    /* result of scraper finishing a download before the catalog read finished */
                    if (Core.get().mBookmarkedPage.getImage() != null) {
                        /* Copy the image into the page_pager/chapter to be set */
                        Core.get().currentChapter.getPages().get(
                                Core.get().mBookmarkedPage.index).setImage(
                                Core.get().mBookmarkedPage.getImage());
                    }
                    if (Core.get().pagerReaderActivity != null)
                        Core.get().pagerReaderActivity.setChapter(Core.get().currentChapter);
                    if (Core.get().stitcherReaderActivity != null)
                        Core.get().stitcherReaderActivity.setChapter(Core.get().currentChapter);
                    /* Reset the bookmark variables to indicate there's nothing to restore */
                    Core.get().mBookmarkedPage = null;
                    Core.get().mBookmarkedChapterHash = 0;
                    Core.get().mBookmarkedSeriesHash = 0;
                }
            }
            Core.get().isLibraryLoading = false;
            if (Core.get().mainActivity != null)
                Core.get().mainActivity.refreshLibraryFragment();
        }
    }

    private static class CatalogWriteTask extends AsyncTask<Boolean, Void, Void> {
        boolean mIsLite;

        @Override
        public Void doInBackground(Boolean... isLite) {
            mIsLite = isLite[0];
            /* Execute the catalog write routine in this background thread */
            if (mIsLite)
                Core.get().writeCatalogXmlRun();
            else
                Core.get().writeCatalogRun();
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            /* If something triggered a catalog write while the catalog was being written */
            if (Core.get().mWriteCatalogAgain) {
                /* Write the catalog again to ensure nothing is missed */
                Core.get().mWriteCatalogAgain = false;
                Core.get().mCatalogTask = new CatalogWriteTask();
                Core.get().mCatalogTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mIsLite);
            } else
                Core.get().mCatalogTask = null;
        }
    }

    private class ChapterDateComparator implements Comparator<Chapter> {
        public int compare(Chapter one, Chapter two) {
            /* If the two chapters are in the same series and were released on the same day */
            if ((one.series == two.series) && (one.date.equals(two.date)))
                return ((int) (two.chapterNumber - one.chapterNumber)); /* Compare by chapter # */
            /* Return the difference in seconds of the chapters' publication dates. */
            /* The conversion to seconds is necessary to avoid wraparound. */
            return ((int) (two.date.getTime() / 1000) - (int) (one.date.getTime() / 1000));
        }
    }
}
