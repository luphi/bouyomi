/* Copyright (C) 2019 Lucas Philipsen
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see http://www.gnu.org/licenses/.
 */

package luphi.bouyomi;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.File;

public class Page {
    private Bitmap mImage = null;

    public Series series = null; /* The series to which the page_pager/chapter belongs */
    public Chapter chapter = null; /* The chapter to which the page_pager belongs */
    public String source = null; /* Either a file name or URL */
    public int index = 0; /* Index of the page_pager within the chapter, starting at 0 */
    public boolean isLocal = false; /* True if the image is saved to disk */

    public void setImage(Bitmap image) {
        mImage = image;
    }

    public Bitmap getImage() {
        if (mImage != null)
            return mImage;
        if (isLocal) {
            try {
                mImage = BitmapFactory.decodeFile(source);
                if (mImage == null) {
                    Utils.Log.w("Page", "Failed to load page_pager at source \"" + source + "\"");
                    if (!new File(source).delete())
                        Utils.Log.e("Page", "Failed to delete erroneous page_pager image \"" + source + "\"");
                    /* Whether the delete is successful or not, the page_pager will be marked as */
                    /* non-local in order to overwrite the image */
                    source = null;
                    isLocal = false;
                    return null;
                }
                return mImage;
            } catch (Exception e) {
                Utils.Log.e("Page", "Could not load local page_pager at \"" + source + "\"", e);
            }
        }
        /* In rare cases, the image for this page_pager may have already been downloaded but failed to */
        /* be recorded.  For example, if the system or user killed this app while the downloader */
        /* was running or the reader activity was open, the image would have been written without */
        /* the page_pager being updated in the catalog. */
        String wouldBeSource = Core.get().downloader.pagePath(this);
        File wouldBeFile = new File(wouldBeSource);
        /* If there is a file at the expected location */
        if (wouldBeFile.exists()) {
            try {
                /* Attempt to load the file and set the page_pager to local if no exception is thrown */
                mImage = BitmapFactory.decodeFile(wouldBeSource);
                if (mImage == null) {
                    Utils.Log.w("Page", "Failed to load would-be source \"" + wouldBeSource + "\"");
                    if (!wouldBeFile.delete())
                        Utils.Log.e("Page", "Failed to delete erroneous would-be page_pager image \"" +
                                wouldBeSource + "\" - this will likely cause an infinite loop");
                    return null;
                }
                Core.get().downloader.writePageToDisk(this);
                return mImage;
            } catch (Exception e) {
                Utils.Log.w("Page", "A file exists at the expected location for page_pager index " +
                        index + "of chapter \"" + chapter.title + "\" but it could be not loaded",
                        e);
            }
        }
        return null;
    }

    public Bitmap getImageImmediate() {
        /* Return the image, or lack thereof, immediately without any attempt to load.  This */
        /* method is intended to be used by the stitcher reader in order to avoid loading */
        /* images with the UI thread.  This avoids obvious stuttering with the ListView, which */
        /* does not manage its View getting as smoothly as pager reader's ViewPager. */
        return mImage;
    }

    void freeImage() {
        if (mImage != null) {
            mImage.recycle();
            mImage = null;
        }
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Page))
            return false;
        return ((index == ((Page) other).index) && (chapter == ((Page) other).chapter));
    }
}
