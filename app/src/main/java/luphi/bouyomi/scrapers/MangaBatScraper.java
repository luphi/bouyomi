/* Copyright (C) 2019 Lucas Philipsen
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see http://www.gnu.org/licenses/.
 */

package luphi.bouyomi.scrapers;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.text.Html;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import luphi.bouyomi.Chapter;
import luphi.bouyomi.Core;
import luphi.bouyomi.Page;
import luphi.bouyomi.Scraper;
import luphi.bouyomi.Series;
import luphi.bouyomi.Utils;

import static luphi.bouyomi.Utils.showUpdateNotification;

public class MangaBatScraper extends Scraper {
    private static String mHostName;
    private static String mSearchUrl;
    private static Pattern mSearchResultPattern;
    private static Pattern mCoverPattern;
    private static Pattern mChapterLinkPattern1;
    private static Pattern mChapterLinkPattern2;
    private static Pattern mDatePattern;
    private static Pattern mPagePattern;

    private static class ChapterParams {
        Series series;
        boolean isUpdateCheck;
        List<String> markup;

        ChapterParams(Series s, boolean i, List<String> m) {
            series = s;
            isUpdateCheck = i;
            markup = m;
        }
    }

    private static class CoverParams {
        Series series;
        List<String> markup;

        CoverParams(Series s, List<String> m) {
            series = s;
            markup = m;
        }
    }

    private static class SearchTask extends AsyncTask<String, Void, Void> {
        private List<Series> mList = new LinkedList<>();

        @Override
        protected Void doInBackground(String... title) {
            if (isCancelled())
                return null;
            List<String> markup = downloadPage(mSearchUrl +
                    title[0].toLowerCase().replace(' ', '_'));
            boolean inSeries = false;
            for (String markupLine : markup) {
                if (inSeries) {
                    Matcher matcher = mSearchResultPattern.matcher(markupLine);
                    if (matcher.find()) {
                        Series series = new Series();
                        series.scraper = Core.get().getScraperByName(mHostName);
                        series.source = Html.fromHtml(matcher.group(1)).toString();
                        series.title = matcher.group(2);
                        mList.add(series);
                        inSeries = false;
                    }
                }
                /* Each search result is placed in a div with these classes */
                else if (markupLine.contains("class=\"update_item list_category\""))
                    inSeries = true;
            }
            Core.get().searchResults.clear();
            Core.get().searchResults.addAll(mList);
            Core.get().searchComplete = true;
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            if (Core.get().mainActivity != null)
                Core.get().mainActivity.refreshSearchFragment();
            mTasks.remove(this);
        }

        @Override
        public void onCancelled() {
            super.onCancelled();
            mTasks.remove(this);
        }
    }

    private static class SeriesTask extends AsyncTask<Series, Void, Void> {
        private Series mSeries = null;
        private List<String> mMarkup = new LinkedList<>();

        @Override
        protected Void doInBackground(Series... series) {
            if (isCancelled())
                return null;
            mSeries = series[0];
            mMarkup = downloadPage(mSeries.source);
            StringBuilder stringBuilder = new StringBuilder();
            boolean inSummary = false;
            for (String markupLine : mMarkup) {
                if (inSummary) {
                    stringBuilder.append(Html.fromHtml(markupLine).toString());
                    /* If the end of the summary has been reached */
                    if (markupLine.contains("</div>")) {
                        mSeries.summary = stringBuilder.toString();
                        return null;
                    }
                }
                /* MangaBat places the series' title, in red, just prior to the summary meaning */
                /* this paragraph element indicates the start of the summary (on the next line) */
                else if (markupLine.contains("<p style=\"color: red;\">"))
                    inSummary = true;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            CoverTask coverTask = new CoverTask();
            mTasks.add(coverTask);
            coverTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
                    new CoverParams(mSeries, mMarkup));
            ChapterListTask chapterListTask = new ChapterListTask();
            mTasks.add(chapterListTask);
            chapterListTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
                    new ChapterParams(mSeries, false, mMarkup));
            Core.get().currentSeries = mSeries;
            mTasks.remove(this);
        }

        @Override
        public void onCancelled() {
            super.onCancelled();
            mTasks.remove(this);
        }
    }

    private static class ChapterListTask extends AsyncTask<ChapterParams, Void, Void> {
        private Series mSeries;
        private List<Chapter> mChapters = new LinkedList<>();
        private List<Chapter> mNewChapters = new LinkedList<>();

        @Override
        protected Void doInBackground(ChapterParams... chapterParams) {
            if (isCancelled())
                return null;
            mSeries = chapterParams[0].series;
            List<String> markup = chapterParams[0].markup;
            /* If spun off from a SeriesTask, the markup will have been passed down.  Otherwise, */
            /* the markup will need to be downloaded now. */
            if (markup == null)
                markup = downloadPage(mSeries.source);
            Chapter chapter = null;
            boolean inChapterList = false;
            for (String markupLine : markup) {
                if (inChapterList) {
                    Matcher matcher = mChapterLinkPattern1.matcher(markupLine);
                    if (matcher.find()) {
                        chapter = new Chapter();
                        chapter.series = mSeries;
                        chapter.source = matcher.group(1);
                        chapter.chapterNumber = Float.parseFloat(matcher.group(3));
                        if (!matcher.group(5).isEmpty()) {
                            chapter.title = "Ch. " + matcher.group(3) + ": " +
                                    Html.fromHtml(matcher.group(5)).toString();
                        }
                        else {
                            chapter.title = "Ch. " + matcher.group(3);
                        }
                    } else {
                        /* Occasionally, some chapters on MangaBat will not contain the chapter */
                        /* number in the title.  In those cases, just assume the chapter number */
                        /* is 0 and grab the rest of the info. */
                        matcher = mChapterLinkPattern2.matcher(markupLine);
                        if (matcher.find()) {
                            chapter = new Chapter();
                            chapter.series = mSeries;
                            chapter.source = matcher.group(1);
                            chapter.chapterNumber = 0.0f;
                            chapter.title = Html.fromHtml(matcher.group(2)).toString();
                        }
                    }
                    matcher = mDatePattern.matcher(markupLine);
                    if ((chapter != null) && matcher.find()) {
                        try {
                            /* E.g. Jun-15-16 */
                            chapter.date = new SimpleDateFormat("MMM-dd-yy",
                                    Locale.getDefault()).parse(matcher.group(1));
                        } catch (Exception e) {
                            chapter.date = new Date(0); /* January 1, 1970 */
                            Utils.Log.e("MangaBatScraper", "Failed to parse chapter date", e);
                        }
                        /* If the series does not contain this chapter */
                        if (!mSeries.getChapters().contains(chapter)) {
                            mChapters.add(chapter);
                            /* If this is an update check and the series does not currently */
                            /* contain this chapter in its list */
                            if (chapterParams[0].isUpdateCheck)
                                mNewChapters.add(chapter);
                        }
                    }
                    /* This class is used following the end of the chapter list */
                    if (markupLine.contains("truyen_info_cm"))
                        return null;
                } else if (markupLine.equals("<div class=\"chapter-list\">"))
                    inChapterList = true;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void p) {
            /* Attempt to add all chapters found.  addChapter() will verify the chapter is not */
            /* already present before adding it. */
            for (Chapter chapter : mChapters)
                mSeries.addChapter(chapter);
            /* If there are chapters to display in the updates list and (possibly) notify the */
            /* user of */
            if (!mNewChapters.isEmpty()) {
                for (Chapter chapter : mNewChapters) {
                    if (!Core.get().updatesList.contains(chapter))
                        Core.get().updatesList.add(chapter);
                }
                Core.get().sortUpdatesList();
                if (Core.get().mainActivity != null)
                    Core.get().mainActivity.refreshUpdatesFragment();
                showUpdateNotification(mNewChapters);
                Core.get().writeCatalogLite();
            }
            mSeries.sortChapters();
            if (Core.get().mainActivity != null) {
                Core.get().mainActivity.refreshLibraryFragment();
                Core.get().mainActivity.refreshSeriesFragment();
            }
            if (Core.get().pagerReaderActivity != null)
                Core.get().pagerReaderActivity.refresh();
            if (Core.get().stitcherReaderActivity != null)
                Core.get().stitcherReaderActivity.refresh();
            mTasks.remove(this);
        }

        @Override
        public void onCancelled() {
            super.onCancelled();
            mTasks.remove(this);
        }
    }

    private static class ChapterTask extends AsyncTask<Chapter, Void, Void> {
        private Chapter mChapter = null;
        private List<Page> mPages = new LinkedList<>();

        @Override
        protected Void doInBackground(Chapter... c) {
            if (isCancelled())
                return null;
            mChapter = c[0];
            List<String> markup = downloadPage(mChapter.source);
            boolean inPages = false;
            for (String markupLine : markup) {
                if (inPages) {
                    String[] elements = markupLine.split("(?=<)|(?<=>)");
                    for (String element : elements) {
                        Matcher matcher = mPagePattern.matcher(element);
                        if (matcher.find()) {
                            Page page = new Page();
                            page.series = mChapter.series;
                            page.chapter = mChapter;
                            page.source = matcher.group(1);
                            page.index = Integer.parseInt(matcher.group(2)) - 1;
                            mPages.add(page);
                        }
                    }
                    /* All pages are located on the same line, so this chapter is done */
                    return null;
                } else if (markupLine.contains("vung_doc") || markupLine.contains("vung-doc"))
                    inPages = true;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            if (mChapter != null) {
                for (Page page : mPages)
                    mChapter.addPage(page);
            }
            if (Core.get().pagerReaderActivity != null)
                Core.get().pagerReaderActivity.refresh();
            if (Core.get().stitcherReaderActivity != null)
                Core.get().stitcherReaderActivity.refresh();
            Core.get().writeCatalog();
            mTasks.remove(this);
        }

        @Override
        public void onCancelled() {
            super.onCancelled();
            mTasks.remove(this);
        }
    }

    private static class CoverTask extends AsyncTask<CoverParams, Void, Void> {
        private Bitmap mImage = null;
        private Series mSeries = null;
        private String mCoverSource = "";

        @Override
        protected Void doInBackground(CoverParams... coverParams) {
            if (isCancelled())
                return null;
            mSeries = coverParams[0].series;
            List<String> markup = coverParams[0].markup;
            /* If the series' overview page_pager HTML was not handed down */
            if (markup == null)
                markup = downloadPage(mSeries.source);
            for (String markupLine : markup) {
                /* MangaBat applies the class "info_image_manga" to the cover's image element */
                if (markupLine.contains("info_image_manga")) {
                    Matcher matcher = mCoverPattern.matcher(markupLine);
                    if (matcher.find()) {
                        mCoverSource = matcher.group(1);
                        break;
                    }
                }
            }
            /* If the series has no cover */
            if ((mCoverSource != null) && !mCoverSource.isEmpty())
                mImage = downloadImage(mCoverSource);
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            if ((mSeries != null) && (mImage != null)) {
                mSeries.setCover(mImage);
                if ((Core.get().currentSeries == mSeries) && (Core.get().mainActivity != null))
                    Core.get().mainActivity.refreshSeriesFragment();
            }
            mTasks.remove(this);
        }

        @Override
        public void onCancelled() {
            super.onCancelled();
            mTasks.remove(this);
        }
    }

    private static class PageTask extends AsyncTask<Page, Void, Void> {
        private Page mPage = null;
        private Bitmap mImage = null;

        @Override
        protected Void doInBackground(Page... p) {
            if (isCancelled())
                return null;
            mPage = p[0];
            if (mPage != null)
                mImage = downloadImage(mPage.source);
            return null;
        }

        @Override
        public void onPostExecute(Void v) {
            if ((mPage != null) && (mImage != null))
                mPage.setImage(mImage);
            mTasks.remove(this);
        }

        @Override
        public void onCancelled() {
            super.onCancelled();
            mTasks.remove(this);
        }
    }

    /**********************************************************************************************/

    public MangaBatScraper() {
        hostName = "MangaBat";
        hasRandom = false;
        mHostName = hostName;
        mSearchUrl = "https://mangabat.com/search_manga/";
        mSearchResultPattern = Pattern.compile("<a rel=\"nofollow\" href=\"([^\"]+)\" " +
                "title=\"([^\"]+)\">");
        mCoverPattern = Pattern.compile("<img class=\"info_image_manga\" " +
                "onerror=\"javascript:this.src='//mangabat.com/themes/home/images/404-avatar.png'" +
                ";\" src=\"([^\"]+)\" alt=\"[^\"]+\" title=\"[^\"]+\">");
        mChapterLinkPattern1 = Pattern.compile("<span><a (?:rel=\"nofollow\" )?href=\"([^\"]+)\" " +
                "title=\".+\">.*Chapter ((\\d+(\\.\\d+)?)(?:\\s?: )?([^<]*))</a></span>");
        mChapterLinkPattern2 = Pattern.compile("<span><a (?:rel=\"nofollow\" )?href=\"([^\"]+)\" " +
                "title=\"[^\"]+\">([^<]+)</a></span>");
        mDatePattern = Pattern.compile("<span>([^<]+)</span>");
        mPagePattern = Pattern.compile("<img src=\"([^\"]+?(\\d+)\\.jpg)\".*");
    }

    public void fillSearchFragment(String title) {
        SearchTask task = new SearchTask();
        mTasks.add(task);
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, title);
    }

    public void fillSeriesFragment(Series series) {
        SeriesTask task = new SeriesTask();
        mTasks.add(task);
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, series);
    }

    public void fillSeriesFragmentRandom() { /* Unsupported */ }

    public void fillChapter(Chapter chapter) {
        if (chapter.getPages().isEmpty()) {
            ChapterTask task = new ChapterTask();
            mTasks.add(task);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, chapter);
        }
    }

    public void fillPage(Page page) {
        if (page.getImage() == null) {
            PageTask task = new PageTask();
            mTasks.add(task);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, page);
        }
    }

    public void checkForUpdates(Series series) {
        CoverTask coverTask = new CoverTask();
        mTasks.add(coverTask);
        coverTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new CoverParams(series, null));
        ChapterListTask chapterListTask = new ChapterListTask();
        mTasks.add(chapterListTask);
        chapterListTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new ChapterParams(series,
                true, null));
    }
}
