/* Copyright (C) 2019 Lucas Philipsen
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see http://www.gnu.org/licenses/.
 */

package luphi.bouyomi.scrapers;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.text.Html;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import luphi.bouyomi.Chapter;
import luphi.bouyomi.Core;
import luphi.bouyomi.Page;
import luphi.bouyomi.Scraper;
import luphi.bouyomi.Series;
import luphi.bouyomi.Utils;

import static luphi.bouyomi.Utils.showUpdateNotification;

public class MangaTownScraper extends Scraper {
    private static String mHostName;
    private static String mSearchUrl;
    private static Pattern mSearchPattern;
    private static Pattern mTitlePattern;
    private static Pattern mSummaryPattern1;
    private static Pattern mSummaryPattern2;
    private static Pattern mSummaryPattern3;
    private static Pattern mCoverPattern;
    private static Pattern mChapterLinkPattern;
    private static Pattern mChapterTitlePattern;
    private static Pattern mChapterTitleDatePattern1;
    private static Pattern mChapterTitleDatePattern2;
    private static Pattern mChapterDatePattern;
    private static Pattern mPageLinkPattern;
    private static Pattern mPageImagePattern;

    private static class ChapterListParams {
        Series series;
        boolean isUpdateCheck;
        List<String> markup;

        ChapterListParams(Series s, boolean i, List<String> m) {
            series = s;
            isUpdateCheck = i;
            markup = m;
        }
    }

    private static class CoverParams {
        Series series;
        List<String> markup;

        CoverParams(Series s, List<String> m) {
            series = s;
            markup = m;
        }
    }

    private static class SearchTask extends AsyncTask<String, Void, Void> {
        private List<Series> mList = new LinkedList<>();

        @Override
        protected Void doInBackground(String... title) {
            if (isCancelled())
                return null;
            List<String> markup = downloadPage(mSearchUrl +
                    title[0].toLowerCase().replace(" ", "%20"));
            for (String markupLine : markup) {
                /* MangaTown uses a very basic link for series, with just an HREF and title.  The */
                /* line will also begin with an A element without anything preceding. */
                if (markupLine.startsWith("<a href")) {
                    Matcher matcher = mSearchPattern.matcher(markupLine);
                    if (matcher.find()) {
                        Series series = new Series();
                        series.scraper = Core.get().getScraperByName(mHostName);
                        series.title = Html.fromHtml(matcher.group(2)).toString();
                        if (matcher.group(1).startsWith("https:"))
                            series.source = matcher.group(1);
                        else
                            series.source = "https:" + matcher.group(1);
                        mList.add(series);
                    }
                }
            }
            Core.get().searchResults.clear();
            Core.get().searchResults.addAll(mList);
            Core.get().searchComplete = true;
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            if (Core.get().mainActivity != null)
                Core.get().mainActivity.refreshSearchFragment();
            mTasks.remove(this);
        }

        @Override
        public void onCancelled() {
            super.onCancelled();
            mTasks.remove(this);
        }
    }

    private static class SeriesTask extends AsyncTask<Series, Void, Void> {
        private Series mSeries = null;
        private List<String> mMarkup = new LinkedList<>();
        private boolean mIsAvailable = true;

        @Override
        protected Void doInBackground(Series... series) {
            if (isCancelled())
                return null;
            mSeries = series[0];
            mMarkup = downloadPage(mSeries.source);
            boolean inSummary = false;
            StringBuilder stringBuilder = new StringBuilder();
            for (String markupLine : mMarkup) {
                /* If the series has a notice stating the series has been licensed and is */
                /* unavailable on Manga Town */
                if (markupLine.contains("has been licensed, it is not available")) {
                    mIsAvailable = false;
                    /* The "has been licensed" notice appears after all the other important */
                    /* information so exiting now is fine */
                    return null;
                }
                /* "title-top" denotes the series title */
                else if (markupLine.startsWith("<h1 class=\"title-top\">")) {
                    Matcher matcher = mTitlePattern.matcher(markupLine);
                    if (matcher.find())
                        mSeries.summary = Html.fromHtml(matcher.group(1)).toString();
                }
                /* The series full summary is (typically) hidden but shown with a "MORE" button */
                /* and that full, hidden text starts on a line beginning with a hidden span */
                /* element.  Note: even series with empty summaries have this element. */
                else if (markupLine.startsWith("<span id=\"show\" style=\"display: none;\">")) {
                    Matcher matcher = mSummaryPattern1.matcher(markupLine);
                    /* If the summary is contained on just one line */
                    if (matcher.find())
                        stringBuilder.append(Html.fromHtml(matcher.group(1)).toString());
                        /* If the summary spans at least two lines */
                    else {
                        matcher = mSummaryPattern2.matcher(markupLine);
                        if (matcher.find())
                            stringBuilder.append(Html.fromHtml(matcher.group(1)).toString());
                        inSummary = true;
                    }
                } else if (inSummary) {
                    Matcher matcher = mSummaryPattern3.matcher(markupLine);
                    /* If the summary ends on this line */
                    if (matcher.find()) {
                        stringBuilder.append(" ");
                        stringBuilder.append(Html.fromHtml(matcher.group(1)).toString());
                        inSummary = false;
                    } else {
                        stringBuilder.append(" ");
                        stringBuilder.append(Html.fromHtml(markupLine).toString());
                    }
                }
            }
            mSeries.summary = Html.fromHtml(stringBuilder.toString()).toString();
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            CoverTask coverTask = new CoverTask();
            mTasks.add(coverTask);
            coverTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
                    new CoverParams(mSeries, mMarkup));
            if (mIsAvailable) {
                ChapterListTask chapterListTask = new ChapterListTask();
                mTasks.add(chapterListTask);
                chapterListTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
                        new ChapterListParams(mSeries, false, mMarkup));
            }
            Core.get().currentSeries = mSeries;
            if (!mIsAvailable)
                Core.get().isCurrentSeriesAvailable = false;
            if (Core.get().mainActivity != null)
                Core.get().mainActivity.refreshSeriesFragment();
            mTasks.remove(this);
        }

        @Override
        public void onCancelled() {
            super.onCancelled();
            mTasks.remove(this);
        }
    }

    private static class CoverTask extends AsyncTask<CoverParams, Void, Void> {
        private Bitmap mImage = null;
        private Series mSeries;

        @Override
        protected Void doInBackground(CoverParams... coverParams) {
            if (isCancelled())
                return null;
            mSeries = coverParams[0].series;
            List<String> markup = coverParams[0].markup;
            String coverSource = "";
            /* If the series' overview page_pager HTML was not handed down */
            if (markup == null)
                markup = downloadPage(mSeries.source);
            for (String markupLine : markup) {
                /* MangaTown has a fallback cover image that pulls in an onError event.  This and */
                /* the actual cover source are on the same line/tag. */
                if (markupLine.contains("manga_cover.jpg")) {
                    Matcher matcher = mCoverPattern.matcher(markupLine);
                    if (matcher.find()) {
                        coverSource = matcher.group(1);
                        break;
                    }
                }
            }
            /* If the series has a cover */
            if ((coverSource != null) && !coverSource.isEmpty())
                mImage = downloadImage(coverSource);
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            if ((mSeries != null) && (mImage != null)) {
                mSeries.setCover(mImage);
                if ((Core.get().currentSeries == mSeries) && (Core.get().mainActivity != null))
                    Core.get().mainActivity.refreshSeriesFragment();
            }
            mTasks.remove(this);
        }

        @Override
        public void onCancelled() {
            super.onCancelled();
            mTasks.remove(this);
        }
    }

    private static class ChapterListTask extends AsyncTask<ChapterListParams, Void, Void> {
        private Series mSeries;
        private List<Chapter> mChapters = new LinkedList<>();
        private List<Chapter> mNewChapters = new LinkedList<>();

        @Override
        protected Void doInBackground(ChapterListParams... chapterParams) {
            if (isCancelled())
                return null;
            mSeries = chapterParams[0].series;
            List<String> markup = chapterParams[0].markup;
            Chapter chapter = null;
            boolean inChapterList = false, inChapter = false;
            StringBuilder stringBuilder = new StringBuilder();
            /* If spun off from a SeriesTask, the markup will have been passed down.  Otherwise, */
            /* the markup will need to be downloaded now. */
            if (markup == null)
                markup = downloadPage(mSeries.source);
            for (String markupLine : markup) {
                if (inChapterList) {
                    markupLine = markupLine.trim();
                    if (markupLine.contains("</ul>"))
                        return null;
                    else if (inChapter) {
                        /* This pattern corresponds with the main chapter title: series name + */
                        /* chapter number */
                        Matcher matcher = mChapterTitlePattern.matcher(markupLine);
                        if (matcher.find()) {
                            /* group(1) = "<series title> <chapter number>", */
                            /* group(2) = chapter number */
                            String title = "Ch. " + Html.fromHtml(matcher.group(2)).toString();
                            stringBuilder.append(title);
                            chapter.chapterNumber = Float.parseFloat(matcher.group(2));
                        }
                        /* This pattern corresponds to lines containing a sub-title of some sort */
                        /* as well as the chapter's upload date.  The sub-title may be the */
                        /* official chapter title or a volume number. */
                        matcher = mChapterTitleDatePattern1.matcher(markupLine);
                        if (matcher.find()) {
                            String title = ": " + Html.fromHtml(matcher.group(1)).toString();
                            stringBuilder.append(title);
                            chapter.date = parseDateString(matcher.group(2));
                        }
                        /* This pattern corresponds to lines containing two sub-titles (volume */
                        /* number and official chapter title) as well as the date */
                        matcher = mChapterTitleDatePattern2.matcher(markupLine);
                        if (matcher.find()) {
                            String title = ": " + Html.fromHtml(matcher.group(1)).toString() +
                                    " " + Html.fromHtml(matcher.group(2)).toString();
                            stringBuilder.append(title);
                            chapter.date = parseDateString(matcher.group(3));
                        }
                        /* This pattern corresponds to a line with just the date, without any */
                        /* sort of sub-title */
                        matcher = mChapterDatePattern.matcher(markupLine);
                        if (matcher.find())
                            chapter.date = parseDateString(matcher.group(1));
                        if (markupLine.equals("</li>")) {
                            chapter.title = stringBuilder.toString();
                            /* If the series does not contain this chapter */
                            if (!mSeries.getChapters().contains(chapter)) {
                                mChapters.add(chapter);
                                /* If this is an update check and the series does not currently */
                                /* contain this chapter in its list */
                                if (chapterParams[0].isUpdateCheck)
                                    mNewChapters.add(chapter);
                            }
                            inChapter = false;
                        }
                    } else if (markupLine.startsWith("<a href")) {
                        chapter = new Chapter();
                        chapter.series = mSeries;
                        stringBuilder = new StringBuilder(); /* in lieu of a clear() method */
                        Matcher matcher = mChapterLinkPattern.matcher(markupLine);
                        if (matcher.find()) {
                            if (!matcher.group(1).startsWith("https:"))
                                chapter.source = "https:" + matcher.group(1);
                            else
                                chapter.source = matcher.group(1);
                        }
                        inChapter = true;
                    }
                } else if (markupLine.contains("class=\"chapter_list\""))
                    inChapterList = true;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void p) {
            /* Attempt to add all chapters found.  addChapter() will verify the chapter is not */
            /* already present before adding it. */
            for (Chapter chapter : mChapters)
                mSeries.addChapter(chapter);
            /* If there are chapters to display in the updates list and (possibly) notify the */
            /* user of */
            if (!mNewChapters.isEmpty()) {
                for (Chapter chapter : mNewChapters) {
                    if (!Core.get().updatesList.contains(chapter))
                        Core.get().updatesList.add(chapter);
                }
                Core.get().sortUpdatesList();
                if (Core.get().mainActivity != null)
                    Core.get().mainActivity.refreshUpdatesFragment();
                showUpdateNotification(mNewChapters);
                Core.get().writeCatalogLite();
            }
            mSeries.sortChapters();
            if (Core.get().mainActivity != null) {
                Core.get().mainActivity.refreshLibraryFragment();
                Core.get().mainActivity.refreshSeriesFragment();
            }
            if (Core.get().pagerReaderActivity != null)
                Core.get().pagerReaderActivity.refresh();
            if (Core.get().stitcherReaderActivity != null)
                Core.get().stitcherReaderActivity.refresh();
            mTasks.remove(this);
        }

        @Override
        public void onCancelled() {
            super.onCancelled();
            mTasks.remove(this);
        }
    }

    private static class ChapterTask extends AsyncTask<Chapter, Void, Void> {
        private Chapter mChapter = null;
        private List<Page> mPages = new LinkedList<>();

        @Override
        protected Void doInBackground(Chapter... c) {
            if (isCancelled())
                return null;
            mChapter = c[0];
            List<String> markup = downloadPage(mChapter.source);
            boolean inOptions = false;
            for (String markupLine : markup) {
                if (inOptions) {
                    Matcher matcher = mPageLinkPattern.matcher(markupLine);
                    if (matcher.find()) {
                        Page page = new Page();
                        page.series = mChapter.series;
                        page.chapter = mChapter;
                        if (matcher.group(1).startsWith("http:"))
                            page.source = matcher.group(1);
                        else
                            page.source = "http:" + matcher.group(1);
                        page.index = Integer.parseInt(matcher.group(2)) - 1;
                        mPages.add(page);
                    } else if (markupLine.contains("</select>"))
                        return null;
                } else if (markupLine.equals("<select " +
                        "onchange=\"javascript:location.href=this.value;\">"))
                    inOptions = true;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            if (mChapter != null) {
                for (Page page : mPages)
                    mChapter.addPage(page);
            }
            if (Core.get().pagerReaderActivity != null)
                Core.get().pagerReaderActivity.refresh();
            if (Core.get().stitcherReaderActivity != null)
                Core.get().stitcherReaderActivity.refresh();
            Core.get().writeCatalog();
            mTasks.remove(this);
        }

        @Override
        public void onCancelled() {
            super.onCancelled();
            mTasks.remove(this);
        }
    }

    private static class PageTask extends AsyncTask<Page, Void, Void> {
        private Page mPage;
        private Bitmap mImage = null;

        @Override
        protected Void doInBackground(Page... p) {
            if (isCancelled())
                return null;
            mPage = p[0];
            List<String> markup = downloadPage(mPage.source);
            for (String markupLine : markup) {
                /* The first img element on the page_pager will be the page_pager's image */
                if (markupLine.startsWith("<img src")) {
                    Matcher matcher = mPageImagePattern.matcher(markupLine);
                    if (matcher.find())
                        mImage = downloadImage(matcher.group(1));
                    /* The rest of the markup can be ignored */
                    return null;
                }
            }
            return null;
        }

        @Override
        public void onPostExecute(Void v) {
            if (mImage != null)
                mPage.setImage(mImage);
            mTasks.remove(this);
        }

        @Override
        public void onCancelled() {
            super.onCancelled();
            mTasks.remove(this);
        }
    }

    private static Date parseDateString(String string) {
        if (string == null)
            return null;
        Date date = new Date();
        if (string.equals("Today") || string.equals("Yesterday")) {
            /* Set the date to the beginning of the current day */
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(date);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);
            /* If the chapter was released yesterday, move the date back one day */
            if (string.contains("Yesterday"))
                calendar.add(Calendar.DAY_OF_WEEK, -1);
            date = calendar.getTime();
        } else {
            try {
                date = new SimpleDateFormat("MMM dd, yyyy", Locale.getDefault()).parse(string);
            } catch (Exception e) {
                Utils.Log.e("MangaTownScraper", "Error while parsing date string", e);
            }
        }
        return date;
    }

    public MangaTownScraper() {
        hostName = "MangaTown";
        hasRandom = false;
        mHostName = hostName;
        mSearchUrl = "https://www.mangatown.com/search.php?name=";
        /* A note on regex: MangaTown has a tendency to insert additional spaces seemingly at */
        /* random in unpredictable places.  \s is used extensively for this reason. */
        mSearchPattern = Pattern.compile("<a\\s+href=\"([^\"]+)\"\\s+title=\"(.+)\">");
        mTitlePattern = Pattern.compile("<h1\\s+class=\"title-top\">(.+)</h1>");
        mSummaryPattern1 = Pattern.compile("<span\\s+id=\"show\"\\s+style=\"display: none;\">(.+)" +
                "<a class=\"more\"\\s+href=\"javascript:;\"\\s+" +
                "onclick=\"cut_hide\\(\\)\">HIDE</a>(?:</span>)?");
        mSummaryPattern2 = Pattern.compile("<span\\s+id=\"show\"\\s+style=\"display: none;\">(.+)");
        mSummaryPattern3 = Pattern.compile("(.+)<a\\s+class=\"more\"\\s+href=\"javascript:;\"\\s+" +
                "onclick=\"cut_hide\\(\\)\">HIDE</a>(?:</span>)?");
        mCoverPattern = Pattern.compile("<img\\s+src=\"(.+)\"\\s+" +
                "onerror=\"this.src='//www.mangatown.com/media/images/manga_cover.jpg'\"\\s*/>");
        mChapterLinkPattern = Pattern.compile("<a\\s+href=\"([^\"]+)\"(?:\\s+name=\".*\")?>");
        mChapterTitlePattern = Pattern.compile("(.+\\s+(\\d+(\\.\\d+)?))\\s+</a>");
        mChapterTitleDatePattern1 = Pattern.compile("<span>(.+)</span>\\s+" +
                "<span\\s+class=\"time\">(.+)</span>");
        mChapterTitleDatePattern2 = Pattern.compile("<span>(.+)</spans>\\s+<span>(.+)</span>\\s+" +
                "<span\\s+class=\"time\">(.+)</span>");
        mChapterDatePattern = Pattern.compile("<span\\s+class=\"time\">(.+)</span>");
        mPageLinkPattern = Pattern.compile("<option\\s+value=\"([^\"]+)\"(?: " +
                "selected=\"selected\")?>(\\d+)</option>");
        mPageImagePattern = Pattern.compile("<img\\s+src=\"(.+)\"\\s+width=\"\\d+\"\\s+" +
                "id=\"image\"\\s+alt=\".+\"\\s*/>");
    }

    public void fillSearchFragment(String title) {
        SearchTask task;

        task = new SearchTask();
        mTasks.add(task);
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, title);
    }

    public void fillSeriesFragment(Series series) {
        SeriesTask task = new SeriesTask();
        mTasks.add(task);
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, series);
    }

    public void fillSeriesFragmentRandom() { /* Unsupported */ }

    public void fillChapter(Chapter chapter) {
        if (chapter.getPages().isEmpty()) {
            ChapterTask task = new ChapterTask();
            mTasks.add(task);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, chapter);
        }
    }

    public void fillPage(Page page) {
        if (page.getImage() == null) {
            PageTask task = new PageTask();
            mTasks.add(task);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, page);
        }
    }

    public void checkForUpdates(Series series) {
        CoverTask coverTask = new CoverTask();
        mTasks.add(coverTask);
        coverTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new CoverParams(series, null));
        ChapterListTask chapterListTask = new ChapterListTask();
        mTasks.add(chapterListTask);
        chapterListTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new ChapterListParams(
                series, true, null));
    }
}
