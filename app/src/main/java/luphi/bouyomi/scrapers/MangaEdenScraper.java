/* Copyright (C) 2019 Lucas Philipsen
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see http://www.gnu.org/licenses/.
 */

package luphi.bouyomi.scrapers;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.text.Html;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import luphi.bouyomi.Chapter;
import luphi.bouyomi.Core;
import luphi.bouyomi.Page;
import luphi.bouyomi.Scraper;
import luphi.bouyomi.Series;
import luphi.bouyomi.Utils;

import static luphi.bouyomi.Utils.showUpdateNotification;

public class MangaEdenScraper extends Scraper {
    private static String mHostName;
    private static String mSearchUrl;
    private static String mRandomUrl;
    private static String mHostUrl;
    private static Pattern mSearchPattern;
    private static Pattern mChapterPageSelectorPattern;
    private static Pattern mSummaryPattern;
    private static Pattern mSummaryPattern2;
    private static Pattern mCoverPattern;
    private static Pattern mChapterLinkPattern;
    private static Pattern mChapterTitlePattern;
    private static Pattern mChapterTitlePattern2;
    private static Pattern mChapterDatePattern;
    private static Pattern mPagePattern;

    private static class ChapterParams {
        Chapter chapter;
        boolean isAvailabilityCheck;

        ChapterParams(Chapter c, boolean a) {
            chapter = c;
            isAvailabilityCheck = a;
        }
    }

    private static class ChapterListParams {
        Series series;
        boolean isUpdateCheck;
        List<String> markup;

        ChapterListParams(Series s, boolean i, List<String> m) {
            series = s;
            isUpdateCheck = i;
            markup = m;
        }
    }

    private static class CoverParams {
        Series series;
        List<String> markup;

        CoverParams(Series s, List<String> m) {
            series = s;
            markup = m;
        }
    }

    private static class SearchTask extends AsyncTask<String, Void, Void> {
        private List<Series> mList = new LinkedList<>();

        @Override
        protected Void doInBackground(String... title) {
            if (isCancelled())
                return null;
            List<String> markup = downloadPage(mSearchUrl +
                    title[0].toLowerCase().replace(' ', '+'));
            for (String markupLine : markup) {
                /* Manga Eden uses one of two classes applied to A elements: "closedManga" or */
                /* "openManga" with them, respectively, referring to completed and ongoing series */
                /* If the line contains such a link */
                if (markupLine.contains("closedManga") || markupLine.contains("openManga")) {
                    Matcher matcher = mSearchPattern.matcher(markupLine);
                    if (matcher.find()) {
                        Series series = new Series();
                        series.scraper = Core.get().getScraperByName(mHostName);
                        series.title = Html.fromHtml(matcher.group(2)).toString();
                        if (matcher.group(1).startsWith(mHostUrl))
                            series.source = matcher.group(1);
                        else
                            series.source = mHostUrl + matcher.group(1);
                        mList.add(series);
                    }
                }
            }
            Core.get().searchResults.clear();
            Core.get().searchResults.addAll(mList);
            Core.get().searchComplete = true;
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            if (Core.get().mainActivity != null)
                Core.get().mainActivity.refreshSearchFragment();
            mTasks.remove(this);
        }

        @Override
        public void onCancelled() {
            super.onCancelled();
            mTasks.remove(this);
        }
    }

    private static class SeriesTask extends AsyncTask<Series, Void, Void> {
        private Series mSeries = null;
        private List<String> mMarkup = new LinkedList<>();

        @Override
        protected Void doInBackground(Series... series) {
            if (isCancelled())
                return null;
            mSeries = series[0];
            boolean inSummary = false;
            StringBuilder stringBuilder = new StringBuilder();
            mMarkup = downloadPage(mSeries.source);
            for (String markupLine : mMarkup) {
                /* If the summary either continues onto the next line or if a closing tag is */
                /* contained in this line */
                if (inSummary) {
                    if (markupLine.equals("</h2>"))
                        inSummary = false;
                    else if (markupLine.endsWith("</h2>")) {
                        Matcher matcher = mSummaryPattern2.matcher(markupLine);
                        if (matcher.find())
                            stringBuilder.append(matcher.group(1));
                        inSummary = false;
                    } else {
                        stringBuilder.append(" ");
                        stringBuilder.append(markupLine);
                    }
                }
                /* If this line is the beginning over, or entirety of, the series' summary and */
                /* there is no closing tag (indicating the summary is empty) */
                else if (markupLine.contains("id=\"mangaDescription\"") &&
                        !markupLine.contains("</h2>")) {
                    Matcher matcher = mSummaryPattern.matcher(markupLine);
                    if (matcher.find())
                        stringBuilder.append(matcher.group(1));
                    inSummary = true;
                }
            }
            mSeries.summary = Html.fromHtml(stringBuilder.toString()).toString();
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            CoverTask coverTask = new CoverTask();
            mTasks.add(coverTask);
            coverTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
                    new CoverParams(mSeries, mMarkup));
            ChapterListTask chapterListTask = new ChapterListTask();
            mTasks.add(chapterListTask);
            chapterListTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
                    new ChapterListParams(mSeries, false, mMarkup));
            Core.get().currentSeries = mSeries;
            if (Core.get().mainActivity != null)
                Core.get().mainActivity.refreshSeriesFragment();
            mTasks.remove(this);
        }

        @Override
        public void onCancelled() {
            super.onCancelled();
            mTasks.remove(this);
        }
    }

    private static class ChapterTask extends AsyncTask<ChapterParams, Void, Void> {
        private Chapter mChapter = null;
        private boolean mIsAvailabilityCheck = false;
        private boolean mIsAvailable = true;
        private List<Page> mPages = new LinkedList<>();

        @Override
        protected Void doInBackground(ChapterParams... chapterParams) {
            if (isCancelled())
                return null;
            mChapter = chapterParams[0].chapter;
            mIsAvailabilityCheck = chapterParams[0].isAvailabilityCheck;
            if (mChapter == null)
                return null;
            List<String> markup = downloadPage(mChapter.source);
            for (String markupLine : markup) {
                if (!mIsAvailabilityCheck) {
                    /* Manga Eden uses a dropdown list to list each page_pager of the chapter giving its */
                    /* page_pager number and URL of the page_pager (but not the image itself).  This list is on */
                    /* each page_pager of the chapter.  It is also condensed to one line of markup and is */
                    /* given the id "pageSelect" on this line. */
                    if (markupLine.contains("pageSelect")) {
                        /* Split the line up by HTML elements/tags */
                        String[] elements = markupLine.split("(?=<)|(?<=>)");
                        for (String element : elements) {
                            /* If this is an opening option tag */
                            if (element.startsWith("<option")) {
                                Matcher matcher = mChapterPageSelectorPattern.matcher(element);
                                if (matcher.find()) {
                                    Page page = new Page();
                                    page.series = mChapter.series;
                                    page.chapter = mChapter;
                                    if (matcher.group(1).startsWith(mHostUrl))
                                        page.source = matcher.group(1);
                                    else
                                        page.source = mHostUrl + matcher.group(1);
                                    page.index = Integer.parseInt(matcher.group(2)) - 1;
                                    mPages.add(page);
                                }
                            }
                            /* If this is the end of the page_pager list */
                            else if (element.equals("</select>"))
                                return null;
                        }
                    }
                } else /* if (mIsAvailabilityCheck) */ {
                    /* If the following line appears anywhere on this page_pager, no chapters */
                    /* of this series will be displayed */
                    if (markupLine.contains(
                            "We are sorry but this manga has been licensed")) {
                        mIsAvailable = false;
                        /* The rest of the markup can be ignored */
                        return null;
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            if (!mIsAvailabilityCheck) {
                if (mChapter != null) {
                    /* Because the chapter may currently be associated with an adapter, it's best */
                    /* to do the following on the UI thread (onPostExecute is on the UI thread) */
                    for (Page page : mPages)
                        mChapter.addPage(page);
                }
                if (Core.get().pagerReaderActivity != null)
                    Core.get().pagerReaderActivity.refresh();
                if (Core.get().stitcherReaderActivity != null)
                    Core.get().stitcherReaderActivity.refresh();
                Core.get().writeCatalog();
            } else if (/* mIsAvailabilityCheck && */ !mIsAvailable) {
                Core.get().isCurrentSeriesAvailable = false;
                if (Core.get().mainActivity != null)
                    Core.get().mainActivity.refreshSeriesFragment();
            }
            mTasks.remove(this);
        }

        @Override
        public void onCancelled() {
            super.onCancelled();
            mTasks.remove(this);
        }
    }

    private static class CoverTask extends AsyncTask<CoverParams, Void, Void> {
        private Bitmap mImage = null;
        private Series mSeries;

        @Override
        protected Void doInBackground(CoverParams... coverParams) {
            if (isCancelled())
                return null;
            mSeries = coverParams[0].series;
            List<String> markup = coverParams[0].markup;
            String coverSource = "";
            /* If the series' overview page_pager HTML was not handed down */
            if (markup == null)
                markup = downloadPage(mSeries.source);
            for (String markupLine : markup) {
                /* Manga Eden uses class "mangaImage2" is used solely for cover art divisions */
                if (markupLine.contains("mangaImage2")) {
                    Matcher matcher = mCoverPattern.matcher(markupLine);
                    if (matcher.find()) {
                        coverSource = matcher.group(1);
                        if (!coverSource.startsWith("https:"))
                            coverSource = "https:" + coverSource;
                        break;
                    }
                }
            }
            /* If the series has no cover */
            if (!coverSource.isEmpty())
                mImage = downloadImage(coverSource);
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            if ((mSeries != null) && (mImage != null)) {
                mSeries.setCover(mImage);
                if ((Core.get().currentSeries == mSeries) && (Core.get().mainActivity != null))
                    Core.get().mainActivity.refreshSeriesFragment();
            }
            mTasks.remove(this);
        }

        @Override
        public void onCancelled() {
            super.onCancelled();
            mTasks.remove(this);
        }
    }

    private static class ChapterListTask extends AsyncTask<ChapterListParams, Void, Void> {
        private Series mSeries;
        private List<Chapter> mChapters = new LinkedList<>();
        private List<Chapter> mNewChapters = new LinkedList<>();

        @Override
        protected Void doInBackground(ChapterListParams... chapterParams) {
            if (isCancelled())
                return null;
            mSeries = chapterParams[0].series;
            List<String> markup = chapterParams[0].markup;
            boolean inChapter = false;
            Chapter chapter = null;
            /* If spun off from a SeriesTask, the markup will have been passed down.  Otherwise, */
            /* the markup will need to be downloaded now. */
            if (markup == null)
                markup = downloadPage(mSeries.source);
            for (String markupLine : markup) {
                /* Manga Eden lists chapters in a table with each row being one chapter and the */
                /* first cell of each row beginning with a link to the chapter and applies the */
                /* "chapterLink" class to the A element. */
                /* If a chapter link begins on this line */
                if (markupLine.contains("chapterLink") || inChapter) {
                    /* If this is the first line of a new chapter */
                    if (!inChapter) {
                        chapter = new Chapter();
                        chapter.series = mSeries;
                        Matcher matcher = mChapterLinkPattern.matcher(markupLine);
                        if (matcher.find()) {
                            if (matcher.group(1).startsWith(mHostUrl))
                                chapter.source = matcher.group(1);
                            else
                                chapter.source = mHostUrl + matcher.group(1);
                        }
                        inChapter = true;
                    }
                    /* If the line contains a B element */
                    else if (markupLine.contains("<b>")) {
                        /* This line contains a chapter number and, possibly, title */
                        Matcher matcher = mChapterTitlePattern.matcher(markupLine);
                        /* If the line is in the <chapter number>: <title> format */
                        if (matcher.find()) {
                            /* group(1) = chapter number, group(3) = chapter title */
                            chapter.chapterNumber = Float.parseFloat(matcher.group(1));
                            chapter.title = "Ch. " + matcher.group(1) + ": " +
                                    Html.fromHtml(matcher.group(3)).toString();
                        }
                        matcher = mChapterTitlePattern2.matcher(markupLine);
                        /* If the line is in the format that only contains the chapter number */
                        if (matcher.find()) {
                            /* group(1) = chapter number */
                            chapter.chapterNumber = Float.parseFloat(matcher.group(1));
                            chapter.title = "Ch. " + matcher.group(1);
                        }
                    }
                    /* If the table cell uses the "chapterDate" class and only that class */
                    else if (markupLine.contains("class=\"chapterDate\"")) {
                        /* This is the date on which the chapter was published to Manga Eden */
                        /* If the chapter was released today or yesterday */
                        if (markupLine.contains("Today") || markupLine.contains("Yesterday")) {
                            /* Set the date to the beginning of the current day */
                            Date date = new Date();
                            Calendar calendar = new GregorianCalendar();
                            calendar.setTime(date);
                            calendar.set(Calendar.HOUR_OF_DAY, 0);
                            calendar.set(Calendar.MINUTE, 0);
                            calendar.set(Calendar.SECOND, 0);
                            calendar.set(Calendar.MILLISECOND, 0);
                            /* If the chapter was released yesterday, Move the date back one day */
                            if (markupLine.contains("Yesterday"))
                                calendar.add(Calendar.DAY_OF_WEEK, -1);
                            date = calendar.getTime();
                            chapter.date = date;
                        } else {
                            Matcher matcher = mChapterDatePattern.matcher(markupLine);
                            if (matcher.find()) {
                                try {
                                    /* E.g. Dec 10, 2017 */
                                    chapter.date = new SimpleDateFormat("MMM dd, yyyy",
                                            Locale.getDefault()).parse(matcher.group(1));
                                } catch (Exception e) {
                                    chapter.date = new Date(0); /* January 1, 1970 */
                                    Utils.Log.e("MangaEdenScraper", "Failed to parse chapter date", e);
                                }
                            }
                        }
                        /* If the series does not contain this chapter */
                        if (!mSeries.getChapters().contains(chapter)) {
                            mChapters.add(chapter);
                            /* If this is an update check and the series does not currently */
                            /* contain this chapter in its list */
                            if (chapterParams[0].isUpdateCheck)
                                mNewChapters.add(chapter);
                        }
                        /* The date cell is also the last one to check in the series meaning that */
                        /* this chapter is now done */
                        inChapter = false;
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void p) {
            if (mChapters.size() > 0) {
                /* Manga Eden does not display any indication of the series' availability */
                /* (whether it's licensed or not) on the series' overview so one chapter must be */
                /* opened to be checked */
                ChapterTask chapterTask = new ChapterTask();
                mTasks.add(chapterTask);
                chapterTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
                        new ChapterParams(mChapters.get(0), true));
            }
            /* Attempt to add all chapters found.  addChapter() will verify the chapter is not */
            /* already present before adding it. */
            for (Chapter chapter : mChapters)
                mSeries.addChapter(chapter);
            /* If there are chapters to display in the updates list and (possibly) notify the */
            /* user of */
            if (!mNewChapters.isEmpty()) {
                for (Chapter chapter : mNewChapters) {
                    if (!Core.get().updatesList.contains(chapter))
                        Core.get().updatesList.add(chapter);
                }
                Core.get().sortUpdatesList();
                if (Core.get().mainActivity != null)
                    Core.get().mainActivity.refreshUpdatesFragment();
                showUpdateNotification(mNewChapters);
                Core.get().writeCatalogLite();
            }
            mSeries.sortChapters();
            if (Core.get().mainActivity != null) {
                Core.get().mainActivity.refreshLibraryFragment();
                Core.get().mainActivity.refreshSeriesFragment();
            }
            if (Core.get().pagerReaderActivity != null)
                Core.get().pagerReaderActivity.refresh();
            if (Core.get().stitcherReaderActivity != null)
                Core.get().stitcherReaderActivity.refresh();
            mTasks.remove(this);
        }

        @Override
        public void onCancelled() {
            super.onCancelled();
            mTasks.remove(this);
        }
    }

    private static class PageTask extends AsyncTask<Page, Void, Void> {
        private Page mPage;
        private Bitmap mImage = null;

        @Override
        protected Void doInBackground(Page... p) {
            if (isCancelled())
                return null;
            mPage = p[0];
            List<String> markup = downloadPage(mPage.source);
            for (String markupLine : markup) {
                /* Manga Eden applies the id "mainImg" to page_pager image */
                /* If this line contains the page_pager itself */
                if (markupLine.contains("id=\"mainImg\"")) {
                    Matcher matcher = mPagePattern.matcher(markupLine);
                    if (matcher.find()) {
                        String pageSource = matcher.group(1);
                        if (!pageSource.startsWith("https:"))
                            pageSource = "https:" + pageSource;
                        mImage = downloadImage(pageSource);
                    }
                    /* The rest of the markup can be ignored */
                    return null;
                }
            }
            return null;
        }

        @Override
        public void onPostExecute(Void v) {
            if (mImage != null)
                mPage.setImage(mImage);
            mTasks.remove(this);
        }

        @Override
        public void onCancelled() {
            super.onCancelled();
            mTasks.remove(this);
        }
    }

    /**********************************************************************************************/

    public MangaEdenScraper() {
        hostName = "Manga Eden";
        hasRandom = true;
        mHostName = hostName;
        mHostUrl = "https://www.mangaeden.com";
        mSearchUrl = "https://www.mangaeden.com/en/en-directory/?title=";
        mRandomUrl = "https://www.mangaeden.com/en/random-manga/?r=";
        mSearchPattern = Pattern.compile("<a href=\"(.+)\" class=\"\\w+Manga\">(.+)</a>");
        mChapterPageSelectorPattern = Pattern.compile("<option value=\"(.+)\" " +
                "data-page=\"(\\d+)\" (?:class=\"selected\" " +
                "selected=\"selected\"|class=\"ui-state-default\")>");
        mSummaryPattern = Pattern.compile("<h2 id=\"mangaDescription\">(.+)(?:</h2>)?");
        mSummaryPattern2 = Pattern.compile("(.+)</h2>");
        mCoverPattern = Pattern.compile("<div class=\"mangaImage2\">" +
                "<img src=\"(.+)\"\\salt=\".+\"\\s/></div>");
        mChapterLinkPattern = Pattern.compile("<a href=\"(.+)\" class=\"chapterLink\">");
        mChapterTitlePattern = Pattern.compile("<b>(\\d+(\\.\\d+)?):\\s(.+)</b>");
        mChapterTitlePattern2 = Pattern.compile("<b>(\\d+(\\.\\d+)?)</b>");
        mChapterDatePattern = Pattern.compile("<td class=\"chapterDate\">(.+)</td>");
        mPagePattern = Pattern.compile("<img width=\"\\d+\" height=\"\\d+\" id=\"mainImg\" " +
                "alt=\".+\" srcset=\".+\" src=\"(.+)\" onerror=\".+\" />");
    }

    public void fillSearchFragment(String title) {
        SearchTask task = new SearchTask();
        mTasks.add(task);
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, title);
    }

    public void fillSeriesFragment(Series series) {
        SeriesTask task = new SeriesTask();
        mTasks.add(task);
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, series);
    }

    public void fillSeriesFragmentRandom() {
        /* Manga Eden uses a five-character seed to choose the random series.  The characters can */
        /* be 0 - 9 or a - z.  To reproduce this, five numbers will be chosen between 0 and 35 */
        /* (26 values corresponding to letters and 10 to numbers).  The result will be something */
        /* like 5gj7z, which will be appended to the URL. */
        String url = mRandomUrl;
        StringBuilder stringBuilder = new StringBuilder();
        Random random = new Random();
        Series series = new Series();
        SeriesTask task = new SeriesTask();
        for (int i = 0; i < 5; i++) {
            /* Generate a value from 0 to 35 (the parameter is exclusive) */
            int value = random.nextInt(36);
            /* If the value will correspond with a letter */
            if (value < 26)
                stringBuilder.append((char) (value + 97));
            else
                stringBuilder.append(value - 26);
        }
        url += stringBuilder.toString();
        series.source = url;
        series.scraper = this;
        mTasks.add(task);
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, series);
    }

    public void fillChapter(Chapter chapter) {
        if (chapter.getPages().isEmpty()) {
            ChapterTask task = new ChapterTask();
            mTasks.add(task);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new ChapterParams(chapter,
                    false));
        }
    }

    public void fillPage(Page page) {
        if (page.getImage() == null) {
            PageTask task = new PageTask();
            mTasks.add(task);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, page);
        }
    }


    public void checkForUpdates(Series series) {
        CoverTask coverTask = new CoverTask();
        mTasks.add(coverTask);
        coverTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new CoverParams(series, null));
        ChapterListTask chapterListTask = new ChapterListTask();
        mTasks.add(chapterListTask);
        chapterListTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new ChapterListParams(
                series, true, null));
    }
}
