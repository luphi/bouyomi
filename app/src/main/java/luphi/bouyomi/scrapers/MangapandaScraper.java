/* Copyright (C) 2019 Lucas Philipsen
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see http://www.gnu.org/licenses/.
 */

package luphi.bouyomi.scrapers;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.text.Html;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import luphi.bouyomi.Chapter;
import luphi.bouyomi.Core;
import luphi.bouyomi.Page;
import luphi.bouyomi.Scraper;
import luphi.bouyomi.Series;
import luphi.bouyomi.Utils;

import static luphi.bouyomi.Utils.showUpdateNotification;

public class MangapandaScraper extends Scraper {
    String mHostUrl;
    String mSearchUrl;
    String mRandomUrl;
    private static Pattern mSearchPattern;
    private static Pattern mTitlePattern;
    private static Pattern mSummaryPattern1;
    private static Pattern mSummaryPattern2;
    private static Pattern mSummaryPattern3;
    private static Pattern mCoverPattern;
    private static Pattern mChapterPattern;
    private static Pattern mChapterDatePattern;
    private static Pattern mChapterPageSelectPattern;
    private static Pattern mPagePattern;

    private static class SearchParams {
        String title, searchUrl, hostName, hostUrl;

        SearchParams(String t, String s, String hn, String hu) {
            title = t;
            searchUrl = s;
            hostName = hn;
            hostUrl = hu;
        }
    }

    private static class SeriesParams {
        Series series;
        String hostName, randomUrl, hostUrl;

        SeriesParams(Series s, String hn, String r, String hu) {
            series = s;
            hostName = hn;
            randomUrl = r;
            hostUrl = hu;
        }
    }

    private static class ChapterListParams {
        Series series;
        boolean isUpdateCheck;
        List<String> markup;
        String hostName, hostUrl;

        ChapterListParams(Series s, boolean i, List<String> m, String hn, String hu) {
            series = s;
            isUpdateCheck = i;
            markup = m;
            hostName = hn;
            hostUrl = hu;
        }
    }

    private static class ChapterParams {
        Chapter chapter;
        String hostUrl;

        ChapterParams(Chapter c, String h) {
            chapter = c;
            hostUrl = h;
        }
    }

    private static class CoverParams {
        Series series;
        List<String> markup;
        String coverSource;

        CoverParams(Series s, List<String> m, String c) {
            series = s;
            markup = m;
            coverSource = c;
        }
    }

    private static class SearchTask extends AsyncTask<SearchParams, Void, Void> {
        private List<Series> mList = new LinkedList<>();

        @Override
        protected Void doInBackground(SearchParams... searchParams) {
            if (isCancelled())
                return null;
            List<String> markup = downloadPage(searchParams[0].searchUrl +
                    searchParams[0].title.toLowerCase().replace(' ', '+'));
            for (String markupLine : markup) {
                /* Mangapanda begins its series results with the <h3> header tag */
                if (markupLine.startsWith("<h3>")) {
                    Matcher matcher = mSearchPattern.matcher(markupLine);
                    if (matcher.find()) {
                        Series series = new Series();
                        series.scraper = Core.get().getScraperByName(searchParams[0].hostName);
                        series.title = Html.fromHtml(matcher.group(2)).toString();
                        if (matcher.group(1).startsWith(searchParams[0].hostUrl))
                            series.source = matcher.group(1);
                        else
                            series.source = searchParams[0].hostUrl + matcher.group(1);
                        mList.add(series);
                    }
                }
            }
            Core.get().searchResults.clear();
            Core.get().searchResults.addAll(mList);
            Core.get().searchComplete = true;
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            if (Core.get().mainActivity != null)
                Core.get().mainActivity.refreshSearchFragment();
            mTasks.remove(this);
        }

        @Override
        public void onCancelled() {
            super.onCancelled();
            mTasks.remove(this);
        }
    }

    private static class SeriesTask extends AsyncTask<SeriesParams, Void, Void> {
        private Series mSeries = null;
        private List<String> mMarkup = new LinkedList<>();
        private String mTitle = "";
        private String mSummary = "";
        private String mCoverSource = "";
        private String mHostName = "";
        private String mHostUrl = "";

        @Override
        protected Void doInBackground(SeriesParams... seriesParams) {
            if (isCancelled())
                return null;
            Series series = seriesParams[0].series;
            mHostName = seriesParams[0].hostName;
            mHostUrl = seriesParams[0].hostUrl;
            String randomUrl = seriesParams[0].randomUrl;
            boolean inSummary = false;
            StringBuilder stringBuilder = new StringBuilder();
            /* If no series is provided */
            if (series == null) {
                /* A random series should be fetched */
                mSeries = new Series();
                mSeries.scraper = Core.get().getScraperByName(mHostName);
                mMarkup = downloadPage(randomUrl);
            }
            /* If a series is provided */
            else {
                /* Fill out the remaining information for that series */
                mSeries = series;
                mMarkup = downloadPage(mSeries.source);
            }
            for (String markupLine : mMarkup) {
                /* Mangapanda applies the class "aname" to title headers */
                if (markupLine.contains("aname")) {
                    Matcher matcher = mTitlePattern.matcher(markupLine);
                    if (matcher.find())
                        mTitle = Html.fromHtml(matcher.group(1)).toString();
                }
                /* Mangapanda uses the paragraph tag solely for series summaries */
                else if (markupLine.startsWith("<p>")) {
                    /* For series with no description, the paragraph element will be empty */
                    if (markupLine.equals("<p></p>"))
                        continue;
                    Matcher matcher = mSummaryPattern1.matcher(markupLine);
                    if (matcher.find())
                        mSummary = Html.fromHtml(matcher.group(1)).toString();
                    else {
                        inSummary = true;
                        matcher = mSummaryPattern2.matcher(markupLine);
                        if (matcher.find())
                            stringBuilder.append(matcher.group(1));
                    }
                } else if (inSummary) {
                    Matcher matcher = mSummaryPattern3.matcher(markupLine);
                    if (matcher.find()) {
                        if (!matcher.group(1).isEmpty()) {
                            stringBuilder.append(" ");
                            stringBuilder.append(matcher.group(1));
                        }
                        inSummary = false;
                    } else if (!markupLine.isEmpty()) {
                        stringBuilder.append(" ");
                        stringBuilder.append(markupLine);
                    }
                }
                /* The ID "mangaimg" is applied to the cover */
                else if (markupLine.contains("mangaimg")) {
                    Matcher matcher = mCoverPattern.matcher(markupLine);
                    if (matcher.find())
                        mCoverSource = matcher.group(1);
                }
            }
            if (mSummary.isEmpty())
                mSummary = Html.fromHtml(stringBuilder.toString()).toString();
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            if (!mTitle.isEmpty())
                mSeries.title = mTitle;
            mSeries.summary = mSummary;
            CoverTask coverTask = new CoverTask();
            mTasks.add(coverTask);
            coverTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
                    new CoverParams(mSeries, mMarkup, mCoverSource));
            ChapterListTask chapterListTask = new ChapterListTask();
            mTasks.add(chapterListTask);
            chapterListTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
                    new ChapterListParams(mSeries, false, mMarkup, mHostName, mHostUrl));
            Core.get().currentSeries = mSeries;
            if (Core.get().mainActivity != null)
                Core.get().mainActivity.refreshSeriesFragment();
            mTasks.remove(this);
        }

        @Override
        public void onCancelled() {
            super.onCancelled();
            mTasks.remove(this);
        }
    }

    private static class CoverTask extends AsyncTask<CoverParams, Void, Void> {
        private Bitmap mImage = null;
        private Series mSeries;

        @Override
        protected Void doInBackground(CoverParams... coverParams) {
            if (isCancelled())
                return null;
            mSeries = coverParams[0].series;
            List<String> markup = coverParams[0].markup;
            String coverSource = coverParams[0].coverSource;
            /* If the series' overview page_pager HTML was not handed down */
            if (markup == null)
                markup = downloadPage(mSeries.source);
            for (String markupLine : markup) {
                /* The ID "mangaimg" is applied to the cover */
                if (markupLine.contains("mangaimg")) {
                    Matcher matcher = mCoverPattern.matcher(markupLine);
                    if (matcher.find())
                        coverSource = matcher.group(1);
                }
            }
            /* If the series has a cover */
            if ((coverSource != null) && !coverSource.isEmpty())
                mImage = downloadImage(coverSource);
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            if ((mSeries != null) && (mImage != null)) {
                mSeries.setCover(mImage);
                if ((Core.get().currentSeries == mSeries) && (Core.get().mainActivity != null))
                    Core.get().mainActivity.refreshSeriesFragment();
            }
            mTasks.remove(this);
        }

        @Override
        public void onCancelled() {
            super.onCancelled();
            mTasks.remove(this);
        }
    }

    private static class ChapterListTask extends AsyncTask<ChapterListParams, Void, Void> {
        private Series mSeries;
        private List<Chapter> mChapters = new LinkedList<>();
        private List<Chapter> mNewChapters = new LinkedList<>();

        @Override
        protected Void doInBackground(ChapterListParams... chapterListParams) {
            if (isCancelled())
                return null;
            mSeries = chapterListParams[0].series;
            List<String> markup = chapterListParams[0].markup;
            boolean inChapter = false, inWholeList = false;
            Chapter chapter = null;
            /* If spun off from a SeriesTask, the markup will have been passed down.  Otherwise, */
            /* the markup will need to be downloaded now. */
            if (markup == null)
                markup = downloadPage(mSeries.source);
            for (String markupLine : markup) {
                /* Series overviews have a list of "latest chapters" which contain only a */
                /* handful of chapters.  The whole list of chapters appears just after the */
                /* following table header. */
                if (markupLine.equals("<th class=\"leftgap\">Chapter Name</th>"))
                    inWholeList = true;
                /* Mangapanda applies the class "chico_manga" to the opening of each chapter */
                else if (inWholeList && markupLine.contains("chico_manga")) {
                    chapter = new Chapter();
                    chapter.series = mSeries;
                    inChapter = true;
                    /* There is no other useful information on this line of the markup */
                } else if (inWholeList && inChapter) {
                    Matcher matcher = mChapterPattern.matcher(markupLine);
                    if (matcher.find()) {
                        /* group(1) = chapter link, group(2) = series title and chapter number, */
                        /* group(3) = chapter number, group(5) = chapter title, possibly empty */
                        if (!matcher.group(5).isEmpty())
                            chapter.title = "Ch. " + matcher.group(3) + ": " +
                                    Html.fromHtml(matcher.group(5)).toString();
                        else
                            chapter.title = "Ch. " + matcher.group(3);
                        chapter.chapterNumber = Float.parseFloat(matcher.group(3));
                        if (matcher.group(1).startsWith(chapterListParams[0].hostUrl))
                            chapter.source = matcher.group(1);
                        else
                            chapter.source = chapterListParams[0].hostUrl + matcher.group(1);
                    }
                    matcher = mChapterDatePattern.matcher(markupLine);
                    if (matcher.find()) {
                        try {
                            /* E.g. 12/31/2017 */
                            chapter.date = new SimpleDateFormat("MM/dd/yyyy",
                                    Locale.getDefault()).parse(matcher.group(1));
                        } catch (Exception e) {
                            chapter.date = new Date(0); /* January 1, 1970 */
                            Utils.Log.e("MangapandaScraper", "Failed to parse chapter date", e);
                        }
                        /* If the series does not contain this chapter */
                        if (!mSeries.getChapters().contains(chapter)) {
                            mChapters.add(chapter);
                            /* If this is an update check and the series does not currently */
                            /* contain this chapter in its list */
                            if (chapterListParams[0].isUpdateCheck)
                                mNewChapters.add(chapter);
                        }
                        /* The date cell is also the last one to check in the series meaning that */
                        /* this chapter is now done */
                        inChapter = false;
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void p) {
            /* Attempt to add all chapters found.  addChapter() will verify the chapter is not */
            /* already present before adding it. */
            for (Chapter chapter : mChapters)
                mSeries.addChapter(chapter);
            /* If there are chapters to display in the updates list and (possibly) notify the */
            /* user of */
            if (!mNewChapters.isEmpty()) {
                for (Chapter chapter : mNewChapters) {
                    if (!Core.get().updatesList.contains(chapter))
                        Core.get().updatesList.add(chapter);
                }
                Core.get().sortUpdatesList();
                if (Core.get().mainActivity != null)
                    Core.get().mainActivity.refreshUpdatesFragment();
                showUpdateNotification(mNewChapters);
                Core.get().writeCatalogLite();
            }
            mSeries.sortChapters();
            if (Core.get().mainActivity != null) {
                Core.get().mainActivity.refreshLibraryFragment();
                Core.get().mainActivity.refreshSeriesFragment();
            }
            if (Core.get().pagerReaderActivity != null)
                Core.get().pagerReaderActivity.refresh();
            if (Core.get().stitcherReaderActivity != null)
                Core.get().stitcherReaderActivity.refresh();
            mTasks.remove(this);
        }

        @Override
        public void onCancelled() {
            super.onCancelled();
            mTasks.remove(this);
        }
    }

    private static class ChapterTask extends AsyncTask<ChapterParams, Void, Void> {
        private Chapter mChapter = null;
        private List<Page> mPages = new LinkedList<>();

        @Override
        protected Void doInBackground(ChapterParams... chapterParams) {
            if (isCancelled())
                return null;
            mChapter = chapterParams[0].chapter;
            List<String> markup = downloadPage(mChapter.source);
            boolean inOptions = false;
            for (String markupLine : markup) {
                /* Each page_pager of the chapter contains a dropdown list at the top of pages to jump */
                /* to, as well as one at the bottom.  The goal is to create a Page object for */
                /* each and assign an index (beginning at 0) to them. */
                if (markupLine.contains("<option")) {
                    inOptions = true;
                    Matcher matcher = mChapterPageSelectPattern.matcher(markupLine);
                    if (matcher.find()) {
                        Page page = new Page();
                        page.series = mChapter.series;
                        page.chapter = mChapter;
                        if (matcher.group(1).startsWith(chapterParams[0].hostUrl))
                            page.source = matcher.group(1);
                        else
                            page.source = chapterParams[0].hostUrl + matcher.group(1);
                        page.index = Integer.parseInt(matcher.group(2)) - 1;
                        mPages.add(page);
                    }
                }
                /* If there are no more option items */
                else if (inOptions && !markupLine.contains("<option"))
                    break;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            if (mChapter != null) {
                for (Page page : mPages)
                    mChapter.addPage(page);
            }
            if (Core.get().pagerReaderActivity != null)
                Core.get().pagerReaderActivity.refresh();
            if (Core.get().stitcherReaderActivity != null)
                Core.get().stitcherReaderActivity.refresh();
            Core.get().writeCatalog();
            mTasks.remove(this);
        }

        @Override
        public void onCancelled() {
            super.onCancelled();
            mTasks.remove(this);
        }
    }

    private static class PageTask extends AsyncTask<Page, Void, Void> {
        private Page mPage = null;
        private Bitmap mImage = null;

        @Override
        protected Void doInBackground(Page... p) {
            if (isCancelled())
                return null;
            mPage = p[0];
            List<String> markup = downloadPage(mPage.source);
            for (String markupLine : markup) {
                /* Mangapanda applies ID "img" to the page_pager's <img> element */
                if (markupLine.contains("id=\"img\"")) {
                    Matcher matcher = mPagePattern.matcher(markupLine);
                    if (matcher.find())
                        mImage = downloadImage(matcher.group(1));
                    /* The rest of the markup can be ignored */
                    return null;
                }
            }
            return null;
        }

        @Override
        public void onPostExecute(Void v) {
            if ((mPage != null) && (mImage != null))
                mPage.setImage(mImage);
            mTasks.remove(this);
        }

        @Override
        public void onCancelled() {
            super.onCancelled();
            mTasks.remove(this);
        }
    }

    public MangapandaScraper() {
        hostName = "Mangapanda";
        hasRandom = true;
        mHostUrl = "https://www.mangapanda.com";
        mSearchUrl = "https://www.mangapanda.com/search/?w=";
        mRandomUrl = "https://www.mangapanda.com/random";
        mSearchPattern = Pattern.compile("<h3><a href=\"(.+)\">(.+)</a></h3>");
        mTitlePattern = Pattern.compile("<h2 class=\"aname\">(.+)</h2>");
        mSummaryPattern1 = Pattern.compile("<p>(.+)</p>");
        mSummaryPattern2 = Pattern.compile("<p>(.+)");
        mSummaryPattern3 = Pattern.compile("(.*)</p>");
        mCoverPattern = Pattern.compile("<div id=\"mangaimg\"><img src=\"(.+)\" " +
                "alt=\".+\" /></div>");
        mChapterPattern = Pattern.compile("<a href=\"(.+)\">(.+\\s(\\d+(\\.\\d+)?))</a> " +
                ": (.*)</td>");
        mChapterDatePattern = Pattern.compile("<td>(.+)</td>");
        mChapterPageSelectPattern = Pattern.compile("<option value=\"([^\"]+)\"(?: " +
                "selected=\"selected\")?>(\\d+)</option>");
        mPagePattern = Pattern.compile("<img id=\"img\" width=\"\\d+\" height=\"\\d+\" " +
                "src=\"([^\"]+)\"(?: alt=\".*\")? />");
    }

    public void fillSearchFragment(String title) {
        SearchTask task = new SearchTask();
        mTasks.add(task);
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new SearchParams(title, mSearchUrl,
                hostName, mHostUrl));
    }

    public void fillSeriesFragment(Series series) {
        SeriesTask task = new SeriesTask();
        mTasks.add(task);
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new SeriesParams(series, hostName,
                mRandomUrl, mHostUrl));
    }

    public void fillSeriesFragmentRandom() {
        SeriesTask task = new SeriesTask();
        mTasks.add(task);
        /* The cast is necessary to avoid an ambiguous method call between SeriesTask and the */
        /* parent AsyncTask */
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new SeriesParams(null, hostName,
                mRandomUrl, mHostUrl));
    }

    public void fillChapter(Chapter chapter) {
        if (chapter.getPages().isEmpty()) {
            ChapterTask task = new ChapterTask();
            mTasks.add(task);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new ChapterParams(chapter,
                    mHostUrl));
        }
    }

    public void fillPage(Page page) {
        if (page.getImage() == null) {
            PageTask task = new PageTask();
            mTasks.add(task);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, page);
        }
    }

    public void checkForUpdates(Series series) {
        CoverTask coverTask = new CoverTask();
        mTasks.add(coverTask);
        coverTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
                new CoverParams(series, null, ""));
        ChapterListTask chapterListTask = new ChapterListTask();
        mTasks.add(chapterListTask);
        chapterListTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new ChapterListParams(
                series, true, null, hostName, mHostUrl));
    }
}
