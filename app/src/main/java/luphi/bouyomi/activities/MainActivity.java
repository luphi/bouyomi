/* Copyright (C) 2019 Lucas Philipsen
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see http://www.gnu.org/licenses/.
 */

package luphi.bouyomi.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.navigation.NavigationView.OnNavigationItemSelectedListener;

import luphi.bouyomi.Chapter;
import luphi.bouyomi.Core;
import luphi.bouyomi.R;
import luphi.bouyomi.Series;
import luphi.bouyomi.fragments.DownloadQueueFragment;
import luphi.bouyomi.fragments.LibraryFragment;
import luphi.bouyomi.fragments.PreferenceFragment;
import luphi.bouyomi.fragments.SearchFragment;
import luphi.bouyomi.fragments.SeriesFragment;
import luphi.bouyomi.fragments.UpdatesFragment;

public class MainActivity extends AppCompatActivity implements OnNavigationItemSelectedListener {
    private Menu mOptionsMenu = null;
    private int mOptionsMenuID = R.menu.library;
    private LibraryFragment mLibraryFragment = null;
    private SearchFragment mSearchFragment = null;
    private UpdatesFragment mUpdatesFragment = null;
    private DownloadQueueFragment mDownloadQueueFragment = null;
    private PreferenceFragment mPreferenceFragment = null;
    private SeriesFragment mSeriesFragment = null;

    public int currentNavID = R.id.nav_library;
    public int previousNavID = R.id.nav_library;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /* Load the core and parse the catalog */
        Core.get().mainActivity = this;
        Core.get().init();
        Core.get().readCatalog();
        /* Parse the main activity */
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        /* Create the fragments */
        mLibraryFragment = new LibraryFragment();
        mSearchFragment = new SearchFragment();
        mUpdatesFragment = new UpdatesFragment();
        mDownloadQueueFragment = new DownloadQueueFragment();
        mPreferenceFragment = new PreferenceFragment();
        mSeriesFragment = new SeriesFragment();
        /* Add the drawer (hide-able tab things) */
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.nav_drawer_open, R.string.nav_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        /* If the scraper in use is one that supports a random series search */
        if (Core.get().getDefaultScraper().hasRandom)
            navigationView.getMenu().findItem(R.id.nav_random).setVisible(true);
        else
            navigationView.getMenu().findItem(R.id.nav_random).setVisible(false);
        /* If the activity is being opened from an update notification */
        if (getIntent().hasExtra("notification"))
            openDrawerByID(R.id.nav_updates); /* Show the recent updates */
        else
            openDrawerByID(R.id.nav_library); /* Show the library */
    }

    @Override
    public void onBackPressed() {
        if (previousNavID == R.id.nav_random) {
            /* In the case of viewing a random series, nav_series will be current with */
            /* nav_random being previous.  In order to avoid an infinite loop, a back press will */
            /* return the user to the library. */
            openDrawerByID(R.id.nav_library);
            return;
        }
        switch (currentNavID) {
            case R.id.nav_library:
                super.onBackPressed(); /* Pause/close the app */
                break;
            case R.id.nav_series:
                openDrawerByID(previousNavID); /* Return to the library or search fragment */
                break;
            default: /* nav_search, nav_updates, etc. */
                openDrawerByID(R.id.nav_library);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mOptionsMenu = menu;
        /* If there's no menu to display */
        if (mOptionsMenuID == -1)
            mOptionsMenu.clear();
        else
            getMenuInflater().inflate(mOptionsMenuID, mOptionsMenu);
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        for (int i = 0; i < permissions.length; i++) {
            if ((permissions[i].contains("WRITE_EXTERNAL_STORAGE") ||
                permissions[i].contains("READ_EXTERNAL_STORAGE")) && (grantResults[i] == 0)) {
                Core.get().unregisterListeners();
                Core.get().init(); /* Will re-register the listener(s) */
                Core.get().readCatalog(); /* Populate the library from the catalog */
                Core.get().writeCatalog(); /* Create the app directory, etc. now with permission */
                refreshLibraryFragment(); /* Display the library */
                setRandomDrawerVisible(Core.get().getDefaultScraper().hasRandom);
                return;
            }
        }
    }

    public void setOptionsMenu(int id) {
        if (mOptionsMenu == null)
            return;
        if (id != -1) {
            mOptionsMenu.clear();
            mOptionsMenuID = id;
            getMenuInflater().inflate(mOptionsMenuID, mOptionsMenu);
        }
    }

    public void clearOptionsMenu() {
        if (mOptionsMenu != null)
            mOptionsMenu.clear();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.library_download_all:
                for (Series series : Core.get().library)
                    Core.get().downloader.addSeriesToQueue(series);
                Core.get().writeCatalogLite();
                if (Core.get().canDownload())
                    Core.get().downloader.start();
                refreshDownloadQueueFragment();
                break;
            case R.id.library_check_for_updates:
                Core.get().checkForUpdates();
                break;
            case R.id.library_mark_all_read:
                for (Series series : Core.get().library) {
                    series.isRead = true;
                    for (Chapter chapter : series.getChapters())
                        chapter.isRead = true;
                }
                refreshLibraryFragment();
                Core.get().writeCatalogLite();
                break;
            case R.id.library_mark_all_unread:
                for (Series series : Core.get().library) {
                    series.isRead = false;
                    for (Chapter chapter : series.getChapters()) {
                        chapter.currentIndex = 0;
                        chapter.isRead = false;
                    }
                }
                refreshLibraryFragment();
                Core.get().writeCatalogLite();
                break;
            case R.id.library_delete_all:
                Core.get().deleteAllSeries();
                break;
            case R.id.random_reroll:
                Core.get().currentSeries = new Series();
                refreshSeriesFragment();
                Core.get().getDefaultScraper().fillSeriesFragmentRandom();
                break;
            case R.id.random_add:
            case R.id.search_add:
                Core.get().addToLibrary(Core.get().currentSeries);
                openDrawerByID(R.id.nav_library);
                break;
            case R.id.download_queue_download_now:
                Core.get().downloader.start();
                setOptionsMenu(R.menu.download_queue_active);
                break;
            case R.id.download_queue_remove_all:
                Core.get().downloader.stop();
                Core.get().downloader.clear();
                setOptionsMenu(R.menu.download_queue_idle);
                refreshDownloadQueueFragment();
                break;
            case R.id.download_queue_pause:
                Core.get().downloader.stop();
                setOptionsMenu(R.menu.download_queue_idle);
                refreshDownloadQueueFragment();
                break;
            case R.id.series_download:
                Core.get().downloader.addSeriesToQueue(Core.get().currentSeries);
                Core.get().writeCatalogLite();
                if (Core.get().canDownload())
                    Core.get().downloader.start();
                break;
            case R.id.series_check_for_updates:
                Core.get().currentSeries.scraper.checkForUpdates(Core.get().currentSeries);
                break;
            case R.id.series_mark_read:
                Core.get().currentSeries.isRead = true;
                for (Chapter chapter : Core.get().currentSeries.getChapters())
                    chapter.isRead = true;
                refreshSeriesFragment();
                Core.get().writeCatalogLite();
                break;
            case R.id.series_mark_unread:
                Core.get().currentSeries.isRead = false;
                for (Chapter chapter : Core.get().currentSeries.getChapters())
                    chapter.isRead = false;
                refreshSeriesFragment();
                Core.get().writeCatalogLite();
                break;
            case R.id.series_delete:
                Core.get().deleteSeries(Core.get().currentSeries);
                openDrawerByID(R.id.nav_library);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        openDrawerByID(item.getItemId());
        return true;
    }

    public void openDrawerByID(int id) {
        int title = R.string.library_title, openDrawerIndex = 0;
        mOptionsMenuID = R.menu.library;
        Fragment fragment = null;
        NavigationView navigationView = findViewById(R.id.nav_view);
        /* When leaving certain tabs, there may be some things to do */
        switch (currentNavID) {
            case R.id.nav_series:
                /* For certain hosts and certain series, the series may have been marked as */
                /* unavailable.  A notice will appear on the SeriesFragment.  To avoid having */
                /* appear again in the future, change the flag: */
                Core.get().isCurrentSeriesAvailable = true;
                break;
            case R.id.nav_random:
                /* Similar to the above case, there are tasks that may need to be cancelled but */
                /* random series searches are performed with the default scraper */
                Core.get().getDefaultScraper().cancelAll();
                break;
        }
        /* Switch the selected tab */
        previousNavID = currentNavID;
        currentNavID = id;
        switch (currentNavID) {
            case R.id.nav_library:
                if (mLibraryFragment != null) {
                    fragment = mLibraryFragment;
                    title = R.string.library_title;
                    mOptionsMenuID = R.menu.library;
                    openDrawerIndex = 0;
                }
                break;
            case R.id.nav_search:
                if (mSearchFragment != null) {
                    Core.get().currentSeries = new Series();
                    fragment = mSearchFragment;
                    title = R.string.search_title;
                    mOptionsMenuID = R.menu.search;
                    openDrawerIndex = 1;
                }
                break;
            case R.id.nav_random:
                if (mSeriesFragment != null) {
                    Core.get().currentSeries = new Series();
                    fragment = mSeriesFragment;
                    Core.get().getDefaultScraper().fillSeriesFragmentRandom();
                    previousNavID = currentNavID;
                    currentNavID = R.id.nav_series;
                    title = R.string.random_title;
                    mOptionsMenuID = R.menu.series_random;
                    openDrawerIndex = 2;
                }
                break;
            case R.id.nav_updates:
                if (mUpdatesFragment != null) {
                    fragment = mUpdatesFragment;
                    title = R.string.updates_title;
                    mOptionsMenuID = -1;
                    openDrawerIndex = 3;
                }
                break;
            case R.id.nav_download_queue:
                if (mDownloadQueueFragment != null) {
                    fragment = mDownloadQueueFragment;
                    title = R.string.download_queue_title;
                    if (Core.get().downloader.isDownloading())
                        mOptionsMenuID = R.menu.download_queue_active;
                    else
                        mOptionsMenuID = R.menu.download_queue_idle;
                    openDrawerIndex = 4;
                }
                break;
            case R.id.nav_preferences:
                if (mPreferenceFragment != null) {
                    fragment = mPreferenceFragment;
                    title = R.string.preferences_title;
                    mOptionsMenuID = -1;
                    openDrawerIndex = 5;
                }
                break;
            case R.id.nav_series:
                if (mSeriesFragment != null) {
                    fragment = mSeriesFragment;
                    /* If the series being opened is one in the library */
                    if (previousNavID == R.id.nav_library) {
                        title = R.string.library_title;
                        openDrawerIndex = 0;
                        if (Core.get().currentSeries.isLocal)
                            mOptionsMenuID = R.menu.series_local;
                        else
                            mOptionsMenuID = R.menu.series_remote;
                    }
                    /* If the series being opened is one from a search */
                    else {
                        title = R.string.search_title;
                        openDrawerIndex = 1;
                        mOptionsMenuID = R.menu.series_search;
                    }
                }
                break;
        }
        if (fragment != null) {
            /* Replace the active fragment with whichever was chosen */
            getSupportFragmentManager().beginTransaction().replace(
                    R.id.content_frame, fragment).commit();
            switch (currentNavID) {
                case R.id.nav_library:
                    refreshLibraryFragment();
                    break;
                case R.id.nav_search:
                    refreshSearchFragment();
                    break;
                case R.id.nav_updates:
                    refreshUpdatesFragment();
                    break;
                case R.id.nav_download_queue:
                    refreshDownloadQueueFragment();
                    break;
                case R.id.nav_random:
                case R.id.nav_series:
                    refreshSeriesFragment();
                    break;
            }
        }
        /* Set the title that appears on the action bar at the top of the screen */
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(title);
        /* Clear all options from the options menu (the drop-down menu with the "..." icon */
        clearOptionsMenu();
        /* If an options menu exists for the chosen fragment */
        if ((mOptionsMenuID != -1) && (mOptionsMenu != null))
            getMenuInflater().inflate(mOptionsMenuID, mOptionsMenu); /* Set the options menu */
        /* Return the menu to an off-screen position */
        ((DrawerLayout) findViewById(R.id.drawer_layout)).closeDrawer(GravityCompat.START);
        /* Mark all drawers as unselected */
        for (int i = 0; i < navigationView.getMenu().size(); i++)
            navigationView.getMenu().getItem(i).setChecked(false);
        /* Mark the chosen drawer as selected */
        navigationView.getMenu().getItem(openDrawerIndex).setChecked(true);
    }

    public void clearApplicationData(View v) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Are you sure?");
        alertDialogBuilder.setMessage("This will remove all data associated with this app " +
                "including preferences, the library, and any downloaded chapters.");
        alertDialogBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Core.get().clearApplicationData();
                dialog.dismiss();
            }
        });
        alertDialogBuilder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void setRandomDrawerVisible(boolean visible) {
        ((NavigationView) findViewById(R.id.nav_view)).getMenu().findItem(R.id.nav_random).
                setVisible(visible);
    }

    public void refreshLibraryFragment() {
        if ((mLibraryFragment != null) && (currentNavID == R.id.nav_library))
            mLibraryFragment.refresh();
    }

    public void refreshSearchFragment() {
        if ((mSearchFragment != null) && (currentNavID == R.id.nav_search))
            mSearchFragment.refresh();
    }

    public void refreshUpdatesFragment() {
        if ((mUpdatesFragment != null) && (currentNavID == R.id.nav_updates))
            mUpdatesFragment.refresh();
    }

    public void refreshDownloadQueueFragment() {
        if ((mDownloadQueueFragment != null) && (currentNavID == R.id.nav_download_queue))
            mDownloadQueueFragment.refresh();
    }

    public void refreshSeriesFragment() {
        if ((mSeriesFragment != null) && (currentNavID == R.id.nav_series))
            mSeriesFragment.refresh();
    }
}
