/* Copyright (C) 2019 Lucas Philipsen
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see http://www.gnu.org/licenses/.
 */

package luphi.bouyomi.activities;

import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import java.lang.ref.WeakReference;

import luphi.bouyomi.Chapter;
import luphi.bouyomi.Core;
import luphi.bouyomi.Page;
import luphi.bouyomi.PageView;
import luphi.bouyomi.R;
import luphi.bouyomi.Series;
import luphi.bouyomi.Utils;
import luphi.bouyomi.adapters.PagerReaderAdapter;
import luphi.bouyomi.ReaderViewPager;

public class PagerReaderActivity extends AppCompatActivity {
    private ReaderViewPager mPager = null;
    private PagerReaderAdapter mAdapter = null;
    private Handler mHideHandler = new Handler();
    protected Chapter mChapter = null;
    protected ChapterWaitTask mWaitTask = null;
    protected Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            /* Cease drawing the controls */
            findViewById(R.id.pager_reader_controls).setVisibility(View.INVISIBLE);
        }
    };
    public boolean timedOut = false;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Core.get().pagerReaderActivity = this;
        Core.get().stitcherReaderActivity = null;
        /* If the activity is being restored from a system-initiated ejection from memory (AKA */
        /* this app was sitting in the background a while and Android freed up memory by killing */
        /* it), the bundle given to this activity will be non-null. */
        if (bundle != null)
        Core.get().restoreFromBundle(bundle);
        setContentView(R.layout.activity_pager_reader);
        if (getSupportActionBar() != null)
            getSupportActionBar().hide();
        mPager = findViewById(R.id.reader_pager);
        setChapter(Core.get().currentChapter);
    }

    @Override
    public void onResume() {
        Core.get().pagerReaderActivity = this;
        Core.get().stitcherReaderActivity = null;
        super.onResume();
        if (mChapter != null)
            setChapter(mChapter);
        /* If the preferences indicate the reader should use fullscreen mode */
        if (Core.get().getPreference("fullscreen").equals("true")) {
            /* Hide the status and navigation bars, if applicable */
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_FULLSCREEN |
                            View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                            View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                            View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                            View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        /* If the preferences indicate the reader should only use portrait mode */
        if (Core.get().getPreference("reader_orientation").equals("Portrait"))
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        else if (Core.get().getPreference("reader_orientation").equals("Landscape"))
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }

    @Override
    public void onStop() {
        /* Return orientation control up to the system (although it may already be set as such) */
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        super.onStop();
    }

    @Override
    public void onDestroy() {
        if (mChapter != null)
            mChapter.freeImages(); /* Bitmaps must wait to be freed until all Views are destroyed */
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle bundle) {
        writeBundle(bundle, mChapter, mPager.getCurrentItem());
        super.onSaveInstanceState(bundle);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        findViewById(R.id.pager_reader_controls).setVisibility(View.VISIBLE);
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, 1000); /* Hide the controls after 1000 ms */
        return super.dispatchTouchEvent(event);
    }

    @Override
    public void onBackPressed() {
        Core.get().writeCatalogLite();
        /* For series that are not local, it's possible for pages to still be downloading in the */
        /* background; cancel them */
        if (Core.get().currentSeries.scraper != null)
            Core.get().currentSeries.scraper.cancelAll();
        if (Core.get().mainActivity != null)
            Core.get().mainActivity.refreshSeriesFragment();
        if (mWaitTask != null) {
            mWaitTask.cancel(true);
            mWaitTask = null;
        }
        if (mAdapter != null)
            mAdapter.cancelAll();
        super.onBackPressed();
    }

    public void back(View v) {
        onBackPressed();
    }

    public void previousPage(View v) {
        fitToView();
        /* If not currently viewing the first page of the chapter */
        if (mPager.getCurrentItem() > 0)
            mPager.setCurrentItem(mPager.getCurrentItem() - 1); /* Turn to the previous page */
        else if (mPager.getCurrentItem() == 0) {
            /* Turn to the last page of the previous chapter */
            previousChapter(null);
            mPager.setCurrentItem(mChapter.getPages().size() - 1);
            mChapter.currentIndex = mChapter.getPages().size() - 1;
        }
    }

    public void fastRewindChapter(View v) {
        /* If currently viewing the first page */
        if (mPager.getCurrentItem() == 0)
            previousChapter(null);
        else {
            /* Turn to the first page of the chapter */
            mPager.setCurrentItem(0);
            mChapter.currentIndex = 0;
        }
    }

    public void previousChapter(View v) {
        Series series = mChapter.series;
        int indexOfChapter = series.getChapters().indexOf(mChapter);
        /* Chapters are stored in reverse order such that the first chapter will be the last */
        /* item in the chapter list (size() - 1) */
        /* If the current chapter is not the first in the series */
        if (indexOfChapter < (series.getChapters().size() - 1)) {
            /* Switch to the chapter sequentially before the current one */
            setChapter(series.getChapters().get(indexOfChapter + 1));
        }
    }

    public void nextChapter(View v) {
        /* If currently viewing the last page of the chapter */
        if (mPager.getCurrentItem() == (mChapter.getPages().size() - 1))
            mChapter.currentIndex = 0; /* Rewind this chapter before going to the next one */
        Series series = mChapter.series;
        int indexOfChapter = series.getChapters().indexOf(mChapter);
        /* Chapters are stored in reverse order such that the last chapter will be the first */
        /* item in the chapter list (index 0) */
        /* If the  current chapter is not the last in the series */
        if (indexOfChapter > 0) {
            /* Switch to the chapter sequentially after the current one */
            setChapter(series.getChapters().get(indexOfChapter - 1));
        }
    }

    public void fastForwardChapter(View v) {
        /* If currently viewing the last page of the chapter */
        if (mPager.getCurrentItem() == (mChapter.getPages().size() - 1)) {
            mChapter.currentIndex = 0; /* Rewind this chapter before going to the next one */
            nextChapter(null);
        }
        /* If NOT currently viewing the last page of the chapter */
        else {
            /* Turn to the last page in the chapter */
            mPager.setCurrentItem(mChapter.getPages().size() - 1);
            mChapter.currentIndex = mChapter.getPages().size() - 1;
        }
    }

    public void nextPage(View v) {
        fitToView();
        /* If not currently viewing the last page of the chapter */
        if (mPager.getCurrentItem() < (mChapter.getPages().size() - 1))
            mPager.setCurrentItem(mPager.getCurrentItem() + 1); /* Turn to the next page */
        else if (mPager.getCurrentItem() == (mChapter.getPages().size() - 1)) {
            mChapter.currentIndex = 0; /* Rewind this chapter before going to the next one */
            nextChapter(null); /* Turn to the first page of the next chapter */
        }
    }

    public void reloadPages(View v) {
        if (mPager != null) {
            /* Force the adapter to re-instantiate all page views then return to the current page */
            int currentItem = mPager.getCurrentItem();
            mPager.setAdapter(mAdapter);
            mPager.setCurrentItem(currentItem);
            findViewById(R.id.page_retry_button).setVisibility(View.GONE);
        }
    }

    public void refresh() {
        if (mChapter == null)
            return;
        if (mAdapter != null)
            mAdapter.notifyDataSetChanged();
        TextView chapterTextView = findViewById(R.id.reader_chapter);
        TextView pageProgressView = findViewById(R.id.pager_reader_page_progress);
        View progressBar = findViewById(R.id.reader_chapter_list_progress_bar);
        /* First, the chapter number */
        /* If the chapter number has no decimal points (e.g. 914.0, not 914.5) */
        if (mChapter.chapterNumber == Math.floor(mChapter.chapterNumber))
            chapterTextView.setText(String.valueOf(Math.round(mChapter.chapterNumber)));
        else
            chapterTextView.setText(String.valueOf(mChapter.chapterNumber));
        /* Second, the page progress (e.g. "12/18" for page 12 of 18 in the chapter) */
        String pageProgress = mChapter.currentIndex + 1 + "/" + mChapter.getPages().size();
        if (pageProgressView != null)
            pageProgressView.setText(pageProgress);
        /* Third, hide or show the refresh button */
        if (timedOut)
            findViewById(R.id.page_retry_button).setVisibility(View.VISIBLE);
        else
            findViewById(R.id.page_retry_button).setVisibility(View.GONE);
        /* Fourth, hide or show the "loading chapter" progress bar */
        if (mChapter.getPages().isEmpty())
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.GONE);
    }

    public void setChapter(Chapter chapter) {
        if ((chapter == null) || (chapter.series == null))
            return;
        timedOut = false;
        mChapter = chapter;
        if (Core.get().currentChapter != mChapter) {
            Core.get().currentChapter.freeImages();
            Core.get().currentChapter = mChapter;
        }
        mAdapter = new PagerReaderAdapter(mChapter.getPages());
        mPager.setAdapter(mAdapter);
        refresh();
        mPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            /* The pager will call this whenever the user swipes to a new page */
            public void onPageSelected(int position) {
                mChapter.currentIndex = position;
                String pageProgress = position + 1 + "/" + mChapter.getPages().size();
                ((TextView) findViewById(R.id.pager_reader_page_progress)).setText(pageProgress);
            }
        });
        /* Skip to whichever page the user left swiped to */
        mPager.setCurrentItem(mChapter.currentIndex);
        /* If the chapter's page list needs to be retrieved from its host */
        if (mChapter.getPages().isEmpty() && (mChapter.series.scraper != null)) {
            mChapter.series.scraper.fillChapter(mChapter); /* Retrieve the page list */
            /* The adapter will handle page scraping, as needed, but this task will remove the */
            /* spinning progress bar once the chapter's page is populated (if not already) */
            mWaitTask = new ChapterWaitTask();
            mWaitTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
                    new ChapterWaitParams(mChapter, findViewById(
                            R.id.reader_chapter_list_progress_bar)));
        }
        mChapter.isRead = true;
        if (Utils.areAllChaptersRead(mChapter.series))
            mChapter.series.isRead = true;
        Core.get().writeCatalogLite();
        if (Core.get().mainActivity != null)
            Core.get().mainActivity.refreshSeriesFragment();
    }

    static void writeBundle(Bundle bundle, Chapter chapter, int pageIndex) {
        /* Save information about the current page to be used by onCreate() */
        Page page = chapter.getPages().get(pageIndex);
        /* The following hashes and page index will be needed when reading the catalog to find */
        /* the series and chapter to which the page belongs and to turn to that page within the */
        /* reader once loaded */
        bundle.putInt("series_hash", chapter.series.hashCode());
        bundle.putInt("chapter_hash", chapter.hashCode());
        bundle.putInt("page_index", page.index);
        /* The source, if local, will allow the page adapter to instantly find the page.  Failing */
        /* that, the host will be needed to immediately begin downloading the remote page. */
        bundle.putString("page_source", page.source);
        bundle.putString("series_host", chapter.series.scraper.hostName);
        bundle.putBoolean("page_is_local", page.isLocal);
        /* The following preferences are needed by this activity and PageView in order to avoid */
        /* undefined behavior (page positioning depends on reader orientation) */
        bundle.putString("fullscreen", Core.get().getPreference("fullscreen"));
        bundle.putString("reader_orientation", Core.get().getPreference("reader_orientation"));
        bundle.putString("invert_page_color", Core.get().getPreference("invert_page_color"));
    }

    private void fitToView() {
        if (mAdapter != null) {
            PageView pageView = mAdapter.getCurrentPageView();
            /* If the page view is not currently fit to the view */
            if ((pageView != null) && !pageView.canSwipe()) {
                /* Fit it to the view (the page view will do this one of two ways depending on */
                /* the user's page orientation setting) */
                pageView.fitToView();
            }
        }
    }

    static class ChapterWaitParams {
        Chapter chapter;
        View progressBar;

        ChapterWaitParams(Chapter c, View b) {
            chapter = c;
            progressBar = b;
        }
    }

    protected static class ChapterWaitTask extends AsyncTask<ChapterWaitParams, Void, Void> {
        WeakReference<View> mProgressBar = null;

        @Override
        protected Void doInBackground(ChapterWaitParams... chapterWaitParams) {
            if (isCancelled() || (chapterWaitParams[0].chapter == null) ||
                    (chapterWaitParams[0].progressBar == null))
                return null;
            Chapter chapter = chapterWaitParams[0].chapter;
            mProgressBar = new WeakReference<>(chapterWaitParams[0].progressBar);
            long ticks = System.currentTimeMillis();
            /* While ten seconds have not yet passed */
            while ((System.currentTimeMillis() - ticks) <= 10000) {
                if (isCancelled())
                    return null;
                /* If the chapter list was populated */
                if (!chapter.getPages().isEmpty())
                    return null; /* Stop waiting and continue to onPostExecute() */
                try {
                    Thread.sleep(50); /* Sleep 50 milliseconds */
                } catch (Exception e) {
                    if (!(e instanceof InterruptedException))
                        Utils.Log.e("*ReaderActivity", "ChapterWaitTask error", e);
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
            if (isCancelled())
                return;
            /* Remove the progress bar view whether a page list was found or not */
            if (mProgressBar != null)
                mProgressBar.get().setVisibility(View.GONE);
            Core.get().pagerReaderActivity.mWaitTask = null;
            Core.get().pagerReaderActivity.refresh();
        }
    }
}
