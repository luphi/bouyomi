package luphi.bouyomi.activities;

import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import luphi.bouyomi.Chapter;
import luphi.bouyomi.Core;
import luphi.bouyomi.R;
import luphi.bouyomi.Series;
import luphi.bouyomi.Utils;
import luphi.bouyomi.adapters.StitcherReaderAdapter;

import static luphi.bouyomi.activities.PagerReaderActivity.writeBundle;

public class StitcherReaderActivity extends AppCompatActivity {
    private ListView mListView = null;
    private StitcherReaderAdapter mAdapter = null;
    private Handler mHideHandler = new Handler();
    protected Chapter mChapter = null;
    protected ChapterWaitTask mWaitTask = null;
    protected Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            findViewById(R.id.stitcher_reader_controls).setVisibility(View.INVISIBLE);
        }
    };
    public boolean timedOut = false;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Core.get().pagerReaderActivity = null;
        Core.get().stitcherReaderActivity = this;
        if (bundle != null)
            Core.get().restoreFromBundle(bundle);
        setContentView(R.layout.activity_stitcher_reader);
        if (getSupportActionBar() != null)
            getSupportActionBar().hide();
        setChapter(Core.get().currentChapter);
    }

    @Override
    public void onResume() {
        Core.get().pagerReaderActivity = null;
        Core.get().stitcherReaderActivity = this;
        super.onResume();
        //if (mChapter != null)
        //    setChapter(mChapter);
        if (Core.get().getPreference("fullscreen").equals("true"))
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_FULLSCREEN |
                            View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                            View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                            View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                            View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        if (Core.get().getPreference("reader_orientation").equals("Portrait"))
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        else if (Core.get().getPreference("reader_orientation").equals("Landscape"))
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }

    @Override
    public void onStop() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        super.onStop();
    }

    @Override
    public void onDestroy() {
        if (mChapter != null)
            mChapter.freeImages();
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle bundle) {
        writeBundle(bundle, mChapter, mChapter.currentIndex);
        super.onSaveInstanceState(bundle);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        findViewById(R.id.stitcher_reader_controls).setVisibility(View.VISIBLE);
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, 1000);
        return super.dispatchTouchEvent(event);
    }

    @Override
    public void onBackPressed() {
        Core.get().writeCatalogLite();
        if (Core.get().currentSeries.scraper != null)
            Core.get().currentSeries.scraper.cancelAll();
        if (Core.get().mainActivity != null)
            Core.get().mainActivity.refreshSeriesFragment();
        if (mWaitTask != null) {
            mWaitTask.cancel(true);
            mWaitTask = null;
        }
        if (mAdapter != null)
            mAdapter.cancelAll();
        super.onBackPressed();
    }

    public void back(View v) {
        onBackPressed();
    }

    public void fastRewindChapter(View v) {
        if (mChapter == null)
            return;
        /* If currently viewing the first page */
        if (mChapter.currentIndex == 0)
            previousChapter(null);
        else {
            /* Turn to the first page of the chapter */
            mListView.setSelection(0);
            mChapter.currentIndex = 0;
        }
    }

    public void previousChapter(View v) {
        Series series = mChapter.series;
        int indexOfChapter = series.getChapters().indexOf(mChapter);
        /* Chapters are stored in reverse order such that the first chapter will be the last */
        /* item in the chapter list (size() - 1) */
        /* If the current chapter is not the first in the series */
        if (indexOfChapter < (series.getChapters().size() - 1)) {
            /* Switch to the chapter sequentially before the current one */
            setChapter(series.getChapters().get(indexOfChapter + 1));
        }
    }

    public void nextChapter(View v) {
        /* If currently viewing the last page of the chapter */
        if (mChapter.currentIndex >= (mChapter.getPages().size() - 2))
            mChapter.currentIndex = 0; /* Rewind this chapter before going to the next one */
        Series series = mChapter.series;
        int indexOfChapter = series.getChapters().indexOf(mChapter);
        /* Chapters are stored in reverse order such that the last chapter will be the first */
        /* item in the chapter list (index 0) */
        /* If the  current chapter is not the last in the series */
        if (indexOfChapter > 0) {
            /* Switch to the chapter sequentially after the current one */
            setChapter(series.getChapters().get(indexOfChapter - 1));
        }
    }

    public void fastForwardChapter(View v) {
        /* If currently viewing the last page of the chapter */
        if (mChapter.currentIndex >= (mChapter.getPages().size() - 2)) {
            mChapter.currentIndex = 0; /* Rewind this chapter before going to the next one */
            nextChapter(null);
        }
        /* If NOT currently viewing the last page of the chapter */
        else {
            /* Turn to the last page in the chapter */
            mListView.setSelection(mChapter.getPages().size() - 1);
            mChapter.currentIndex = mChapter.getPages().size() - 1;
        }
    }

    public void refresh() {
        if (mChapter == null)
            return;
        if (mAdapter != null)
            mAdapter.notifyDataSetChanged();
        TextView chapterTextView = findViewById(R.id.reader_chapter);
        View progressBar = findViewById(R.id.reader_chapter_list_progress_bar);
        /* First, the chapter number */
        /* If the chapter number has no decimal points (e.g. 914.0, not 914.5) */
        if (mChapter.chapterNumber == Math.floor(mChapter.chapterNumber))
            chapterTextView.setText(String.valueOf(Math.round(mChapter.chapterNumber)));
        else
            chapterTextView.setText(String.valueOf(mChapter.chapterNumber));
        /* Second, hide or show the refresh button */
        if (timedOut)
            findViewById(R.id.page_retry_button).setVisibility(View.VISIBLE);
        else
            findViewById(R.id.page_retry_button).setVisibility(View.GONE);
        /* Third, hide or show the "loading chapter" progress bar */
        if (mChapter.getPages().isEmpty())
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.GONE);
    }

    public void setChapter(Chapter chapter) {
        if ((chapter == null) || (chapter.series == null))
            return;
        timedOut = false;
        mChapter = chapter;
        if (Core.get().currentChapter != mChapter) {
            Core.get().currentChapter.freeImages();
            Core.get().currentChapter = mChapter;
        }
        refresh();
        mAdapter = new StitcherReaderAdapter(mChapter.getPages());
        mListView = findViewById(R.id.stitcher_reader_list);
        mListView.setAdapter(mAdapter);
        mListView.setOnScrollListener(new AbsListView.OnScrollListener(){
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount,
                                 int totalItemCount) {
                /* If the items aren't yet loaded/visible */
                if (visibleItemCount == 0)
                    return; /* Don't want to divide by 0 below */
                int sum = 0;
                for (int i = 0; i < visibleItemCount; i++) {
                    sum += firstVisibleItem + i;
                }
                /* Approximate the currently viewed page by averaging the visible indices */
                mChapter.currentIndex = sum / visibleItemCount;
            }
            public void onScrollStateChanged(AbsListView view, int scrollState) { } /* Required */
        });
        if (mChapter.currentIndex > 0)
            mListView.setSelection(mChapter.currentIndex);
        if (mChapter.getPages().isEmpty() && (mChapter.series.scraper != null)) {
            mChapter.series.scraper.fillChapter(mChapter);
            mWaitTask = new ChapterWaitTask();
            mWaitTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
                    new PagerReaderActivity.ChapterWaitParams(mChapter, findViewById(
                            R.id.reader_chapter_list_progress_bar)));
        }
        mChapter.isRead = true;
        if (Utils.areAllChaptersRead(mChapter.series))
            mChapter.series.isRead = true;
        Core.get().writeCatalogLite();
        if (Core.get().mainActivity != null)
            Core.get().mainActivity.refreshSeriesFragment();
    }

    protected static class ChapterWaitTask extends PagerReaderActivity.ChapterWaitTask {
        @Override
        protected void onPostExecute(Void v) {
            if (isCancelled())
                return;
            if (mProgressBar != null)
                mProgressBar.get().setVisibility(View.GONE);
            Core.get().stitcherReaderActivity.mWaitTask = null;
            Core.get().stitcherReaderActivity.refresh();
        }
    }
}
