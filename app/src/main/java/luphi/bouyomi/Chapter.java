/* Copyright (C) 2019 Lucas Philipsen
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see http://www.gnu.org/licenses/.
 */

package luphi.bouyomi;

import androidx.annotation.NonNull;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class Chapter implements Comparable<Chapter> {
    private List<Page> mPages = new LinkedList<>();

    public Series series = null; /* Reference to the Series which holds this chapter */
    public String title = ""; /* Chapter title */
    public float chapterNumber = 0.0f; /* Chapter number */
    public String source = ""; /* URL on the host */
    public Date date = new Date(0); /* Date published on the associated host, not the date added */
    public int numLocalPages = 0; /* Number of pages of this chapter currently saved to disk */
    public int currentIndex = 0; /* Page on which the user left off */
    public boolean isLocal = false; /* True if written to disk, false if on a remote host */
    public boolean isRead = false; /* True if any page_pager of the chapter has been viewed */

    public List<Page> getPages() {
        return mPages;
    }

    public void addPage(Page page) {
        if (!mPages.contains(page))
            mPages.add(page);
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Chapter))
            return false;
        return ((hashCode() == other.hashCode()) &&
                (chapterNumber == ((Chapter) other).chapterNumber));
    }

    @Override
    public int compareTo(@NonNull Chapter other) {
        /* If chapter numbers are the same, compare by either date or title */
        if (chapterNumber == other.chapterNumber) {
            /* If even the dates are the same */
            if (date.equals(other.date) && (title != null) && (other.title != null))
                return title.compareTo(other.title);
            return date.compareTo(other.date);
        }
        return Float.compare(chapterNumber, other.chapterNumber);
    }

    @Override
    public int hashCode() {
        if ((series == null) || (series.scraper == null))
            return 0;
        return (series.scraper.hostName.hashCode() * title.hashCode());
    }

    @NonNull
    @Override
    public String toString() {
        return title;
    }

    public void freeImages() {
        for (Page page : mPages)
            page.freeImage();
    }
}
