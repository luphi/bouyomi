# Bouyomi

Bouyomi is a manga/manhuga/comic reader with reading progress, update checking, remote and local storage options, and various lesser features.

It was initially written for personal use but is shared here under a permissive license simply because there's no harm in it.

This project will no longer be maintained.  As it turns out, Tachiyomi not only has a similar name but similar features and is simply better.  Anyone using Bouyomi should try Tachiyomi.

## Screenshots

![](app/src/main/res/drawable/Screenshot_Bouyomi_20190521-212414.png)
![](app/src/main/res/drawable/Screenshot_Bouyomi_20190521-212429.png)
![](app/src/main/res/drawable/Screenshot_Bouyomi_20190521-212555.png)
![](app/src/main/res/drawable/Screenshot_Bouyomi_20190521-212623.png)
![](app/src/main/res/drawable/Screenshot_Bouyomi_20190610-014909.png)

## License

This project is licensed under the GNU Public License v3 - see the [COPYING](COPYING) file for details.